Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction SilentlyContinue | Out-Null

Write-Host "Disabling telemetry" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection", "AllowTelemetry", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection", "AllowTelemetry", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\DataCollection", "AllowTelemetry", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Privacy", "TailoredExperiencesWithDiagnosticDataEnabled", 0)
Disable-ScheduledTask -TaskName "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\Application Experience\ProgramDataUpdater" | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\Autochk\Proxy" | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" | Out-Null

Write-Host "Disabling Wi-Fi Sense" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\WiFi", "AllowWiFiHotSpotReporting", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\WiFi", "AllowAutoConnectToWiFiSenseHotspots", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\config", "AutoConnectAllowedOEM", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\config", "WiFISenseAllowed", 0)

Write-Host "Disabling SmartScreen Filter" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer", "SmartScreenEnabled", "Off")
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost", "EnableWebContentEvaluation", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\$((Get-AppxPackage -AllUsers "Microsoft.MicrosoftEdge").PackageFamilyName)\MicrosoftEdge\PhishingFilter", "EnabledV9", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\$((Get-AppxPackage -AllUsers "Microsoft.MicrosoftEdge").PackageFamilyName)\MicrosoftEdge\PhishingFilter", "PreventOverride", 0)

Write-Host "Disabling Bing Search in Start Menu" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Search", "BingSearchEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Search", "DisableWebSearch", 1)

Write-Host "Disabling Application suggestions" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "ContentDeliveryAllowed", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "OemPreInstalledAppsEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "PreInstalledAppsEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "PreInstalledAppsEverEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "SilentInstalledAppsEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "SubscribedContent-338389Enabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "SystemPaneSuggestionsEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "SubscribedContent-338388Enabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent", "DisableWindowsConsumerFeatures", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent", "DisableThirdPartySuggestions", 1)

Write-Host "Disabling Lock screen spotlight" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "RotatingLockScreenEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "RotatingLockScreenOverlayEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager", "SubscribedContent-338387Enabled", 0) 

Write-Host "Disabling Location Tracking" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Sensor\Overrides\{BFA794E4-F964-4FDB-90F6-51056BFE4B44}", "SensorPermissionState", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\lfsvc\Service\Configuration", "Status", 0)

Write-Host "Disabling automatic Maps updates" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SYSTEM\Maps", "AutoUpdateEnabled", 0)

Write-Host "Disabling Feedback" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Siuf\Rules", "NumberOfSIUFInPeriod", 0)
Disable-ScheduledTask -TaskName "Microsoft\Windows\Feedback\Siuf\DmClient" -ErrorAction SilentlyContinue | Out-Null
Disable-ScheduledTask -TaskName "Microsoft\Windows\Feedback\Siuf\DmClientOnScenarioDownload" -ErrorAction SilentlyContinue | Out-Null

Write-Host "Disabling Advertising ID" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo", "Enabled", 0)

Write-Host "Disabling Cortana" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Personalization\Settings", "AcceptedPrivacyPolicy", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\InputPersonalization", "RestrictImplicitTextCollection", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\InputPersonalization", "RestrictImplicitInkCollection", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore", "HarvestContacts", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Search", "AllowCortana", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Search", "AllowCloudSearch", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Search", "AllowCortanaAboveLock", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "CortanaConsent", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "HistoryViewEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "DeviceHistoryEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "VoiceShortcut", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Speech_OneCore\Preferences", "VoiceActivationOn", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Speech_OneCore\Preferences", "VoiceActivationEnableAboveLockscreen", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Speech_OneCore\Preferences", "ModelDownloadAllowed", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Speech_OneCore\Preferences", "VoiceActivationDefaultOn", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "ShowCortanaButton", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Search", "CortanaEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Search", "CortanaInAmbientMode", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "CortanaEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search", "CanCortanaBeEnabled", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\Experience\AllowCortana", "value", 0)

Write-Host "Disabling Error reporting" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting", "Disabled", 1)
Disable-ScheduledTask -TaskName "Microsoft\Windows\Windows Error Reporting\QueueReporting" | Out-Null

Write-Host "Removing AutoLogger file and restricting directory" -Foreground Yellow
Remove-Item -Path "$env:PROGRAMDATA\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl" -Force -ErrorAction SilentlyContinue | Out-Null
icacls "$env:PROGRAMDATA\Microsoft\Diagnosis\ETLLogs\AutoLogger" /deny SYSTEM:`(OI`)`(CI`)F | Out-Null

Write-Host "Disabling Autoplay" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers", "DisableAutoplay", 1)

Write-Host "Disabling Autorun for all drives" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer", "NoDriveTypeAutoRun", 255)

Write-Host "Disabling Action Center" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Policies\Microsoft\Windows\Explorer", "DisableNotificationCenter", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\PushNotifications", "ToastEnabled", 0)

Write-Host "Disabling Sticky keys prompt" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Control Panel\Accessibility\StickyKeys", "Flags", "506")

Write-Host "Showing file operations details" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\OperationStatusManager", "EnthusiastMode", 1)

Write-Host "Hiding Taskbar Search box / button" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Search", "SearchboxTaskbarMode", 0)

Write-Host "Hiding Task View button" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "ShowTaskViewButton", 0)

Write-Host "Showing small icons in taskbar" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "TaskbarSmallIcons", 1)

Write-Host "Hiding titles in taskbar" -Foreground Yellow
[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", $true).DeleteValue("TaskbarGlomLevel")

Write-Host "Hiding People icon" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People", "PeopleBand", 0)

Write-Host "Showing all tray icons" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer", "EnableAutoTray", 0)

Write-Host "Showing known file extensions" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "HideFileExt", 0)

Write-Host "Showing hidden files" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "Hidden", 1)

Write-Host "Hiding sync provider notifications" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "ShowSyncProviderNotifications", 0)

Write-Host "Hiding recent shortcuts" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer", "ShowRecent", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer", "ShowFrequent", 0)

Write-Host "Changing default Explorer view to This PC" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "LaunchTo", 1)

Write-Host "Showing This PC shortcut on desktop" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0)

Write-Host "Hiding Music icon from This PC" -Foreground Yellow
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{1CF1260C-4DD0-4ebb-811F-33C572699FDE}", $false)

Write-Host "Hiding Music icon from Explorer namespace" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{a0c69a99-21c8-4671-8703-7934162fcf1d}\PropertyBag", "ThisPCPolicy", "Hide")
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{a0c69a99-21c8-4671-8703-7934162fcf1d}\PropertyBag", "ThisPCPolicy", "Hide")

Write-Host "Hiding Pictures icon from This PC" -Foreground Yellow
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3ADD1653-EB32-4cb0-BBD7-DFA0ABB5ACCA}", $false)

Write-Host "Hiding Pictures icon from Explorer namespace" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{0ddd015d-b06c-45d5-8c4c-f59713854639}\PropertyBag", "ThisPCPolicy", "Hide")
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{0ddd015d-b06c-45d5-8c4c-f59713854639}\PropertyBag", "ThisPCPolicy", "Hide")

Write-Host "Hiding Videos icon from This PC" -Foreground Yellow
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{A0953C92-50DC-43bf-BE83-3742FED03C9C}", $false)

Write-Host "Hiding Videos icon from Explorer namespace" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{35286a68-3c57-41a1-bbb1-0eae73d76c95}\PropertyBag", "ThisPCPolicy", "Hide")
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{35286a68-3c57-41a1-bbb1-0eae73d76c95}\PropertyBag", "ThisPCPolicy", "Hide")

Write-Host "Hiding 3D Objects icon from This PC" -Foreground Yellow
[Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}", $false)

Write-Host "Hiding 3D Objects icon from Explorer namespace" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{31C0DD25-9439-4F12-BF41-7FF4EDA38722}\PropertyBag", "ThisPCPolicy", "Hide")
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{31C0DD25-9439-4F12-BF41-7FF4EDA38722}\PropertyBag", "ThisPCPolicy", "Hide")

Write-Host "Disabling quick access" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Explorer", "HubMode", 1)

Write-Host "Unpinning all Start Menu tiles" -Foreground Yellow
If ([System.Environment]::OSVersion.Version.Build -ge 15063 -And [System.Environment]::OSVersion.Version.Build -le 16299) {
        Get-ChildItem -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\Cache\DefaultAccount" -Include "*.group" -Recurse | ForEach-Object {
                $data = (Get-ItemProperty -Path "$($_.PsPath)\Current", "Data").Data -Join ","
                $data = $data.Substring(0, $data.IndexOf(",0,202,30") + 9) + ",0,202,80,0,0"
                Set-ItemProperty -Path "$($_.PsPath)\Current", "Data" -Type Binary -Value $data.Split(",")
        }
} ElseIf ([System.Environment]::OSVersion.Version.Build -ge 17134) {
        $key = Get-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudStore\Store\Cache\DefaultAccount\*start.tilegrid`$windows.data.curatedtilecollection.tilecollection\Current"
        $data = $key.Data[0..25] + ([byte[]](202,50,0,226,44,1,1,0,0))
        Set-ItemProperty -Path $key.PSPath -Name "Data" -Type Binary -Value $data
        Stop-Process -Name "ShellExperienceHost" -Force -ErrorAction SilentlyContinue
}

Write-Host "Unpinning all Taskbar icons" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband", "Favorites", 0xFF)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband", $true).DeleteValue("FavoritesResolve")

Write-Host "Enabling dark mode" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize", "AppsUseLightTheme", 0)

Write-Host "Stopping and disabling Diagnostics Tracking Service" -Foreground Yellow
Stop-Service "DiagTrack" -WarningAction SilentlyContinue
Set-Service "DiagTrack" -StartupType Disabled

Write-Host "Stopping and disabling OneSyncSvc Service" -Foreground Yellow
Stop-Service "OneSyncSvc" -WarningAction SilentlyContinue
Set-Service "OneSyncSvc" -StartupType Disabled

Write-Host "Stopping and disabling WAP Push Service" -Foreground Yellow
Stop-Service "dmwappushservice" -WarningAction SilentlyContinue
Set-Service "dmwappushservice" -StartupType Disabled

Write-Host "Stopping and disabling Superfetch service" -Foreground Yellow
Stop-Service "SysMain" -WarningAction SilentlyContinue
Set-Service "SysMain" -StartupType Disabled

#Write-Host "Stopping and disabling Windows Search indexing service" -Foreground Yellow
#Stop-Service "WSearch" -WarningAction SilentlyContinue
#Set-Service "WSearch" -StartupType Disabled

Write-Host "Disabling scheduled tasks" -Foreground Yellow
$sch_tasks=$("ScheduledDefrag","Microsoft Compatibility Appraiser","ProgramDataUpdater","Consolidator","KernelCeipTask","UsbCeip","Microsoft-Windows-DiskDiagnosticDataCollector","GatherNetworkInfo","QueueReporting")
#$sch_tasks | % { Disable-ScheduledTask -TaskName $_ -ErrorAction SilentlyContinue | Out-Null }
$sch_tasks | % { Unregister-ScheduledTask -TaskName $_ -ErrorAction SilentlyContinue -Confirm:$false | Out-Null }

Write-Host "Adding Command Prompt to context menu" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "MUIVerb", "Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "Icon", "cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuCmd")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "Icon","cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "MUIVerb", "Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open\command", "", "cmd.exe /s /k pushd `"%V`"")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "Icon", "cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "MUIVerb", "Elevated Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "HasLUASHield", "")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas\command", "", "cmd.exe /s /k pushd `"%V`"")

Write-Host "Adding PowerShell to context menu" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "MUIVerb", "PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "Icon", "powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuPowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "Icon","powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "MUIVerb", "PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "Icon", "powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "MUIVerb", "Elevated PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "HasLUASHield", "")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")

Write-Host "Setting consistent fonts and sizes" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "QuickEdit", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "CursorType", 2)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "FaceName", "Consolas")
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "FontWeight", 400)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "FontSize", 0x100008)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "WindowSize", 0x001e005a)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "DefaultBackground", 0xffffffff)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "DefaultBackground", 0xffffffff)
[Microsoft.Win32.Registry]::SetValue("HKEY_CURRENT_USER\Console", "ScreenColors", 7)

[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("Console\%SystemRoot%_system32_cmd.exe", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("Console\%SystemRoot%_system32_WindowsPowerShell_v1.0_powershell.exe", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe", $false)

Write-Host "Disabling Windows Feeds" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Windows Feeds", "EnableFeeds", 0)

Write-Host "Disabling MeetNow" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "HideSCAMeetNow", 1)

Write-Host "Disabling automatic Windows updates" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "NoAutoUpdate", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "AUOptions", 2)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "ScheduledInstallDay", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU", "ScheduledInstallTime", 3)

Write-Host "Removing OneDrive" -Foreground Yellow
C:\Windows\SysWOW64\OneDriveSetup.exe /Uninstall
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive", "DisableFileSyncNGSC", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive", "DisableFileSync", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}", "System.IsPinnedToNameSpaceTree", 0)
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Wow6432Node\{018D5C66-4533-4307-9B53-224DE2ED1FE6}", "System.IsPinnedToNameSpaceTree", 0)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("CurrentUser", [Microsoft.Win32.RegistryView]::Registry64).OpenSubKey("Environment", $true).DeleteValue("OneDrive")
[Microsoft.Win32.RegistryKey]::OpenBaseKey("ClassesRoot", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}", $false)
[Microsoft.Win32.RegistryKey]::OpenBaseKey("ClassesRoot", [Microsoft.Win32.RegistryView]::Registry64).DeleteSubKeyTree("Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}", $false)
Remove-Item -Path "$env:UserProfile\OneDrive" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "$env:LocalAppData\Microsoft\OneDrive" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "$env:ProgramData\Microsoft OneDrive" -Recurse -Force -ErrorAction SilentlyContinue
Remove-Item -Path "$env:SystemDrive\OneDriveTemp" -Recurse -Force -ErrorAction SilentlyContinue

Write-Host "Removing unnecessary AppX packages" -Foreground Yellow
$SafeApps = "AAD.brokerplugin|accountscontrol|apprep.chxapp|assignedaccess|asynctext|bioenrollment|capturepicker|cloudexperience|contentdelivery|desktopappinstaller|ecapp|edge|extension|getstarted|immersivecontrolpanel|lockapp|net.native|oobenet|parentalcontrols|PPIProjection|search|sechealth|secureas|shellexperience|startmenuexperience|terminal|vclibs|xaml|XGpuEject"
Get-AppxPackage -PackageTypeFilter bundle -AllUsers | Where-Object { $_.DisplayName -notmatch $SafeApps } | Remove-AppxPackage -ErrorAction SilentlyContinue | Out-Null
Get-AppxPackage -AllUsers | Where-Object { $_.DisplayName -notmatch $SafeApps } | Remove-AppxPackage -ErrorAction SilentlyContinue | Out-Null

Write-Host "Removing default desktop shortcuts" -Foreground Yellow
(Get-ChildItem -Path "C:\Users" -Recurse -File -Filter "*.lnk" -ErrorAction SilentlyContinue).FullName | % { Remove-Item $_ -Force -ErrorAction SilentlyContinue }
