#!/bin/bash

plugins=$(packer plugins installed)

if ! printf "%s\n" "${plugins}" | grep -qi vmware; then
    packer plugins install github.com/hashicorp/vmware
else
    printf "%s\n" "[INFO] packer vmware plugin already installed"
fi

if ! printf "%s\n" "${plugins}" | grep -qi virtualbox; then
    packer plugins install github.com/hashicorp/virtualbox
else
    printf "%s\n" "[INFO] packer virtualbox plugin already installed"
fi

if ! printf "%s\n" "${plugins}" | grep -qi qemu; then
    packer plugins install github.com/hashicorp/qemu
else
    printf "%s\n" "[INFO] packer qemu plugin already installed"
fi

if ! printf "%s\n" "${plugins}" | grep -qi docker; then
    packer plugins install github.com/hashicorp/docker
else
    printf "%s\n" "[INFO] packer docker plugin already installed"
fi

