Get-WmiObject Win32_UserAccount -Filter "Name='vagrant'" | % { $_.PasswordExpires = $false; $_.Put() }

Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Power" -Name "HiberFileSizePercent" -Value 0 -Force
Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Power" -Name "HibernateEnabled" -Value 0 -Force

if ((Get-WmiObject -Class Win32_OperatingSystem).ProductType -ne 1) {
    Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "DisableCAD" -Value 1 -Force
}
