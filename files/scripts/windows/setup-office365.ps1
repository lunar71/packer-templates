@"
<Configuration>
    <Add OfficeClientEdition="64" Channel="Current">
        <Product ID="O365ProPlusRetail">
            <Language ID="en-us"/>
            <ExcludeApp ID="Access"/>
            <ExcludeApp ID="Groove"/>
            <ExcludeApp ID="Lync"/>
            <ExcludeApp ID="OneDrive"/>
            <ExcludeApp ID="OneNote"/>
            <ExcludeApp ID="Outlook"/>
            <ExcludeApp ID="Publisher"/>
            <ExcludeApp ID="Teams"/>
        </Product>
    </Add>
   <Display Level="Full" AcceptEULA="TRUE"/>
</Configuration>
"@ | Out-File "C:\Windows\Temp\office365.xml"

$officedeploymenttool = (Invoke-WebRequest -UseBasicParsing -Uri "https://www.microsoft.com/en-us/download/confirmation.aspx?id=49117").Links | Where-Object { $_.href -like "*officedeploymenttool*.exe" } | Select -ExpandProperty href | Get-Unique | Select -First 1
(New-Object System.Net.WebClient).DownloadFile($officedeploymenttool, "C:\Windows\Temp\officedeploymenttool.exe")
Write-Output "[INFO] Downloaded Office Deployment Tool"

Start-Process -FilePath "C:\Windows\Temp\officedeploymenttool.exe" -ArgumentList "/extract:C:\Windows\Temp\Office /passive /quiet" -Wait -Verbose
Write-Output "[INFO] Extracted ODT to C:\Windows\Temp\Office"

Start-Process -FilePath "C:\Windows\Temp\Office\setup.exe" -ArgumentList "/download C:\Windows\Temp\office365.xml" -Wait -Verbose
Write-Host "[INFO] Downloaded Office to C:\Windows\Temp"

Start-Process -FilePath "C:\Windows\Temp\Office\setup.exe" -ArgumentList "/configure C:\Windows\Temp\office365.xml" -Wait -Verbose
Write-Host "[INFO] Configured Office"

