$cert = New-SelfSignedCertificate -CertstoreLocation Cert:\LocalMachine\My -DnsName "WinRMCertificate"
Enable-PSRemoting -SkipNetworkProfileCheck -Force

New-Item -Path WSMan:\localhost\Listener -Transport HTTPS -Address * -CertificateThumbPrint $cert.Thumbprint -Force

$version=(Get-WmiObject win32_operatingsystem).caption
if($version.trim() -like '*Windows Server 2008*') {
    Write-Host "Running on Windows Server 2008, using netsh instead of New-NetFirewallRule"
    netsh advfirewall firewall set rule group="remote administration" new enable=yes
    netsh firewall add portopening TCP 5986 "Port 5986"
} else {
    New-NetFirewallRule -DisplayName "Windows Remote Management (HTTPS-In)" -Name "Windows Remote Management (HTTPS-In)" -Profile Any -LocalPort 5986 -Protocol TCP
}

Set-Item WSMan:\localhost\Service\AllowUnencrypted  -Value $false
Set-Item WSMan:\localhost\Service\Auth\Basic        -Value $true
Set-Item WSMan:\localhost\Service\Auth\Negotiate    -Value $true
Set-Item WSMan:\localhost\Service\Auth\Kerberos     -Value $false
Set-Item WSMan:\localhost\Service\Auth\CredSSP      -Value $false
Set-Item WSMan:\localhost\Client\Auth\Basic         -Value $false
Set-Item WSMan:\localhost\Client\Auth\Digest        -Value $false
Set-Item WSMan:\localhost\Client\Auth\Negotiate     -Value $true
Set-Item WSMan:\localhost\Client\Auth\Kerberos      -Value $false
Set-Item WSMan:\localhost\Client\Auth\CredSSP       -Value $false

Set-Service -Name WinRM -StartupType Automatic
