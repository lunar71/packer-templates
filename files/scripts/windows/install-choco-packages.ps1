choco install -y --ignore-checksums --no-progress C:\choco-packages.config

$xmlContent = Get-Content -Path "C:\choco-packages.config" -Raw
$xml = [xml]$xmlContent
$packageIds = $xml.SelectNodes('//package') | ForEach-Object { $_.id }
$packages = $($packageIds -join "\n")
@"
Installed chocolatey packages: $packageIds
"@ | Out-File C:\README.txt

Remove-Item C:\choco-packages.config -Force | Out-Null
