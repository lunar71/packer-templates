#!/bin/bash

if test $1 == "qemu"; then
    ./build.sh \
        -v qemu \
        -t ubuntu-10.04-server.json \
        -t ubuntu-12.04-server.json \
        -t ubuntu-14.04-server.json \
        -t ubuntu-16.04-server.json \
        -t ubuntu-18.04-server.json \
        -t ubuntu-20.04-server.json \
        -t ubuntu-22.04-server.json \
        -t ubuntu-24.04-server.json \
        -t ubuntu-22.04-gnome.json \
        -t ubuntu-22.04-kubuntu.json \
        -t ubuntu-22.04-xubuntu.json \
        -t ubuntu-24.04-gnome.json \
        -t winserver2019-desktop.json \
        -t win10-22h2-pro-clean.json \
        -t win10-22h2-pro-office365.json \
        -t winserver2012-clean.json \
        -t winserver2016-clean.json \
        -t winserver2019-clean.json \
        -t kali-everything-offline-luks-kde.json \
        -t kali-everything-offline-luks-xfce.json \
        -t kali-top10-offline-luks-kde.json \
        -t kali-top10-offline-luks-xfce.json
else
    ./build.sh \
        -v vmware,vbox \
        -t ubuntu-10.04-server.json \
        -t ubuntu-12.04-server.json \
        -t ubuntu-14.04-server.json \
        -t ubuntu-16.04-server.json \
        -t ubuntu-18.04-server.json \
        -t ubuntu-20.04-server.json \
        -t ubuntu-22.04-server.json \
        -t ubuntu-24.04-server.json \
        -t ubuntu-22.04-gnome.json \
        -t ubuntu-22.04-kubuntu.json \
        -t ubuntu-22.04-xubuntu.json \
        -t ubuntu-24.04-gnome.json \
        -t winserver2012-clean.json \
        -t winserver2016-clean.json \
        -t winserver2019-clean.json \
        -t winserver2019-desktop.json \
        -t win10-22h2-pro-clean.json \
        -t win10-22h2-pro-office365.json \
        -t kali-everything-offline-luks-kde.json \
        -t kali-everything-offline-luks-xfce.json \
        -t kali-top10-offline-luks-kde.json \
        -t kali-top10-offline-luks-xfce.json
fi
