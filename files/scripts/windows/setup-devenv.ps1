#Requires -RunAsAdministrator

Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction SilentlyContinue | Out-Null

$tools_dir = "C:\Tools"
$temp_dir = "C:\Windows\Temp"

New-Item $tools_dir -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
Set-MpPreference -ExclusionPath $tools_dir -ErrorAction SilentlyContinue | Out-Null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$ProgressPreference = "SilentlyContinue"
$global:ProgressPreference = "SilentlyContinue"

$wc = New-Object System.Net.WebClient
$wc.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"

Function Get-File {
    [CmdletBinding()]
    param(
        [Parameter(mandatory = $true)]$Url,
        [Parameter(mandatory = $true)]$Destination,
        [Parameter()]$Pattern,
        [Parameter()][switch]$GitHubRelease,
        [Parameter()]$ExtractDestination,
        [Parameter()][switch]$Install,
        [Parameter()]$InstallArgs,
        [Parameter()][switch]$MsiExec,
        [Parameter()]$MsiExecArgs,
        [Parameter()]$Prepend
    )

    if ($Pattern) {
        if ($GitHubRelease) {
            $f = (Invoke-WebRequest -Uri $Url -UseBasicParsing).Content `
                | ConvertFrom-Json `
                | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
                | Where-Object { $_ -like "$Pattern" } `
                | Select-Object -First 1 `
                | Select-Object -ExpandProperty "browser_download_url"
        } else {
            $f = (Invoke-WebRequest -Uri $Url -UseBasicParsing).Links `
                | Where-Object { $_ -like "$Pattern" } `
                | Select-Object -ExpandProperty href `
                | Select-Object -First 1
        }
        if ($Prepend) { $f = ($Prepend,$f | foreach {$_.trim('/')}) -join '/' }
            Write-Host "[INFO] Downloading $f to $Destination"
            try {
                $wc.DownloadFile($f, $Destination)
            } catch { Write-Host "[ERR] Failed to download $f to $Destination" }

    } else {
        Write-Host "[INFO] Downloading $Url to $Destination"
        try {
            $wc.DownloadFile($Url, $Destination)
        } catch { Write-Host "[ERR] Failed to download $Url to $Destination" }
    }

    if ($ExtractDestination) {
        Write-Host "[INFO] Extracting $Destination to $ExtractDestination"
        try {
            Expand-Archive -Path $Destination -DestinationPath $ExtractDestination
        } catch { Write-Host "[ERR] Failed to extract $Destination to $ExtractDestination" }
    }

    if ($Install) {
        Write-Host "[INFO] Installing $Destination"
        try {
            Start-Process -FilePath $Destination -ArgumentList "$InstallArgs" -Wait -Verbose
        } catch { Write-Host "[ERR] Failed to install $Destination with $InstallArgs arguments" }
    }

    if ($MsiExec) {
        Write-Host "[INFO] Installing $Destination with MsiExec"
        try {
            Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $Destination $MsiExecArgs" -Wait -Verbose
        } catch { Write-Host "[ERR] Failed to install $Destination with $MsiExecArgs MsiExec arguments" }
    }
}

Get-File -Url "https://download.sysinternals.com/files/SysinternalsSuite.zip" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "SysinternalsSuite.zip") `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "SysinternalSuite")

Get-File -Url "https://sourceforge.net/projects/regshot/files/latest/download" `
         -Destination (Join-Path -Path $tools_dir -ChildPath "regshot.7z")

Get-File -Url "https://www.winitor.com/tools/pestudio/current/pestudio.zip" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "pestudio.zip") `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "pestudio")

Get-File -Url "http://www.angusj.com/resourcehacker/resource_hacker.zip" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "resource_hacker.zip") `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "resource_hacker")

Get-File -Url "https://github.com/dnSpy/dnSpy/releases/download/v6.1.8/dnSpy-net-win64.zip" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "dnSpy.zip") `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "dnSpy")

Get-File -Url "https://download.mozilla.org/?product=firefox-esr-latest-ssl&os=win64&lang=en-US" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "firefox-esr.exe") `
         -Install -InstallArgs "/S /PreventRebootRequired=true"

Get-File -Url "https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "python-3.10.0-amd64.exe") `
         -Install -InstallArgs "/quiet InstallAllUsers=1 PrependPath=1 Include_test=0"

Get-File -Url "https://ntcore.com/files/ExplorerSuite.exe" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "ExplorerSuite.exe") `
         -Install -InstallArgs "/SILENT"

Get-File -Url "http://www.rohitab.com/download" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "api-monitor.exe") `
         -Pattern "*-setup-x64.exe*" `
         -Install -InstallArgs "/s /v`"/qb`""

Get-File -Url "https://git-scm.com/download/win" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "git.exe") `
         -Pattern "*Git*64*exe*" `
         -Install -InstallArgs "/VERYSILENT /NORESTART"

Get-File -Url "https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "putty.msi") `
         -Pattern "*64bit*.msi*" `
         -MsiExec -MsiExecArgs "/qb"

Get-File -Url "https://www.oracle.com/java/technologies/downloads" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "java-17.exe") `
         -Pattern "*jdk-17*.exe*" `
         -Install -InstallArgs "/s"

Get-File -Url "https://www.wireshark.org/download.html" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "wireshark.exe") `
         -Pattern "*win64-*.exe*" `
         -Install -InstallArgs "/S"

Get-File -Url "https://api.github.com/repos/x64dbg/x64dbg/releases" `
         -Destination (Join-Path -Path $tools_dir -ChildPath "x64dbg.zip") `
         -GitHubRelease -Pattern "*zip*"  # TODO: NOT LIKE PDB

Get-File -Url "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "ghidra.zip") `
         -GitHubRelease -Pattern "*ghidra_*.zip*" `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "ghidra")

Get-File -Url "https://api.github.com/repos/hasherezade/pe-bear-releases/releases" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "pebear.zip") `
         -GitHubRelease -Pattern "*x64_win_vs13*" `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "pebear")

Get-File -Url "https://api.github.com/repos/dnSpyEx/dnSpy/releases" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "dnSpyEx.zip") `
         -GitHubRelease -Pattern "*-net-win64.zip*" `
         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "dnSpyEx")

Get-File -Url "https://processhacker.sourceforge.io/downloads.php" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "process_hacker.exe") `
         -Pattern "*setup.exe*" `
         -Install -InstallArgs "install /silent"

Get-File -Url "https://mh-nexus.de/downloads/HxDSetup.zip" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "HxDSetup.zip") `
         -ExtractDestination (Join-Path -Path $temp_dir -ChildPath "HxDSetup")
Start-Process -FilePath (Join-Path -Path $temp_dir -ChildPath "HxDSetup\HxDSetup.exe") -ArgumentList "/SILENT" -Wait -Verbose

Get-File -Url "https://www.7-zip.org" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "7z.exe") `
         -Pattern "*-x64.exe*"  `
         -Prepend "https://www.7-zip.org" `
         -Install -InstallArgs "/S"

Get-File -Url "https://api.github.com/repos/notepad-plus-plus/notepad-plus-plus/releases" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "npp.exe") `
         -GitHubRelease -Pattern "*npp*installer*x64*exe*" `
         -Install -InstallArgs "/S"

Get-File -Url "https://api.github.com/repos/PintaProject/Pinta/releases" `
         -Destination (Join-Path -Path $temp_dir -ChildPath "pinta.exe") `
         -GitHubRelease -Pattern "*pinta*exe*" `
         -Install -InstallArgs "/install /quiet /norestart /silent /verysilent"

Write-Host "Adding Command Prompt to context menu" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "MUIVerb", "Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "Icon", "cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuCmd")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "Icon","cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "MUIVerb", "Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open\command", "", "cmd.exe /s /k pushd `"%V`"")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "Icon", "cmd.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "MUIVerb", "Elevated Command Prompt")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "HasLUASHield", "")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas\command", "", "cmd.exe /s /k pushd `"%V`"")

Write-Host "Adding PowerShell to context menu" -Foreground Yellow
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "MUIVerb", "PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "Icon", "powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuPowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "Icon","powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "MUIVerb", "PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "Icon", "powershell.exe")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "MUIVerb", "Elevated PowerShell")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "HasLUASHield", "")
[Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")

$readme = @"
Developer Windows 10 build

Additions:
    - custom tools to C:\Tools
    - custom scripts to C:\Scripts
    - installed vs community, office365, firefox esr, python3.10, explorersuite, api-monitor, git, oracle java 17, wireshark, processhacker, hxd, 7zip, notepad++, pinta
    - context menus for command prompt and powershell
"@

$readme | Out-File "C:\Users\Public\Desktop\README.txt"

#Get-File -Url "https://telerik-fiddler.s3.amazonaws.com/fiddler/FiddlerSetup.exe" `
#         -Destination (Join-Path -Path $temp_dir -ChildPath "FiddlerSetup.exe") `
#         -Install -InstallArgs "/S"

#Get-File -Url "https://download.jetbrains.com/product?code=tb&latest&distribution=windows" `
#         -Destination (Join-Path -Path $temp_dir -ChildPath "jetbrains-toolbox.exe") `
#         -Install -InstallArgs "/S"

#Get-File -Url "https://api.github.com/repos/mkaring/ConfuserEx/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "ConfuserEx-GUI.zip") `
#         -GitHubRelease -Pattern "*ConfuserEx*GUI*.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "ConfuserEx-GUI")
#
#Get-File -Url "https://api.github.com/repos/pwntester/ysoserial.net/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "ysoserialnet.zip") `
#         -GitHubRelease -Pattern "*zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "ysoserialnet")
#
#Get-File -Url "https://api.github.com/repos/frohoff/ysoserial/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "ysoserial.jar") `
#         -GitHubRelease -Pattern "*jar*" 

#Get-File -Url "https://api.github.com/repos/java-decompiler/jd-gui/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "jd-gui.jar") `
#         -GitHubRelease -Pattern "*jar*" 

#Get-File -Url "https://api.github.com/repos/gurnec/HashCheck/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "hashcheck.exe") `
#         -GitHubRelease -Pattern "*.exe*" `
#         -Install -InstallArgs "/S"

#Get-File -Url "https://api.github.com/repos/glmcdona/Process-Dump/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "p32.exe") `
#         -GitHubRelease -Pattern "*pd32.exe*" 
#
#Get-File -Url "https://api.github.com/repos/glmcdona/Process-Dump/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "p64.exe") `
#         -GitHubRelease -Pattern "*pd64.exe*" 
#
#Get-File -Url "https://api.github.com/repos/enkomio/RunDotNetDll/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "rundotnetdll.zip") `
#         -GitHubRelease -Pattern "*rundotnetdll.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "rundotnetdll")
#
#Get-File -Url "https://api.github.com/repos/Bioruebe/UniExtract2/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "uniextract2.zip") `
#         -GitHubRelease -Pattern "*uniextract*.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "uniextract2")
#
#Get-File -Url "https://api.github.com/repos/mandiant/flare-fakenet-ng/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "fakenet-ng.zip") `
#         -GitHubRelease -Pattern "*fakenet*.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "fakenet-ng")

#Get-File -Url "https://api.github.com/repos/gchq/CyberChef/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "cyberchef.zip") `
#         -GitHubRelease -Pattern "*cyberchef*.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "cyberchef")

#Get-File -Url "https://api.github.com/repos/hasherezade/hollows_hunter/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "hollows_hunter32.exe") `
#         -GitHubRelease -Pattern "*hollows_hunter32*.exe*"
#
#Get-File -Url "https://api.github.com/repos/hasherezade/hollows_hunter/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "hollows_hunter64.exe") `
#         -GitHubRelease -Pattern "*hollows_hunter64*.exe*"
#
#Get-File -Url "https://api.github.com/repos/dzzie/MAP/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "malware_analyst_pack.exe") `
#         -GitHubRelease -Pattern "*map*setup*.exe*" `
#         -Install -InstallArgs "/silent"

#Get-File -Url "https://api.github.com/repos/mandiant/capa/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "capa.zip") `
#         -GitHubRelease -Pattern "*capa-*-windows*.zip*" `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "capa")

#Get-File -Url "https://api.github.com/repos/skahwah/SQLRecon/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "sqlrecon.exe") `
#         -GitHubRelease -Pattern "*sqlrecon*exe*"

#Get-File -Url "https://portswigger.net/burp/releases/professional/latest" `
#         -Destination (Join-Path -Path $tools_dir -ChildPath "burp_pro-latest.jar") `
#         -Pattern "*product=pro*jar*" `
#         -Prepend "https://portswigger.net"
#
#Get-File -Url "https://portswigger.net/burp/releases/community/latest" `
#         -Destination (Join-Path -Path $tools_dir -ChildPath "burp_community-latest.jar") `
#         -Pattern "*product=community*jar*" `
#         -Prepend "https://portswigger.net"
#
#Get-File -Url "https://www.torproject.org/download" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "tor-browser.exe") `
#         -Pattern "*.exe*" `
#         -Prepend "https://www.torproject.org"
#
#Get-File -Url "https://www.heidisql.com/download.php" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "heidisql.exe") `
#         -Pattern "*setup*.exe*" `
#         -Prepend "https://www.heidisql.com" `
#         -Install -InstallArgs "/SILENT /VERYSILENT /NORESTART /CURRENTUSER"

#Get-File -Url "https://windows.metasploit.com/metasploitframework-latest.msi" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "metasploitframework-latest.msi") `
#         -MsiExec -MsiExecArgs "/qb INSTALLLOCATION=`"C:\metasploit-framework`""

#Get-File -Url "https://awscli.amazonaws.com/AWSCLIV2.msi" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "AWSCLIV2.msi") `
#         -MsiExec -MsiExecArgs "/qb"
#
#Get-File -Url "https://go.microsoft.com/fwlink/?linkid=2200731" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "msodbcsql.msi") `
#         -MsiExec -MsiExecArgs "/qb IACCEPTMSODBCSQLLICENSETERMS=YES"
#
#Get-File -Url "https://go.microsoft.com/fwlink/?linkid=2142258" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "MsSqlCmdLnUtils.msi") `
#         -MsiExec -MsiExecArgs "/qb IACCEPTMSSQLCMDLNUTILSLICENSETERMS=YES"

#Get-File -Url "https://go.dev/dl/go1.20.1.windows-amd64.msi" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "golang-amd64.msi") `
#         -MsiExec -MsiExecArgs "/qb"

#Get-File -Url "https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7B486B20EC-8AE9-BA1E-6288-EA45FE27A42D%7D%26lang%3Den%26browser%3D3%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dtrue%26ap%3Dx64-stable-statsdef_0%26brand%3DGCEA/dl/chrome/install/googlechromestandaloneenterprise64.msi" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "google-chrome-enterprise.msi") `
#         -MsiExec -MsiExecArgs "/qb"

#Get-File -Url "https://nmap.org/download.html" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "nmap.exe") `
#         -Pattern "*-setup.exe*"
#
#Get-File -Url "https://nmap.org/download.html" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "npcap.exe") `
#         -Pattern "*npcap*.exe*"

#Get-File -Url "https://api.github.com/repos/iBotPeaches/Apktool/releases" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "apktool.jar") `
#         -GitHubRelease -Pattern "*jar*"
#
#Get-File -Url "https://api.github.com/repos/ropnop/go-windapsearch/releases" `
#         -Destination (Join-Path -Path $tools_dir -ChildPath "windapsearch-windows-amd64.exe") `
#         -GitHubRelease -Pattern "*windows-amd64*"

#Get-File -Url "https://www.sublimetext.com/download_thanks?target=win-x64" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "sublime.exe") `
#         -Pattern "*.exe*" `
#         -Install -InstallArgs "/VERYSILENT /NORESTART"

#Get-File -Url "https://www.nirsoft.net/utils/hashmyfiles-x64.zip" `
#         -Destination (Join-Path -Path $kits_dir -ChildPath "hashmyfiles-x64.zip") `
#         -ExtractDestination (Join-Path -Path $tools_dir -ChildPath "hashmyfiles")

# RSAT
#Write-Host "Installing RSAT" 
#Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online | Out-Null
#
## compress _kits for potential distribution
#Write-Host "Compressing $kits_dir" 
#Compress-Archive -Path $kits_dir -DestinationPath (Join-Path -Path $tools_dir -ChildPath "_kits.zip")
#Remove-Item $kits_dir -Recurse -Force | Out-Null
#
## create shortcut to desktop
#New-Item -ItemType SymbolicLink -Path "$HOME\Desktop" -Name Tools -Value $tools_dir
#$s = New-Object -ComObject WScript.Shell
#$lnk = $s.CreateShortcut("$HOME\Desktop\Tools.lnk")
#$lnk.TargetPath = "C:\Tools"
#$lnk.Save()
