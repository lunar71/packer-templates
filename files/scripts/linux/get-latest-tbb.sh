#!/bin/bash
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null

latest_version=$(curl -sL https://www.torproject.org/download/ | grep -Eo '/.*tor-browser-linux-x86_64-[0-9.]+.tar.xz' | sort -u)
latest_version_sig=$(curl -sL https://www.torproject.org/download/ | grep -Eo '/.*tor-browser-linux-x86_64-[0-9.]+.tar.xz.asc' | sort -u)
printf "%s\n" "[INFO] found latest tor browser bundle version $(basename ${latest_version})"
latest_version_url="https://www.torproject.org${latest_version}"
latest_version_sig="https://www.torproject.org${latest_version_sig}"

mkdir -p ../../include &>/dev/null
if [[ ! $(\ls ../../include | grep -E $(basename "${latest_version}")) ]]; then
    printf "%s\n" "[INFO] attempting to download latest tor browser bundle from ${latest_version_url}"
    printf "%s\n" "[INFO] latest version of tor browser bundle not found in ../../include, downloading"
    cd ../../include
    rm -rf *.tar.xz *.tar.xz.asc
    printf "%s\n" "[INFO] removing any tor browser bundle old versions"
    if curl -sSLOJ $latest_version_url; then
        printf "%s\n" "[INFO] downloaded latest tor browser bundle"
        if curl -sSLOJ $latest_version_sig; then
            printf "%s\n" "[INFO] downloaded tor browser bundle signature"
        else
            printf "%s\n" "[ERR] failed to download tor browser bundle signature"
        fi
    else
        printf "%s\n" "[ERR] error occured while downloading latest tor browser bundle"
    fi
else
    printf "%s\n" "[INFO] found latest tor browser bundle version in ../../include, skipping download"
fi
