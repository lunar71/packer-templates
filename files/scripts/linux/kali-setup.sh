#!/bin/bash
# -*- coding: utf-8 -*-
#set -x
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

RUNNER="kali-setup.sh"
LOWPRIV_USER="${LOWPRIV_USER:-kali}"
export_dir="/opt/tools"

[[ $EUID -ne 0 ]] && printf "\t\t [${RUNNER}] %s\n" "run as root" && exit 255

if [[ $(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release | tr '[[:upper:]]' '[[:lower:]]') != *kali* ]]; then
    printf "\t\t [${RUNNER}] %s\n" "not a kali environment, exiting"
    exit 255
fi

case "${BUILD_TYPE}" in
    kali-linux-default)
        build_backages="${BUILD_TYPE}"
        ;;
    kali-linux-core)
        build_backages="${BUILD_TYPE}"
        ;;
    custom)
        build_packages="
            apt-transport-https apktool arp-scan arping atftp bettercap binwalk build-essential
            cadaver cewl cifs-utils curl dbeaver default-mysql-client-core dex2jar
            dislocker dnsutils enum4linux exploitdb fping file git gpg gpp-decrypt hashcat-utils
            hydra impacket-scripts iputils-ping jadx jd-gui john jq maskprocessor
            masscan msfpc netdiscover libnfs-utils nikto ncat nmap openssh-client
            p0f p7zip passing-the-hash patator powershell postgresql proxychains-ng
            proxytunnel python3-dev python3-pip python3-virtualenv python3-full python-is-python3
            rdesktop responder samba sendemail smbmap snmpcheck socat sqlmap sslscan
            stegcracker steghide swaks tcpdump tmux vim voiphopper webshells weevely
            whois wmi-client wordlists wpscan libnetfilter-queue-dev libusb-1.0-0-dev libpcap-dev
        "
        ;;
    *)
        printf "%s\n" "\t\t [${RUNNER}] unknown BUILD_TYPE, exiting"
        exit 255
        ;;
esac

case "${DESKTOP_VNC}" in
    kali-desktop-xfce)
        desktop_name="kali-desktop-xfce"
        desktop_packages="kali-desktop-xfce tigervnc-standalone-server dbus-x11"
        ;;
    openbox)
        desktop_name="openbox"
        desktop_packages="openbox tigervnc-standalone-server dbus-x11 vim obconf x11-xserver-utils rxvt-unicode tint2"
        ;;
esac

printf "\t\t [${RUNNER}] %s\n" "installing sudo adduser passwd"
DEBIAN_FRONTEND=noninteractive apt-get update -yqq >/dev/null
DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq >/dev/null
DEBIAN_FRONTEND=noninteractive apt-get install -y sudo adduser passwd
DEBIAN_FRONTEND=noninteractive apt-get clean -yqq >/dev/null
DEBIAN_FRONTEND=noninteractive apt-get autoclean -yqq >/dev/null
DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -yqq >/dev/null

if ! id "${LOWPRIV_USER}" &>/dev/null; then
    printf "\t\t [${RUNNER}] %s\n" "Creating low privileged user ${LOWPRIV_USER}"
    adduser --gecos '' --disabled-password "${LOWPRIV_USER}" &>/dev/null
    echo "${LOWPRIV_USER}:${LOWPRIV_USER}" | chpasswd 
    usermod -aG sudo "${LOWPRIV_USER}"
    echo ""${LOWPRIV_USER}" ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/90-kali-user
    chmod 440 /etc/sudoers.d/90-kali-user
    chsh -s /bin/bash "${LOWPRIV_USER}"
fi

chsh -s /bin/bash root

if [[ "${DESKTOP_VNC}" ]]; then
    printf "\t\t [${RUNNER}] %s\n" "DESKTOP_VNC value set, building kali vnc environment with ${desktop_name}"

    DEBIAN_FRONTEND=noninteractive apt-get update -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get install -y $desktop_packages
    DEBIAN_FRONTEND=noninteractive apt-get clean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoclean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -yqq >/dev/null
    cat > "/opt/start-desktop" << EOF
#!/bin/bash
vncserver -depth 32 -geometry 1280x1024 -localhost yes -securitytypes none -cleanstale -rfbport 31337 -fg
EOF
    chmod +x "/opt/start-desktop"

    if [[ "${DESKTOP_VNC}" = "openbox" ]]; then
        sed -i 's/<name>Clearlooks<\/name>/<name>Nightmare<\/name>/g' /etc/xdg/openbox/rc.xml
        sed -i 's/<name>sans<\/name>/<name>Monospace<\/name>/g' /etc/xdg/openbox/rc.xml
        sed -i 's/<weight>.*<\/weight>/<weight>normal<\/weight>/g' /etc/xdg/openbox/rc.xml
        sed -i 's/<slant>.*<\/weight>/<slant>normal<\/slant>/g' /etc/xdg/openbox/rc.xml
        sed -i 's/panel_items.*/panel_items = LTS/g' /etc/xdg/tint2/tint2rc
        sed -i 's/background_color = #000000.*/background_color = #303030 60/g' /etc/xdg/tint2/tint2rc

        echo "tint2 &" >> /etc/xdg/openbox/autostart

        mkdir -p /etc/X11/Xresources &>/dev/null
        cat >> "/etc/X11/Xresources/x11-common" << EOF
URxvt.font:xft:Inconsolata:style=Medium:size=10
URxvt.scrollBar: false
*.foreground:   #c5c8c6
*.background:   #1d1f21
*.cursorColor:  #c5c8c6
*.color0:       #1d1f21
*.color8:       #969896
*.color1:       #cc342b
*.color9:       #cc342b
*.color2:       #198844
*.color10:      #198844
*.color3:       #fba922
*.color11:      #fba922
*.color4:       #3971ed
*.color12:      #3971ed
*.color5:       #a36ac7
*.color13:      #a36ac7
*.color6:       #3971ed
*.color14:      #3971ed
*.color7:       #c5c8c6
*.color15:      #ffffff
EOF

        cat > /etc/xdg/openbox/menu.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<openbox_menu xmlns="http://openbox.org/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://openbox.org/
                file:///usr/share/openbox/menu.xsd">
<menu id="root-menu" label="Openbox 3">
  <item label="Terminal emulator">
    <action name="Execute"><execute>rxvt-unicode</execute></action>
  </item>
  <item label="Web browser">
    <action name="Execute"><execute>x-www-browser</execute></action>
  </item>
  <separator />
  <item label="ObConf">
    <action name="Execute"><execute>obconf</execute></action>
  </item>
  <item label="Reconfigure">
    <action name="Reconfigure" />
  </item>
  <item label="Restart">
    <action name="Restart" />
  </item>
  <separator />
  <item label="Exit">
    <action name="Exit" />
  </item>
</menu>
</openbox_menu>
EOF
    fi
fi

if [[ "${BUILD_TYPE}" = "custom" ]]; then
    printf "\t\t [${RUNNER}] %s\n" "BUILD_TYPE value set to ${BUILD_TYPE}"

    if [[ ! -f $(pwd)/tools.sh ]]; then
        printf "\t\t [${RUNNER}] %s\n" "tools.sh that contains repositories and tool definitions not found"
        printf "\t\t [${RUNNER}] %s\n" "run the generate-tools-array.sh script"
        exit 255
    else
        source $(pwd)/tools.sh
    fi

    printf "\t\t [${RUNNER}] %s\n" "installing custom packages"
    DEBIAN_FRONTEND=noninteractive apt-get update -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get install -y $build_packages
    DEBIAN_FRONTEND=noninteractive apt-get clean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoclean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -yqq >/dev/null

    printf "\t\t [${RUNNER}] %s\n" "installing google chrome"
    curl -sSL https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor --output /etc/apt/trusted.gpg.d/google-signing-key.gpg
    echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/google-signing-key.gpg] https://dl.google.com/linux/chrome/deb stable main" > /etc/apt/sources.list.d/google-chrome.list
    DEBIAN_FRONTEND=noninteractive apt-get update -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get install -yqq google-chrome-stable

    printf "\t\t [${RUNNER}] %s\n" "installing golang"
    curl -sSLo /tmp/go_installer https://get.golang.org/$(uname) && \
        chmod +x /tmp/go_installer && \
        /tmp/go_installer && \
        rm -rf /tmp/go_installer

    rm -rf /usr/local/go
    mkdir -p /usr/local/go
    mv /root/.go/* /usr/local/go
    rm -rf /root/.go
    export PATH=$PATH:/usr/local/go/bin

    printf "\t\t [${RUNNER}] %s\n" "installing golang tools"
    for i in $(printf "$golang_tools" | sort -u); do
        printf "\t\t [${RUNNER}] %s\n" "Installing ${i}"
        GOBIN=/usr/local/bin/ /usr/local/go/bin/go install -trimpath $i
    done

    printf "\t\t [${RUNNER}] %s\n" "installing python tools"
    sudo -i -u "${LOWPRIV_USER}" /bin/bash << EOF
    rm -rf ~/py3venv
    virtualenv -p python3 ~/py3venv
    for i in $(printf "$python_tools" | sort -u); do printf "%s\n" "Installing \${i}" && ~/py3venv/bin/pip install \$i; done
EOF

    printf "\t\t [${RUNNER}] %s\n" "installing custom repos and tools"

    for dir in "${!tools[@]}"; do
        mkdir -p "${export_dir}/${dir}"
    done

    for i in "${!tools[@]}"; do 
        for repos in "${tools[$i]}"; do
            for each in $(printf "$repos" | sort -u); do
                if [[ $(curl -o /dev/null -s -w "%{http_code}" "${each}") != "404" ]]; then
                    printf "\t\t [${RUNNER}] %s\n" "Downloading ${each} to ${export_dir}/${i}"
                    git -C "${export_dir}/${i}" clone $each -q || wget -q $each -P "${export_dir}/${i}"
                fi
            done
        done
    done

    printf "\t\t [${RUNNER}] %s\n" "downloading latest burp suite pro & community"
    (cd "${export_dir}"; curl -sSLOJ 'https://portswigger-cdn.net/burp/releases/download?product=pro&version=&type=jar'; cd -)
    (cd "${export_dir}"; curl -sSLOJ 'https://portswigger-cdn.net/burp/releases/download?product=community&version=&type=jar'; cd -)

    printf "\t\t [${RUNNER}] %s\n" "downloading latest chromium snapshot"
    rev=$(curl -sSL 'https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2FLAST_CHANGE?alt=media')
    zip="https://storage.googleapis.com/chromium-browser-snapshots/Linux_x64/${rev}/chrome-linux.zip"
    curl -sSL $zip -o "${export_dir}/chromium-linux.zip"

    cat > /root/update-tools.sh << EOF
#!/bin/bash

trap 'git config --global --unset safe.directory' EXIT
[[ -z \$(git config --global safe.directory) ]] && git config --global --add safe.directory '*'

for i in \$(find ${export_dir} -name ".git" -type d | sed 's/\/.git//g'); do
    (cd \$i; git pull)
done

chown -R "${LOWPRIV_USER}:${LOWPRIV_USER}" ${export_dir}
EOF
    chmod +x /root/update-tools.sh

    cat > /root/update-golang-tools.sh << EOF
#!/bin/bash

golang_tools="$golang_tools"
for i in \$(printf "\$golang_tools" | sort -u); do printf "%s\n" "Installing \${i}" && GOBIN=/usr/local/bin/ /usr/local/go/bin/go install -trimpath \$i; done
EOF
    chmod +x /root/update-golang-tools.sh
    chown -R "${LOWPRIV_USER}:${LOWPRIV_USER}" "${export_dir}"
    sudo -i -u "${LOWPRIV_USER}" /bin/bash -c "ln -s ${export_dir} \"/home/${LOWPRIV_USER}/tools\""
else
    printf "\t\t [${RUNNER}] %s\n" "BUILD_TYPE value set to ${BUILD_TYPE}"

    DEBIAN_FRONTEND=noninteractive apt-get update -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get install -y $build_packages
    DEBIAN_FRONTEND=noninteractive apt-get clean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoclean -yqq >/dev/null
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -yqq >/dev/null
fi

if [[ -f /.dockerenv ]]; then
    ln -s /dev/null "/root/.bash_history"
    sudo -i -u "${LOWPRIV_USER}" /bin/bash -c "ln -s /dev/null \"/home/${LOWPRIV_USER}/.bash_history\""
fi

mkdir -p /root/.vim/colors
mkdir -p "/home/${LOWPRIV_USER}/.BurpSuite"
mkdir -p "/home/${LOWPRIV_USER}/.vim/colors"

touch "/root/.hushlogin" 
echo $(date -u --rfc-3339=date) > /root/BUILD.txt
