#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

if getent passwd jenkins &>/dev/null; then
    printf "%s\n" "[INFO] detected jenkins user"
    if getent group kvm &>/dev/null; then
        printf "%s\n" "[INFO] detected kvm group"
        usermod -aG kvm jenkins
        printf "%s\n" "[INFO] added jenkins user to kvm group"
    fi
fi

command -v qemu-system-x86_64 &>/dev/null && printf "%s\n" "[INFO] qemu already installed" && exit 0

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_SUSPEND=1

apt-get update -y
apt-get install -y qemu-system-x86
