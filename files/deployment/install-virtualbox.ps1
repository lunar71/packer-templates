#Requires -RunAsAdministrator
$ProgressPreference = 'SilentlyContinue'

$latestStableUrl = "https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT"
$latestStableVersion = (Invoke-RestMethod -Uri $latestStableUrl -UseBasicParsing).Trim()

$downloadPageUrl = "https://download.virtualbox.org/virtualbox/${latestStableVersion}"
$downloadPageContent = Invoke-WebRequest -Uri $downloadPageUrl -UseBasicParsing
$windowsInstallerUrl = ($downloadPageContent.Links | Where-Object { $_.href -match ".*VirtualBox-.*-Win.exe" }).href
$windowsInstallerUrl = "${downloadPageUrl}/${windowsInstallerUrl}"
$outputInstallerPath = "$env:USERPROFILE\Downloads\VirtualBox-${latestStableVersion}-Win.exe"

Write-Host "[INFO] Downloading VirtualBox ${latestStableVersion} installer"
Invoke-WebRequest -Uri $windowsInstallerUrl -OutFile $outputInstallerPath

Write-Host "[INFO] Installing VirtualBox ${latestStableVersion}"
Start-Process -FilePath $outputInstallerPath -ArgumentList "--silent" -Wait

$extensionPackName = ($downloadPageContent.Links | Where-Object { $_.href -match ".*${latestStableVersion}.*.vbox-extpack" }).href | Select-Object -First 1
$extensionPackUrl = "${downloadPageUrl}/${extensionPackName}"
$outputExtensionPackPath = "$env:USERPROFILE\Downloads\${extensionPackName}"

Write-Host "[INFO] Downloading VirtualBox Extension Pack ${latestStableVersion}"
Invoke-WebRequest -Uri $extensionPackUrl -OutFile $outputExtensionPackPath

Write-Host "[INFO] Installing VirtualBox Extension Pack ${latestStableVersion}"
echo y | & "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" extpack install --replace $outputExtensionPackPath

Write-Host "[INFO] VirtualBox and Extension Pack ${latestStableVersion} installation completed"

Remove-Item -Path "${outputInstallerPath}"
Remove-Item -Path "${outputExtensionPackPath}"
