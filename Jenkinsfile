pipeline {
    agent { label "${params.agent_label}" }

    parameters {
        string(
            name: 'agent_label',
            defaultValue: '',
            trim: true,
            description: 'jenkins agent to run on'
        )
        booleanParam(
            name: 'kali_default_offline_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-default-offline-kde.json'
        )
        booleanParam(
            name: 'kali_default_offline_luks_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-default-offline-luks-kde.json'
        )
        booleanParam(
            name: 'kali_default_offline_luks_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-default-offline-luks-xfce.json'
        )
        booleanParam(
            name: 'kali_default_offline_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-default-offline-xfce.json'
        )
        booleanParam(
            name: 'kali_everything_offline_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-everything-offline-kde.json'
        )
        booleanParam(
            name: 'kali_everything_offline_luks_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-everything-offline-luks-kde.json'
        )
        booleanParam(
            name: 'kali_everything_offline_luks_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-everything-offline-luks-xfce.json'
        )
        booleanParam(
            name: 'kali_everything_offline_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-everything-offline-xfce.json'
        )
        booleanParam(
            name: 'kali_top10_offline_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-top10-offline-kde.json'
        )
        booleanParam(
            name: 'kali_top10_offline_luks_kde_json',
            defaultValue: false,
            description: 'packer template name: kali-top10-offline-luks-kde.json'
        )
        booleanParam(
            name: 'kali_top10_offline_luks_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-top10-offline-luks-xfce.json'
        )
        booleanParam(
            name: 'kali_top10_offline_xfce_json',
            defaultValue: false,
            description: 'packer template name: kali-top10-offline-xfce.json'
        )
        booleanParam(
            name: 'ubuntu_10_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-10.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_12_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-12.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_14_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-14.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_16_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-16.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_18_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-18.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_20_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-20.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_22_04_gnome_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-22.04-gnome.json'
        )
        booleanParam(
            name: 'ubuntu_22_04_kubuntu_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-22.04-kubuntu.json'
        )
        booleanParam(
            name: 'ubuntu_22_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-22.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_22_04_xubuntu_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-22.04-xubuntu.json'
        )
        booleanParam(
            name: 'ubuntu_24_04_gnome_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-24.04-gnome.json'
        )
        booleanParam(
            name: 'ubuntu_24_04_kubuntu_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-24.04-kubuntu.json'
        )
        booleanParam(
            name: 'ubuntu_24_04_server_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-24.04-server.json'
        )
        booleanParam(
            name: 'ubuntu_24_04_xubuntu_json',
            defaultValue: false,
            description: 'packer template name: ubuntu-24.04-xubuntu.json'
        )
        booleanParam(
            name: 'win10_22h2_pro_clean_json',
            defaultValue: false,
            description: 'packer template name: win10-22h2-pro-clean.json'
        )
        booleanParam(
            name: 'win10_22h2_pro_developer_json',
            defaultValue: false,
            description: 'packer template name: win10-22h2-pro-developer.json'
        )
        booleanParam(
            name: 'win10_22h2_pro_office365_json',
            defaultValue: false,
            description: 'packer template name: win10-22h2-pro-office365.json'
        )
        booleanParam(
            name: 'win11_iot_enterprise_ltsc_clean_json',
            defaultValue: false,
            description: 'packer template name: win11-iot-enterprise-ltsc-clean.json'
        )
        booleanParam(
            name: 'win11_iot_enterprise_ltsc_pentest_json',
            defaultValue: false,
            description: 'packer template name: win11-iot-enterprise-ltsc-pentest.json'
        )
        booleanParam(
            name: 'win7_pro_sp1_json',
            defaultValue: false,
            description: 'packer template name: win7-pro-sp1.json'
        )
        booleanParam(
            name: 'winserver2008_clean_json',
            defaultValue: false,
            description: 'packer template name: winserver2008-clean.json'
        )
        booleanParam(
            name: 'winserver2012_clean_json',
            defaultValue: false,
            description: 'packer template name: winserver2012-clean.json'
        )
        booleanParam(
            name: 'winserver2016_clean_json',
            defaultValue: false,
            description: 'packer template name: winserver2016-clean.json'
        )
        booleanParam(
            name: 'winserver2019_clean_json',
            defaultValue: false,
            description: 'packer template name: winserver2019-clean.json'
        )
        booleanParam(
            name: 'winserver2022_clean_json',
            defaultValue: false,
            description: 'packer template name: winserver2022-clean.json'
        )
        choice(
            name: 'virt',
            choices: ['virtualbox', 'vmware', 'qemu'],
            description: 'virtualization engine to use for resulting vm'
        )
        string(
            name: 'boot_wait',
            defaultValue: '',
            trim: true,
            description: 'override template boot_wait variable; must end in "s"'
        )
        string(
            name: 'iso_url',
            defaultValue: '',
            trim: true,
            description: 'path to network hosted iso files e.g., http://nas.local.lan/iso'
        )
        text(
            name: 'download_urls',
            defaultValue: '',
            description: 'list of URLs (one per line) to download before building the template'
        )
        string(
            name: 'output',
            defaultValue: 'output',
            trim: true,
            description: 'custom output directory for built vms'
        )
        booleanParam(
            name: 'prebuild',
            defaultValue: false,
            description: 'download tools before executing packer build'
        )
    }

    stages {
        stage('Check and install prerequisites') {
            steps {
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-packer.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-virtualbox.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-qemu.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-docker.sh"'
                sh '/bin/bash files/deployment/install-packer-plugins.sh'
            }
        }

        stage('Download Files') {
            steps {
                script {
                    def includeDir = "${env.WORKSPACE}/files/include"
                    sh "mkdir -p ${includeDir} &>/dev/null"

                    def urls = []
                    if (params.download_urls?.trim()) {
                        urls = params.download_urls.split(/[,\n\r]+/).collect { it.trim() }.findAll { it }
                        for (url in urls) {
                            def filename = url.tokenize('/')[-1]

                            sh "rm -rf ${includeDir}/${filename}"
                            sh "curl -sSkL ${url} -o ${includeDir}/${filename}"
                        }
                    }
                }
            }
        }

        stage('Build template') {
            steps {
                script {
                    def selectedTemplates = []
                    if (params.kali_default_offline_kde_json) {
                        selectedTemplates << "kali-default-offline-kde.json"
                    }
                    if (params.kali_default_offline_luks_kde_json) {
                        selectedTemplates << "kali-default-offline-luks-kde.json"
                    }
                    if (params.kali_default_offline_luks_xfce_json) {
                        selectedTemplates << "kali-default-offline-luks-xfce.json"
                    }
                    if (params.kali_default_offline_xfce_json) {
                        selectedTemplates << "kali-default-offline-xfce.json"
                    }
                    if (params.kali_everything_offline_kde_json) {
                        selectedTemplates << "kali-everything-offline-kde.json"
                    }
                    if (params.kali_everything_offline_luks_kde_json) {
                        selectedTemplates << "kali-everything-offline-luks-kde.json"
                    }
                    if (params.kali_everything_offline_luks_xfce_json) {
                        selectedTemplates << "kali-everything-offline-luks-xfce.json"
                    }
                    if (params.kali_everything_offline_xfce_json) {
                        selectedTemplates << "kali-everything-offline-xfce.json"
                    }
                    if (params.kali_top10_offline_kde_json) {
                        selectedTemplates << "kali-top10-offline-kde.json"
                    }
                    if (params.kali_top10_offline_luks_kde_json) {
                        selectedTemplates << "kali-top10-offline-luks-kde.json"
                    }
                    if (params.kali_top10_offline_luks_xfce_json) {
                        selectedTemplates << "kali-top10-offline-luks-xfce.json"
                    }
                    if (params.kali_top10_offline_xfce_json) {
                        selectedTemplates << "kali-top10-offline-xfce.json"
                    }
                    if (params.ubuntu_10_04_server_json) {
                        selectedTemplates << "ubuntu-10.04-server.json"
                    }
                    if (params.ubuntu_12_04_server_json) {
                        selectedTemplates << "ubuntu-12.04-server.json"
                    }
                    if (params.ubuntu_14_04_server_json) {
                        selectedTemplates << "ubuntu-14.04-server.json"
                    }
                    if (params.ubuntu_16_04_server_json) {
                        selectedTemplates << "ubuntu-16.04-server.json"
                    }
                    if (params.ubuntu_18_04_server_json) {
                        selectedTemplates << "ubuntu-18.04-server.json"
                    }
                    if (params.ubuntu_20_04_server_json) {
                        selectedTemplates << "ubuntu-20.04-server.json"
                    }
                    if (params.ubuntu_22_04_gnome_json) {
                        selectedTemplates << "ubuntu-22.04-gnome.json"
                    }
                    if (params.ubuntu_22_04_kubuntu_json) {
                        selectedTemplates << "ubuntu-22.04-kubuntu.json"
                    }
                    if (params.ubuntu_22_04_server_json) {
                        selectedTemplates << "ubuntu-22.04-server.json"
                    }
                    if (params.ubuntu_22_04_xubuntu_json) {
                        selectedTemplates << "ubuntu-22.04-xubuntu.json"
                    }
                    if (params.ubuntu_24_04_gnome_json) {
                        selectedTemplates << "ubuntu-24.04-gnome.json"
                    }
                    if (params.ubuntu_24_04_kubuntu_json) {
                        selectedTemplates << "ubuntu-24.04-kubuntu.json"
                    }
                    if (params.ubuntu_24_04_server_json) {
                        selectedTemplates << "ubuntu-24.04-server.json"
                    }
                    if (params.ubuntu_24_04_xubuntu_json) {
                        selectedTemplates << "ubuntu-24.04-xubuntu.json"
                    }
                    if (params.win10_22h2_pro_clean_json) {
                        selectedTemplates << "win10-22h2-pro-clean.json"
                    }
                    if (params.win10_22h2_pro_developer_json) {
                        selectedTemplates << "win10-22h2-pro-developer.json"
                    }
                    if (params.win10_22h2_pro_office365_json) {
                        selectedTemplates << "win10-22h2-pro-office365.json"
                    }
                    if (params.win11_iot_enterprise_ltsc_clean_json) {
                        selectedTemplates << "win11-iot-enterprise-ltsc-clean.json"
                    }
                    if (params.win11_iot_enterprise_ltsc_pentest_json) {
                        selectedTemplates << "win11-iot-enterprise-ltsc-pentest.json"
                    }
                    if (params.win7_pro_sp1_json) {
                        selectedTemplates << "win7-pro-sp1.json"
                    }
                    if (params.winserver2008_clean_json) {
                        selectedTemplates << "winserver2008-clean.json"
                    }
                    if (params.winserver2012_clean_json) {
                        selectedTemplates << "winserver2012-clean.json"
                    }
                    if (params.winserver2016_clean_json) {
                        selectedTemplates << "winserver2016-clean.json"
                    }
                    if (params.winserver2019_clean_json) {
                        selectedTemplates << "winserver2019-clean.json"
                    }
                    if (params.winserver2022_clean_json) {
                        selectedTemplates << "winserver2022-clean.json"
                    }

                    if (selectedTemplates.isEmpty()) {
                        error "No template selected. Please choose at least one template."
                    }
                    def templateArgs = selectedTemplates.collect { "-t " + it }.join(" ")

                    def bootWaitFlag = params.boot_wait ? "-w ${params.boot_wait}" : ""
                    def preBuildFlag = params.prebuild ? "-x" : ""
                    def cmd = "/bin/bash build.sh -v ${params.virt} " + templateArgs + " -u ${params.iso_url} -H -j 1 -o ${params.output} " + preBuildFlag + " " + bootWaitFlag
                    sh cmd
                }
            }
        }
    }
}
