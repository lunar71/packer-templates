#!/bin/bash

zip="packer-template_release.zip"
rm -rf $zip
zip -q -r $zip . -x "*.gitgnore" "*.zip" "logs/*" ".cache/*" "iso/*.iso" "iso/*.lock" "ova/*.ova" "files/avs/*" "files/include/*" ".git/*" "_unsupported/*" "*.log" "vmware_output*" "virtualbox_output*"
[[ $? -eq 0 ]] && printf "%s\n" "created $zip" || printf "%s\n" "error creating $zip"
