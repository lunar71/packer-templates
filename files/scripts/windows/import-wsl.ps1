param (
    [Parameter(Mandatory=$true)][string]$WSLTar,
    [Parameter(Mandatory=$true)][string]$WSLName,
    [Parameter(Mandatory=$true)][string]$WSLUsername
)

if (Test-Path C:\Windows\System32\wsl.exe) {
    if (Test-Path -PathType Leaf $WSLTar) {
        Write-Host "[INFO] importing $WSLTar as $WSLName; depending on size, this might take a while..."
        C:\Windows\System32\wsl.exe --import "$WSLName" "C:\WSL\$WSLName" "$WSLTar"
        Write-Host "[INFO] imported $WSLTar as $WSLName"

        $ShortcutPath = "$env:USERPROFILE\Desktop\$WSLName.lnk"
        $TargetPath = "C:\Windows\System32\cmd.exe"
        $Arguments = "/k wsl.exe -d $WSLName --user $WSLUsername"
        $WorkingDirectory = "C:\"

        $WScriptShell = New-Object -ComObject WScript.Shell
        $Shortcut = $WScriptShell.CreateShortcut($ShortcutPath)

        $Shortcut.TargetPath = $TargetPath
        $Shortcut.Arguments = $Arguments
        $Shortcut.WorkingDirectory = $WorkingDirectory
        $Shortcut.IconLocation = "C:\Windows\System32\wsl.exe"

        $Shortcut.Save()
        Write-Host "[INFO] created $WSLName WSL shortcut at $ShortcutPath"
    } else {
        Write-Host "[ERR] $WSLTar is not a file"
    }
}
