#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

command -v vmware &>/dev/null && printf "%s\n" "[INFO] vmware already installed" && exit 0

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_SUSPEND=1

version_16=(
    ZF3R0-FHED2-M80TY-8QYGC-NPKYF
    YF390-0HF8P-M81RQ-2DXQE-M2UT6
    ZF71R-DMX85-08DQY-8YMNC-PPHV8
    AZ3E8-DCD8J-0842Z-N6NZE-XPKYF
    FC11K-00DE0-0800Z-04Z5E-MC8T6
)

version_17=(
    MC60H-DWHD5-H80U9-6V85M-8280D
    4A4RR-813DK-M81A9-4U35H-06KND
    NZ4RR-FTK5H-H81C1-Q30QH-1V2LA
    JU090-6039P-08409-8J0QH-2YR7F
    4Y09U-AJK97-089Z0-A3054-83KLA
    4C21U-2KK9Q-M8130-4V2QH-CF810
)

curl \
    -sSL \
    -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36." \
    https://www.vmware.com/go/getworkstation-linux -o /tmp/vmware-ws-pro.bundle

bash /tmp/vmware-ws-pro.bundle
rm /tmp/vmware-ws-pro.bundle

version=$(vmware -v | grep -Eo "([0-9]{1,}\.)+[0-9]{1,}")
case "${version}" in
    16*) random_key=${version_16[$RANDOM%${#version_16[@]}]}
         sudo /usr/lib/vmware/bin/licenseTool enter "${random_key}" "" "" "${version}" "VMware Workstation" /usr/lib/vmware
         ;;
    17*) random_key=${version_17[$RANDOM%${#version_17[@]}]}
         sudo /usr/lib/vmware/bin/licenseTool enter "${random_key}" "" "" "${version}" "VMware Workstation" /usr/lib/vmware
         ;;
esac

apt update -yqq
apt install -yqq libxi6 libxinerama1 libxcursor1 libxtst6 build-essential
