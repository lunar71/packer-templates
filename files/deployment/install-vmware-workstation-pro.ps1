#Requires -RunAsAdministrator

$ts = [datetime]::Now.ToUniversalTime().ToString("yyyy-MM-dd @ HH:mm:ss UTC")
#$license = "ZF3R0-FHED2-M80TY-8QYGC-NPKYF", "YF390-0HF8P-M81RQ-2DXQE-M2UT6", "ZF71R-DMX85-08DQY-8YMNC-PPHV8", "AZ3E8-DCD8J-0842Z-N6NZE-XPKYF", "FC11K-00DE0-0800Z-04Z5E-MC8T6" | Get-Random
$license = "MC60H-DWHD5-H80U9-6V85M-8280D", "4A4RR-813DK-M81A9-4U35H-06KND", "NZ4RR-FTK5H-H81C1-Q30QH-1V2LA", "JU090-6039P-08409-8J0QH-2YR7F", "4Y09U-AJK97-089Z0-A3054-83KLA", "4C21U-2KK9Q-M8130-4V2QH-CF810" | Get-Random

Write-Host "Downloading VMware Workstation Pro"
Invoke-WebRequest -Uri https://www.vmware.com/go/getworkstation-win -OutFile C:\Windows\Temp\vmware-ws-pro.exe

Write-Host "Installing VMware Workstation Pro"
Start-Process -FilePath "C:\Windows\Temp\vmware-ws-pro.exe" -ArgumentList "/s /v /qn EULAS_AGREED=1 AUTOSOFTWAREUPDATE=0 DATACOLLECTION=0 ADDLOCAL=ALL REBOOT=ReallySuppress" -Wait -Verbose
Remove-Item "C:\Windows\Temp\vmware-ws-pro.exe"

Write-Host "Licensing VMware Workstation Pro"
Start-Process -FilePath "C:\Program Files (x86)\VMware\VMware Workstation\x64\vmware-vmx.exe" -ArgumentList "--new-sn $license"

Write-Host "Finished installing and licensing VMware Workstation Pro"

#Write-Host "Licensing VMware Workstation Pro"
#$ws_edition = Get-ChildItem "HKLM:\SOFTWARE\WOW6432Node\VMware, Inc.\VMware Workstation\Dormant" | % { If((Get-ItemProperty -Path $_.PSPath -Name LicenseEdition | Select-Object -ExpandProperty LicenseEdition) -eq "ws.fusion.pro") { $_.PSPath } }
#$ws_edition_leaf = $ws_edition | Split-Path -Leaf
#Copy-Item -Path $ws_edition -Destination "HKLM:\SOFTWARE\WOW6432Node\VMware, Inc.\VMware Workstation" -Recurse
#New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\VMware, Inc.\VMware Workstation\$ws_edition_leaf" -Name Serial -Value "$license" -Type String | Out-Null
#New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\VMware, Inc.\VMware Workstation\$ws_edition_leaf" -Name LastModified -Value "$ts" -Type String | Out-Null
