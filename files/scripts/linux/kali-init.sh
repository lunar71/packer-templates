#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

LOWPRIV_USER="${LOWPRIV_USER:-kali}"
LOWPRIV_HOME=$(getent passwd $LOWPRIV_USER | awk -F ':' '{print $6}')

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1

mkdir -p "/etc/skel/.local/bin" /opt/tools/bin &>/dev/null

if [[ $PACKER_NAME == *top10* ]]; then
    title="Custom Kali Linux top10 build"

elif [[ $PACKER_NAME == *default* ]]; then
    title="Custom Kali Linux default build"

elif [[ $PACKER_NAME == *everything* ]]; then
    title="Custom Kali Linux everything build"
fi

cat > /etc/skel/.local/README.txt << EOF
${title}

Additions:
    - extra tools in /opt/tools
    - bash default shell
    - resize LVM to add free/unallocated space with "extend-lvm"
    - extra path with \$HOME/go/bin, \$HOME/.local/bin, /opt/tools/bin
    - ipv6 disabled
    - NetworkManager interfaces autoconnect disabled
    - startup/shutdown cleanup service in /etc/systemd/services/cleanup.service, /etc/systemd/system/cleanup.sh
    - mac and hostname spoofing service in /etc/systemd/services/hostspoof@.service, /etc/systemd/system/hostspoof.sh; activate the hostspoof@<interface> service
    - disabled services: apt-daily, apt-daily-upgrade, chkrootkit, fwupd, rsync, smartmontools, packagekit, lynis
    - use /opt/tools/customize-kali.sh for extra customizations
EOF

if [[ $PACKER_BUILDER_TYPE == vmware-iso ]]; then
    cat >> /etc/skel/.local/README.txt << EOF
    - vmware shared folder located in /mnt/hgfs
EOF
fi

if [[ $PACKER_BUILDER_TYPE == virtualbox-iso ]]; then
    cat >> /etc/skel/.local/README.txt << EOF
    - virtualbox shared folder located in /media
EOF
fi

if [[ $PACKER_NAME == *luks* ]]; then
    cat > /usr/bin/change-luks-passphrase << "EOF"
#!/bin/bash
test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1
printf "%s\n" "WARNING: this will overwrite the supplied LUKS passphrase keyslot with a new one"
printf "%s\n\n" "         follow the on screen instructions and input old and new passphrases"
cryptsetup luksChangeKey $(lsblk -o NAME,FSTYPE -l | awk '$2=="crypto_LUKS" {print "/dev/"$1}')
EOF
    chmod +x /usr/bin/change-luks-passphrase

    cat > /usr/bin/nuke << "EOF"
#!/bin/bash
printf "%s\n" "WARNING: this will erase all LUKS keyslots"
printf "%s\n\n" "         system will become inaccessible after this operation"
for i in {1..3}; do
  read -p "[${i}/3] press ENTER to confirm"
done
sudo cryptsetup luksErase $(lsblk -o NAME,FSTYPE -l | awk '$2=="crypto_LUKS" {print "/dev/"$1}') -q
poweroff
EOF
    chmod +x /usr/bin/nuke
    chmod 755 /usr/bin/nuke

    cat >> /etc/sudoers << EOF

# allow all users to execute $(which cryptsetup) luksErase as root without a password prompt
ALL ALL=(ALL) NOPASSWD: $(which cryptsetup) luksErase *
EOF

    cat >> /etc/skel/.local/README.txt << EOF
    - luks encrypted, change password with "change-luks-passphrase"
    - grub protected, credentials in /root/grub_password.txt
    - nuke luks keyslots with "nuke" or "sudo nuke"
EOF
fi

printf "%s\n" "[INFO] created README.txt in /etc/skel/.local/README.txt"

cat > /etc/xdg/user-dirs.defaults << EOF
DESKTOP=Desktop
DOWNLOAD=Downloads
DOCUMENTS=Documents
PICTURES=Pictures
PROJECTS=Projects
EOF
printf "%s\n" "[INFO] set custom xdg dirs in /etc/xdg/user-dirs.defaults"

sed -i 's/SHELL=.*/SHELL=\/bin\/bash/g' /etc/default/useradd

chsh -s /bin/bash root
xdg-user-dirs-update
cp -rf /etc/skel/. /root

chsh -s /bin/bash $LOWPRIV_USER
sudo -i -u $LOWPRIV_USER /bin/bash -c "xdg-user-dirs-update"
cp -rf /etc/skel/. $LOWPRIV_HOME

printf "%s\n" \
    "[INFO] set /bin/bash default shell" \
    "[INFO] updated \$HOME with /etc/skel"

cat > "/usr/local/sbin/adduser.local" << "EOF"
#!/bin/bash
ln -sfn $4/.local/README.txt $4/Desktop/README.txt
EOF
chmod +x "/usr/local/sbin/adduser.local"
printf "%s\n" "[INFO] created /usr/local/sbin/adduser.local"

ln -sfn /root/.local/README.txt /root/Desktop/README.txt
ln -sfn "/home/${LOWPRIV_USER}/.local/README.txt" "/home/${LOWPRIV_USER}/Desktop/README.txt"

if [[ $(command -v docker) ]]; then
    if [[ $(getent group docker) ]]; then
        usermod -aG docker $LOWPRIV_USER
        printf "%s\n" "[INFO] added ${LOWPRIV_USER} to docker group"
    fi
fi

sed -i '/^AcceptEnv LANG LC_\*/s/^/#/' /etc/ssh/sshd_config
printf "%s\n" "[INFO] disabled ssh AcceptEnv in /etc/ssh/sshd_config"

sed -i 's|^PATH="\(.*\)"|PATH="\1:/opt/tools/bin"|' /etc/environment
printf "%s\n" "[INFO] appended /opt/tools/bin to /etc/environment"

sed -i '/swap/s/^/#/' /etc/fstab
printf "%s\n" "ulimit -S -c 0" >> /etc/profile
printf "%s\n" "[INFO] disabled swap"

printf "%s\n" \
    "fs.suid_dumpable=0" \
    "kernel.core_pattern=/dev/null" >> /etc/sysctl.conf
printf "%s\n" "[INFO] set up custom parameters in /etc/sysctl.conf"

printf "%s\n" \
    "* hard core 0" \
    "* soft core 0" >> /etc/security/limits.conf
printf "%s\n" "[INFO] set up custom limits in /etc/security.limits.conf"

sed -i '/GRUB_CMDLINE_LINUX=/ s/\"$/ ipv6.disable=1\"/; s/\" /\"/' /etc/default/grub && update-grub &>/dev/null
printf "%s\n" "[INFO] disabled ipv6 with boot parameter in /etc/default/grub"

if test -f /usr/lib/kali_tweaks/data/mount-shared-folders; then
    install -v -D -m0755 /usr/lib/kali_tweaks/data/mount-shared-folders /usr/local/sbin/mount-shared-folders &>/dev/null
    printf "%s\n" "[INFO] added kali tweak /usr/local/sbin/mount-shared-folders"
fi

if test -f /usr/lib/kali_tweaks/data/restart-vm-tools; then
    install -v -D -m0755 /usr/lib/kali_tweaks/data/restart-vm-tools /usr/local/sbin/restart-vm-tools &>/dev/null
    printf "%s\n" "[INFO] added kali tweak /usr/local/sbin/restart-vm-tools"
fi

services_to_disable="apt-daily.service apt-daily.timer apt-daily-upgrade.service apt-daily-upgrade.timer chkrootkit.service chkrootkit.timer fwupd-refresh.service fwupd-refresh.timer fwupd.service fwupd-offline-update.service rsync.service smartmontools.service packagekit.service packagekit-offline-update.service lynis.service"

set +e
systemctl disable --now -q $services_to_disable &>/dev/null
printf "%s\n" "[INFO] disabled ${services_to_disable}"

systemctl mask -q $services_to_disable &>/dev/null
printf "%s\n" "[INFO] masked ${services_to_disable}"
set -e

systemctl daemon-reload
systemctl enable --now -q cleanup.service

apt-get clean
apt-get autoclean
