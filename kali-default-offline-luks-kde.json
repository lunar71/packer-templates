{
  "variables": {
    "iso_name": "kali-linux-2024.4-installer-everything-amd64.iso",
    "iso_path": "{{pwd}}/iso/{{user `iso_name`}}",
    "iso_sha256": "sha256:4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2",
    "timestamp": "{{isotime `2006-01-02`}}",
    "headless": "false",
    "boot_wait": "5s",
    "luks_wait": "60m",

    "local_iso_url": "", 
    "local_iso": "{{user `local_iso_url`}}/{{user `iso_name`}}",
    "output": "",
        
    "name": "kali-2024.4-default-offline-luks-kde",
    "answer_file": "kali-preseed-default-offline-luks-kde.cfg",
    "luks_default_password": "kali",
    "cpu": "4",
    "cores": "2",
    "ram": "4096",
    "disk_size": "204800",
    "virtual_disk_dev": "lsisas1068",
    "virtual_version": "17",
    "virtual_firmware": "efi",
    "vmware_guest_os_type": "debian10-64",
    "virtualbox_guest_os_type": "Debian_64",
    "communicator_username": "root",
    "communicator_password": "root",
    "lowpriv_user": "kali",

    "eula": "Custom Kali Linux default build\n\nBuild time: {{user `timestamp`}}\nDefault credentials: {{user `lowpriv_user`}}:{{user `lowpriv_user`}}, {{user `communicator_username`}}:{{user `communicator_password`}}\nDefault LUKS password: {{user `luks_default_password`}}\n\nCheck ~/Desktop/README.txt or /root/README.txt"
  },

  "builders": [
    {
      "type": "vmware-iso",
      "format": "ova",
      "headless": "{{user `headless`}}",
      "name": "vmware",
      "vm_name": "vmware-{{user `name`}}_{{user `timestamp`}}",
      "cpus": "{{user `cpu`}}",
      "cores": "{{user `cores`}}",
      "memory": "{{user `ram`}}",
      "disk_size": "{{user `disk_size`}}",
      "disk_type_id": 0,
      "sound": false,
      "guest_os_type": "{{user `vmware_guest_os_type`}}",
      "communicator": "ssh",
      "ssh_username": "{{user `communicator_username`}}",
      "ssh_password": "{{user `communicator_password`}}",
      "ssh_pty": true,
      "ssh_timeout": "120m",
      "ssh_handshake_attempts": "1337",
      "shutdown_command": "shutdown -hP now",
      "boot_wait": "{{user `boot_wait`}}",
      "boot_command": [
        "<esc><wait>",
        "install preseed/url=http://{{.HTTPIP}}:{{.HTTPPort}}/{{user `answer_file`}} ",
        "locale=en_US.UTF-8 debian-installer=en_US.UTF-8 ",
        "ipv6.disable=1 fb=false auto ",
        "kbd-chooser/method=us keyboard-configuration/xkb-keymap=us console-keymaps-at/keymap=us ",
        "netcfg/get_hostname=localhost netcfg/get_domain=unassigned-domain debconf/frontend=noninteractive console-setup/ask_detect=false ",
        "<enter><wait{{user `luks_wait`}}>{{user `luks_default_password`}}<enter>"
      ],
      "iso_urls": [
        "{{user `local_iso`}}",
        "{{user `iso_path`}}"
      ],
      "ovftool_options": ["--eula={{user `eula`}}"],
      "iso_checksum": "{{user `iso_sha256`}}",
      "iso_target_path": "{{user `iso_path`}}",
      "http_directory": "files/answer_files/linux/kali",
      "output_directory": "vmware_output_{{user `name`}}",
      "vmx_data": {
        "scsi0.virtualDev": "{{user `virtual_disk_dev`}}",
        "virtualHW.version": "{{user `virtual_version`}}"
      }
    },
    {
      "type": "virtualbox-iso",
      "format": "ova",
      "headless": "{{user `headless`}}",
      "name": "virtualbox",
      "vm_name": "virtualbox-{{user `name`}}_{{user `timestamp`}}",
      "disk_size": "{{user `disk_size`}}",
      "guest_os_type": "{{user `virtualbox_guest_os_type`}}",
      "communicator": "ssh",
      "ssh_username": "{{user `communicator_username`}}",
      "ssh_password": "{{user `communicator_password`}}",
      "ssh_pty": true,
      "ssh_timeout": "120m",
      "ssh_handshake_attempts": "1337",
      "shutdown_command": "echo {{user `lowpriv_user`}} | sudo -S shutdown -hP now",
      "boot_wait": "{{user `boot_wait`}}",
      "boot_command": [
        "<esc><wait>",
        "install preseed/url=http://{{.HTTPIP}}:{{.HTTPPort}}/{{user `answer_file`}} ",
        "locale=en_US.UTF-8 debian-installer=en_US.UTF-8 ",
        "ipv6.disable=1 fb=false auto ",
        "kbd-chooser/method=us keyboard-configuration/xkb-keymap=us console-keymaps-at/keymap=us ",
        "netcfg/get_hostname=localhost netcfg/get_domain=unassigned-domain debconf/frontend=noninteractive console-setup/ask_detect=false ",
        "<enter><wait{{user `luks_wait`}}>{{user `luks_default_password`}}<enter>"
      ],
      "iso_urls": [
        "{{user `local_iso`}}",
        "{{user `iso_path`}}"
      ],
      "iso_checksum": "{{user `iso_sha256`}}",
      "iso_target_path": "{{user `iso_path`}}",
      "http_directory": "files/answer_files/linux/kali",
      "output_directory": "virtualbox_output_{{user `name`}}",
      "guest_additions_mode": "disable",
      "vboxmanage": [
        ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
        ["modifyvm", "{{.Name}}", "--memory", "{{user `ram`}}"],
        ["modifyvm", "{{.Name}}", "--cpus", "{{user `cpu`}}"],
        ["modifyvm", "{{.Name}}", "--vram", "128"],
        ["modifyvm", "{{.Name}}", "--audio-driver", "none"],
        ["modifyvm", "{{.Name}}", "--graphicscontroller", "vmsvga"]
      ],
      "export_opts": [
        "--manifest",
        "--vsys", "0",
        "--eula", "{{user `eula`}}"
      ]
    },
    {
      "type": "qemu",
      "format": "qcow2",
      "accelerator": "kvm",
      "net_device": "virtio-net",
      "disk_interface": "virtio",
      "headless": "{{user `headless`}}",
      "name": "qemu",
      "vm_name": "qemu-{{user `name`}}_{{user `timestamp`}}",
      "disk_size": "{{user `disk_size`}}",
      "disk_cache": "none",
      "disk_discard": "unmap",
      "disk_compression": true,
      "communicator": "ssh",
      "ssh_username": "{{user `communicator_username`}}",
      "ssh_password": "{{user `communicator_password`}}",
      "ssh_pty": true,
      "ssh_timeout": "60m",
      "ssh_handshake_attempts": "1337",
      "shutdown_command": "echo {{user `lowpriv_user`}} | sudo -S shutdown -hP now",
      "boot_wait": "{{user `boot_wait`}}",
      "boot_command": [
        "<esc><wait>",
        "install preseed/url=http://{{.HTTPIP}}:{{.HTTPPort}}/{{user `answer_file`}} ",
        "locale=en_US.UTF-8 debian-installer=en_US.UTF-8 ",
        "ipv6.disable=1 fb=false auto ",
        "kbd-chooser/method=us keyboard-configuration/xkb-keymap=us console-keymaps-at/keymap=us ",
        "netcfg/get_hostname=localhost netcfg/get_domain=unassigned-domain debconf/frontend=noninteractive console-setup/ask_detect=false ",
        "<enter><wait{{user `luks_wait`}}>{{user `luks_default_password`}}<enter>"
      ],
      "iso_urls": [
        "{{user `local_iso`}}",
        "{{user `iso_path`}}"
      ],
      "iso_checksum": "{{user `iso_sha256`}}",
      "iso_target_path": "{{user `iso_path`}}",
      "http_directory": "files/answer_files/linux/kali",
      "output_directory": "qemu_output_{{user `name`}}",
      "qemuargs": [
        ["-m", "{{user `ram`}}M"],
        ["-cpu", "host"],
        ["-smp", "cpus={{user `cpu`}}"]
      ]
    }
  ],

  "provisioners": [
    {
      "type": "file",
      "source": "files/configs/etc/skel/",
      "destination": "/etc/skel"
    },
    {
      "type": "file",
      "source": "files/configs/etc/systemd/",
      "destination": "/etc/systemd"
    },
    {
      "type": "shell",
      "environment_vars": [
        "LOWPRIV_USER={{user `lowpriv_user`}}",
        "PACKER_NAME={{user `name`}}"
      ],
      "script": "files/scripts/linux/kali-init.sh"
    },
    {
      "type": "shell-local",
      "only_on": ["linux"],
      "script": "files/scripts/linux/make-customize-package.sh"
    },
    {
      "type": "shell-local",
      "only_on": ["linux"],
      "script": "files/scripts/linux/get-latest-burp.sh"
    },
    {
      "type": "shell-local",
      "only_on": ["windows"],
      "execute_command": ["powershell.exe", "{{.vars}}", "{{.script}}"],
      "env_var_format": "$env:%s=\"%s\"; ",
      "script": "files/scripts/linux/get-latest-burp.ps1"
    },
    {
      "type": "shell-local",
      "only_on": ["linux"],
      "script": "files/scripts/linux/get-latest-tbb.sh"
    },
    {
      "type": "shell-local",
      "only_on": ["windows"],
      "execute_command": ["powershell.exe", "{{.vars}}", "{{.script}}"],
      "env_var_format": "$env:%s=\"%s\"; ",
      "script": "files/scripts/linux/get-latest-tbb.ps1"
    },
    {
      "type": "shell-local",
      "only_on": ["linux"],
      "script": "files/scripts/linux/kali-download-tools.sh"
    },
    {
      "type": "file",
      "source": "files/include/",
      "destination": "/opt/tools"
    },
    {
      "type": "shell",
      "script": "files/scripts/linux/secure-grub.sh"
    },
    {
      "type": "shell",
      "environment_vars": [
        "LOWPRIV_USER={{user `lowpriv_user`}}"
      ],
      "script": "files/scripts/linux/kali-post-install.sh"
    },
    {
      "type": "shell",
      "script": "files/scripts/linux/sysprep.sh"
    }
  ],

  "post-processors": [
    {
      "type": "shell-local",
      "only_on": ["linux"],
      "environment_vars": [
        "NAME={{user `name`}}",
        "TIMESTAMP={{user `timestamp`}}",
        "OUTPUT={{user `output`}}"
      ],
      "script": "files/scripts/create-checksums.sh"
    },
    {
      "type": "shell-local",
      "only_on": ["windows"],
      "execute_command": ["powershell.exe", "{{.vars}}", "{{.script}}"],
      "env_var_format": "$env:%s=\"%s\"; ",
      "environment_vars": [
        "NAME={{user `name`}}",
        "TIMESTAMP={{user `timestamp`}}"
      ],
      "script": "files/scripts/create-checksums.ps1"
    }
  ]
}
