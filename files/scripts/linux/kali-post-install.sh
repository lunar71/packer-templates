#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

LOWPRIV_USER="${LOWPRIV_USER:-kali}"
chown -R "${LOWPRIV_USER}:${LOWPRIV_USER}" /opt/tools "/home/${LOWPRIV_USER}"

printf "%s\n" "[INFO] attempting to disable interfaces autoconnect"
if command -v nmcli &>/dev/null; then
    printf "%s\n" "[INFO] found NetworkManager, disabling interfaces autoconnect"
    nmcli -t -f NAME,DEVICE connection show | grep -v -e lo -e loopback -e docker | awk -F ':' '{print $1}' | while IFS= read -r i; do
        nmcli connection modify "${i}" connection.autoconnect off
        printf "%s\n" "[INFO] disabled \"${i}\" autoconnect"
    done
else
    printf "%s\n" "[ERR] NetworkManager not found, skipping"
fi
