#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1

# prereqs
DEBIAN_FRONTEND=noninteractive apt-get update -yqq
DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq
DEBIAN_FRONTEND=noninteractive apt-get install -yqq curl ca-certificates apt-transport-https gpg

# apt keys
curl -sSL https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor --output /etc/apt/trusted.gpg.d/sublimehq-archive.gpg 
printf "%s\n" "deb https://download.sublimetext.com/ apt/stable/" > /etc/apt/sources.list.d/sublime-text.list
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor --output /etc/apt/trusted.gpg.d/vscode.gpg
printf "%s\n" "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/vscode.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list

# packages
DEBIAN_FRONTEND=noninteractive apt-get update -yqq
DEBIAN_FRONTEND=noninteractive apt-get upgrade -yqq
DEBIAN_FRONTEND=noninteractive apt-get install -yqq python3 python3-virtualenv python3-pip openjdk-17-jdk sublime-text code
DEBIAN_FRONTEND=noninteractive apt-get clean -yqq
DEBIAN_FRONTEND=noninteractive apt-get autoclean -yqq
DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -yqq

# nim
curl -sSLf https://nim-lang.org/choosenim/init.sh | bash

# golang
curl -sSLo "/tmp/go_installer" https://get.golang.org/$(uname) && \
    chmod +x "/tmp/go_installer" && \
    /tmp/go_installer 

rm -rf /usr/local/go
mkdir -p /usr/local/go
mv /root/.go/* /usr/local/go
rm -rf /root/.go
ln -s /usr/local/go/bin/go /usr/local/bin/go

# jetbrains toolbox
tar -C /tmp -xzf <(curl -sSL 'https://download.jetbrains.com/product?code=tb&latest&distribution=linux')
mv /tmp/jetbrains*/jetbrains-toolbox /usr/local/bin/jetbrains-toolbox
chmod +x /usr/local/bin/jetbrains-toolbox

# docker and docker-compose
curl -sSLf https://get.docker.com -o - | bash
curl -sSkL $(curl -sKL https://api.github.com/repos/docker/compose/releases | grep -i "$(uname -s)-$(uname -m)" | grep -i 'browser_download_url' | grep -v 'sha256' | head -1 | awk -F'"' '{print $4}') -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# ida free
curl -sSLo /tmp/idafree_linux.run https://out7.hex-rays.com/files/idafree83_linux.run
chmod +x /tmp/idafree_linux.run
/tmp/idafree_linux.run --unattendedmodeui none --mode unattended
find /home -type f -iname "*desktop" -exec rm -rf {} \;
