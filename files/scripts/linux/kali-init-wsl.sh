#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

export DEBIAN_FRONTEND=noninteractive

LOWPRIV_USER="${LOWPRIV_USER:-kali}"
LOWPRIV_HOME=$(getent passwd $LOWPRIV_USER | awk -F ':' '{print $6}')

mkdir -p "/etc/skel/.local/bin" /opt/tools/bin &>/dev/null

apt-get update
apt-get -y upgrade
apt-get -y install kali-linux-core rsync
printf "%s\n" "[INFO] installed kali-linux-core metapackage"

sed -i 's|^PATH="\(.*\)"|PATH="\1:/opt/tools/bin"|' /etc/environment
printf "%s\n" "[INFO] appended /opt/tools/bin to /etc/environment"

apt-get update
apt-get -y upgrade
apt-get -y install kali-linux-everything vim impacket-scripts python3 \
                   python3-virtualenv nmap ncat ffuf seclists golang-go 
apt-get clean
apt-get autoclean
