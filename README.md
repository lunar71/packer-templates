# packer-templates

## Table of Contents

### [Building templates](#building-templates-1)

### [Checksums](#checksums-1)

### [Useful commands](#useful-commands-1)

### [Notes](#notes-1)

### Available templates

<details>
<summary>Click here to display all templates</summary>

- [kali-default-offline-kde.json](#kali-default-offline-kdejson)
- [kali-default-offline-luks-kde.json](#kali-default-offline-luks-kdejson)
- [kali-default-offline-luks-xfce.json](#kali-default-offline-luks-xfcejson)
- [kali-default-offline-xfce.json](#kali-default-offline-xfcejson)
- [kali-everything-offline-kde.json](#kali-everything-offline-kdejson)
- [kali-everything-offline-luks-kde.json](#kali-everything-offline-luks-kdejson)
- [kali-everything-offline-luks-xfce.json](#kali-everything-offline-luks-xfcejson)
- [kali-everything-offline-xfce.json](#kali-everything-offline-xfcejson)
- [kali-top10-offline-kde.json](#kali-top10-offline-kdejson)
- [kali-top10-offline-luks-kde.json](#kali-top10-offline-luks-kdejson)
- [kali-top10-offline-luks-xfce.json](#kali-top10-offline-luks-xfcejson)
- [kali-top10-offline-xfce.json](#kali-top10-offline-xfcejson)
- [ubuntu-10.04-server.json](#ubuntu-1004-serverjson)
- [ubuntu-12.04-server.json](#ubuntu-1204-serverjson)
- [ubuntu-14.04-server.json](#ubuntu-1404-serverjson)
- [ubuntu-16.04-server.json](#ubuntu-1604-serverjson)
- [ubuntu-18.04-server.json](#ubuntu-1804-serverjson)
- [ubuntu-20.04-server.json](#ubuntu-2004-serverjson)
- [ubuntu-22.04-gnome.json](#ubuntu-2204-gnomejson)
- [ubuntu-22.04-kubuntu.json](#ubuntu-2204-kubuntujson)
- [ubuntu-22.04-server.json](#ubuntu-2204-serverjson)
- [ubuntu-22.04-xubuntu.json](#ubuntu-2204-xubuntujson)
- [ubuntu-24.04-gnome.json](#ubuntu-2404-gnomejson)
- [ubuntu-24.04-kubuntu.json](#ubuntu-2404-kubuntujson)
- [ubuntu-24.04-server.json](#ubuntu-2404-serverjson)
- [ubuntu-24.04-xubuntu.json](#ubuntu-2404-xubuntujson)
- [win10-22h2-pro-clean.json](#win10-22h2-pro-cleanjson)
- [win10-22h2-pro-developer.json](#win10-22h2-pro-developerjson)
- [win10-22h2-pro-office365.json](#win10-22h2-pro-office365json)
- [win11-iot-enterprise-ltsc-clean.json](#win11-iot-enterprise-ltsc-cleanjson)
- [win11-iot-enterprise-ltsc-pentest.json](#win11-iot-enterprise-ltsc-pentestjson)
- [win7-pro-sp1.json](#win7-pro-sp1json)
- [winserver2008-clean.json](#winserver2008-cleanjson)
- [winserver2012-clean.json](#winserver2012-cleanjson)
- [winserver2016-clean.json](#winserver2016-cleanjson)
- [winserver2019-clean.json](#winserver2019-cleanjson)
- [winserver2022-clean.json](#winserver2022-cleanjson)


</details>

## Building templates
- on Linux: `./build.sh [-t <template1.json> -t <template2.json>] [-t template*] -v vmware|vbox|qemu|all`
- on Windows: `.\build.ps1 [-t <template1.json> -t <template2.json>] [-t template*] -v vmware|vbox|qemu|all`
- manually: `packer build [-only=vmware|vbox|qemu] <template>`

## Building via Jenkins server/agent on Proxmox

- ensure nested virtualization is supported

```
cat /sys/module/kvm_intel/parameters/nested
```

- enable nested virtualization for server/agent

```
echo "options kvm-intel nested=Y" | sudo tee /etc/modprobe.d/kvm-intel.conf
modprobe -r kvm_intel
modprobe kvm_intel
```

- enable nested virtualization for vm

```
qm set <vmid> --cpu host
```

- schedule parameterized builds via job configuration + Parameterized Scheduler plugin daily at 03:00 AM UTC

```
00 03 7,14,21,28 * * %kali_everything_offline_luks_kde_json=true;kali_everything_offline_luks_xfce_json=true;virt=<virtualbox|vmware|qemu>;iso_url=http://storage.lan/iso;output=/path/to/output;boot_wait=5s;download_urls=http://storage.local/file.zip,http://storage.local/file.txt;prebuild=true
```

### via Freestyle job

- configure freestyle job
- parameterize job with:

```
template:
$(ls -la *.json | awk '{print $9}')

virt:
virtualbox
vmware
qemu

iso_url:
http://nas.local.lan/iso

output: /path/to/output/dir
```

- build step shell script

```
if test -d "${WORKSPACE}/packer-templates"; then
    cd "${WORKSPACE}/packer-templates" && git pull
else
    git clone https://gitlab.com/lunar71/packer-templates
fi

cd "${WORKSPACE}/packer-templates"
/bin/bash build.sh -v "${virt}" -t "${template}" -u "${iso_url}" -H -j 1 -o "${output}"
```

### via Pipeline job

- generate a fresh Jenkinsfile:

```
python3 generate-jenkinsfile.py
```

- create a pipeline job pointing to this repo
- build a 1st time in order to add parameters to the job
- build a 2nd time with necessary parameters

## build.sh cron schedule

```
SHELL=/bin/bash
LOGNAME=root
USER=root
HOME=/root
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

00 03 7,14,21,28 * * /path/to/build.sh -c -v virtualbox -t template.json -t template.json -u http://nas.lan/iso -w 5s -H -o /path/to/nfs -x -n discord-notifications 2>&1 >>/root/cron.log
```

## build.sh notifications
- with the -n <provider id> parameter to build.sh, notifications can be sent
- requires /etc/notify/notify.yaml

```
discord:
  - id: "discord-notifications"
    discord_channel: "notifications"
    discord_username: "notifications-bot"
    discord_format: "{data}"
    discord_webhook_url: "https://discord.com/api/webhooks/aaa/bbb"
```

## Checksums
- all built templates run a final post-processor `create-checksums.sh|create-checksums.ps1`
- md5 and sha512 checksums are added next to each generated ova e.g.

```
vmware-ubuntu-20.04-server_2024-08-19.ova
vmware-ubuntu-20.04-server_2024-08-19.sums
```

## Useful commands
- rebuild vmmon and vmnet: `sudo vmware-modconfig --console --install-all`
- list virtualbox os types: `VBoxManage list ostypes`
- remove virtualbox old uuids: `vboxmanage list hdds; vboxmanage closemedium disk <uuid> --delete`
- add files to zip archive: `zip -urj tools.zip <file>`
- fix zip with: `zip -FFv tools.zip --out tools_fixed.zip`

## Notes
- packer opens random ports for preseed files, ensure firewall is disabled

## Templates

<details>
<summary>Click here to display template build configurations</summary>


## kali-default-offline-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-default-offline-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-default-offline-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## kali-default-offline-luks-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-default-offline-luks-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-default-offline-luks-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-default-offline-luks-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-default-offline-luks-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-default-offline-luks-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-default-offline-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-default-offline-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-default-offline-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## kali-everything-offline-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-everything-offline-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-everything-offline-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## kali-everything-offline-luks-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-everything-offline-luks-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-everything-offline-luks-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-everything-offline-luks-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-everything-offline-luks-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-everything-offline-luks-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-everything-offline-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-everything-offline-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-everything-offline-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## kali-top10-offline-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-top10-offline-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-top10-offline-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## kali-top10-offline-luks-kde.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-top10-offline-luks-kde_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-top10-offline-luks-kde_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-top10-offline-luks-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-top10-offline-luks-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-top10-offline-luks-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/secure-grub.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |
| Default LUKS password | `kali` |

## kali-top10-offline-xfce.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kali-linux-2024.4-installer-everything-amd64.iso` |
| ISO sha256 checksum   | `4fa526aafdabb5f30fe2234f25a479e31caa813af21bb08c3ee6d63438f0a0e2` |
| Output VM             | `output/<builder>/<builder>-kali-2024.4-top10-offline-xfce_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-kali-2024.4-top10-offline-xfce_<timestamp>.sums` |
| Default resources     | 4 CPUs, 4.0 GB, 200.0 GB |
| VM Provisioners       | `files/scripts/linux/kali-init.sh`, `files/scripts/linux/kali-post-install.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | `files/scripts/linux/make-customize-package.sh` (linux), `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `files/configs/etc/skel/` -> `/etc/skel`, `files/configs/etc/systemd/` -> `/etc/systemd`, `files/include/` -> `/opt/tools` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `kali`:`kali` |

## ubuntu-10.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-10.04-server-amd64.iso` |
| ISO sha256 checksum   | `212cdd71b95b8ee957b826782983890c536ba1fde547e42e9764ee5c12f43c2d` |
| Output VM             | `output/<builder>/<builder>-ubuntu-10.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-10.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-12.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-12.04-server-amd64.iso` |
| ISO sha256 checksum   | `9c769f0eb9740f785aa62cff54d9ae60dd6e73763d313c69734bf21924101d83` |
| Output VM             | `output/<builder>/<builder>-ubuntu-12.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-12.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-14.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-14.04-server-amd64.iso` |
| ISO sha256 checksum   | `ababb88a492e08759fddcf4f05e5ccc58ec9d47fa37550d63931d0a5fa4f7388` |
| Output VM             | `output/<builder>/<builder>-ubuntu-14.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-14.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-16.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-16.04-server-amd64.iso` |
| ISO sha256 checksum   | `b8b172cbdf04f5ff8adc8c2c1b4007ccf66f00fc6a324a6da6eba67de71746f6` |
| Output VM             | `output/<builder>/<builder>-ubuntu-16.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-16.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-18.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-18.04-server-amd64.iso` |
| ISO sha256 checksum   | `a7f5c7b0cdd0e9560d78f1e47660e066353bb8a79eb78d1fc3f4ea62a07e6cbc` |
| Output VM             | `output/<builder>/<builder>-ubuntu-18.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-18.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-20.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-20.04-live-server-amd64.iso` |
| ISO sha256 checksum   | `caf3fd69c77c439f162e2ba6040e9c320c4ff0d69aad1340a514319a9264df9f` |
| Output VM             | `output/<builder>/<builder>-ubuntu-20.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-20.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-22.04-gnome.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-22.04-desktop-amd64.iso` |
| ISO sha256 checksum   | `b85286d9855f549ed9895763519f6a295a7698fb9c5c5345811b3eefadfb6f07` |
| Output VM             | `output/<builder>/<builder>-ubuntu-22.04-gnome_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-22.04-gnome_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-22.04-kubuntu.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kubuntu-22.04.2-desktop-amd64.iso` |
| ISO sha256 checksum   | `0e98571bda4d92cb130b95bc5a581280b1969512c8dbca68ea7434ba94c554a7` |
| Output VM             | `output/<builder>/<builder>-ubuntu-22.04-kubuntu_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-22.04-kubuntu_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-22.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-22.04-live-server-amd64.iso` |
| ISO sha256 checksum   | `84aeaf7823c8c61baa0ae862d0a06b03409394800000b3235854a6b38eb4856f` |
| Output VM             | `output/<builder>/<builder>-ubuntu-22.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-22.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-22.04-xubuntu.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `xubuntu-22.04.2-desktop-amd64.iso` |
| ISO sha256 checksum   | `c7072859113399bef0e680a08c987c360f41642d8089ebb8928689066a9c9759` |
| Output VM             | `output/<builder>/<builder>-ubuntu-22.04-xubuntu_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-22.04-xubuntu_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-24.04-gnome.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-24.04.1-desktop-amd64.iso` |
| ISO sha256 checksum   | `c2e6f4dc37ac944e2ed507f87c6188dd4d3179bf4a3f9e110d3c88d1f3294bdc` |
| Output VM             | `output/<builder>/<builder>-ubuntu-24.04-gnome_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-24.04-gnome_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-24.04-kubuntu.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `kubuntu-24.04.1-desktop-amd64.iso` |
| ISO sha256 checksum   | `a828578f407471a69d13c0caa7cc0d4013f5b9f672de069e8017163d13695c3c` |
| Output VM             | `output/<builder>/<builder>-ubuntu-24.04-kubuntu_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-24.04-kubuntu_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-24.04-server.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `ubuntu-24.04-live-server-amd64.iso` |
| ISO sha256 checksum   | `8762f7e74e4d64d72fceb5f70682e6b069932deedb4949c6975d0f0fe0a91be3` |
| Output VM             | `output/<builder>/<builder>-ubuntu-24.04-server_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-24.04-server_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## ubuntu-24.04-xubuntu.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `xubuntu-24.04.1-desktop-amd64.iso` |
| ISO sha256 checksum   | `c333806173558ccc2a95f44c5c7b57437ee3d409b50a3a5a1367bcf7eaf3ef90` |
| Output VM             | `output/<builder>/<builder>-ubuntu-24.04-xubuntu_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-ubuntu-24.04-xubuntu_<timestamp>.sums` |
| Default resources     | 2 CPUs, 2.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/linux/setup-vmwaretools.sh`, `files/scripts/linux/setup-vboxtools.sh`, `files/scripts/linux/sysprep.sh` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `root`:`root`, `ubuntu`:`ubuntu` |

## win10-22h2-pro-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `Win10_22H2_English_x64.iso` |
| ISO sha256 checksum   | `f41ba37aa02dcb552dc61cef5c644e55b5d35a8ebdfac346e70f80321343b506` |
| Output VM             | `output/<builder>/<builder>-win10-22h2-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win10-22h2-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## win10-22h2-pro-developer.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `Win10_22H2_English_x64.iso` |
| ISO sha256 checksum   | `f41ba37aa02dcb552dc61cef5c644e55b5d35a8ebdfac346e70f80321343b506` |
| Output VM             | `output/<builder>/<builder>-win10-22h2-developer_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win10-22h2-developer_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/av-init.ps1`, `files/scripts/windows/modify-ntuserdat.ps1`, `files/scripts/windows/setup-chocolatey.ps1`, `files/scripts/windows/install-choco-packages.ps1`, `files/scripts/windows/setup-contextmenu.ps1`, `files/scripts/windows/install-windows-terminal.ps1`, `files/scripts/windows/setup-office365.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `files/scripts/windows/sysprep-shutdown.bat` -> `C:\sysprep-shutdown.bat`, `files/include/` -> `C:\tools`, `files/scripts/windows/choco-packages.config` -> `C:\choco-packages.config` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |
| Choco packages | `7zip, adobereader, apimonitor, apktool, autohotkey, autohotkey.install, awscli, azure-cli, burp-suite-free-edition, cyberchef, dbeaver, exiftool, explorersuite, fiddler, firefox, ghidra, git, golang, googlechrome, hashmyfiles, heidisql, hxd, ilspy, netcat, notepadplusplus, openvpn, pebear, pestudio, pinta, processhacker, putty, python310, resourcehacker.portable, sysinternals, telnet, tor-browser, uniextract, vlc, vscode, winscp, wireshark, dnspy` |

## win10-22h2-pro-office365.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `Win10_22H2_English_x64.iso` |
| ISO sha256 checksum   | `f41ba37aa02dcb552dc61cef5c644e55b5d35a8ebdfac346e70f80321343b506` |
| Output VM             | `output/<builder>/<builder>-win10-22h2-office365_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win10-22h2-office365_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-office365.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## win11-iot-enterprise-ltsc-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `en-us_windows_11_iot_enterprise_ltsc_2024_x64_dvd_f6b14814.iso` |
| ISO sha256 checksum   | `4f59662a96fc1da48c1b415d6c369d08af55ddd64e8f1c84e0166d9e50405d7a` |
| Output VM             | `output/<builder>/<builder>-win11-iot-enterprise-ltsc-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win11-iot-enterprise-ltsc-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## win11-iot-enterprise-ltsc-pentest.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `en-us_windows_11_iot_enterprise_ltsc_2024_x64_dvd_f6b14814.iso` |
| ISO sha256 checksum   | `4f59662a96fc1da48c1b415d6c369d08af55ddd64e8f1c84e0166d9e50405d7a` |
| Output VM             | `output/<builder>/<builder>-win11-iot-enterprise-ltsc-pentest_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win11-iot-enterprise-ltsc-pentest_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/copy-cd-files.ps1`, `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | `files/scripts/linux/get-latest-burp.sh` (linux), `files/scripts/linux/get-latest-burp.ps1` (windows), `files/scripts/linux/get-latest-tbb.sh` (linux), `files/scripts/linux/get-latest-tbb.ps1` (windows), `files/scripts/linux/kali-download-tools.sh` (linux)
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## win7-pro-sp1.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox` |
| ISO                   | `7601.24214.180801-1700.win7sp1_ldr_escrow_CLIENT_PROFESSIONAL_x64FRE_en-us.iso` |
| ISO sha256 checksum   | `19bba30eb8231119ac23a11bba65b70446b6ea9e35cf64dc2c12d21d8fd445ea` |
| Output VM             | `output/<builder>/<builder>-win7-pro-sp1_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-win7-pro-sp1_<timestamp>.sums` |
| Default resources     | 2 CPUs, 4.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | None |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## winserver2008-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `7601.17514.101119-1850_x64fre_server_eval_en-us-GRMSXEVAL_EN_DVD.iso` |
| ISO sha256 checksum   | `30832ad76ccfa4ce48ccb936edefe02079d42fb1da32201bf9e3a880c8ed6312` |
| Output VM             | `output/<builder>/<builder>-winserver2008-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-winserver2008-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 8.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## winserver2012-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `9600.17050.WINBLUE_REFRESH.140317-1640_X64FRE_SERVER_EVAL_EN-US-IR3_SSS_X64FREE_EN-US_DV9.iso` |
| ISO sha256 checksum   | `6612b5b1f53e845aacdf96e974bb119a3d9b4dcb5b82e65804ab7e534dc7b4d5` |
| Output VM             | `output/<builder>/<builder>-winserver2012-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-winserver2012-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 8.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## winserver2016-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `Windows_Server_2016_Datacenter_EVAL_en-us_14393_refresh.iso` |
| ISO sha256 checksum   | `1ce702a578a3cb1ac3d14873980838590f06d5b7101c5daaccbac9d73f1fb50f` |
| Output VM             | `output/<builder>/<builder>-winserver2016-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-winserver2016-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 8.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## winserver2019-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso` |
| ISO sha256 checksum   | `549bca46c055157291be6c22a3aaaed8330e78ef4382c99ee82c896426a1cee1` |
| Output VM             | `output/<builder>/<builder>-winserver2019-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-winserver2019-clean_<timestamp>.sums` |
| Default resources     | 4 CPUs, 8.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

## winserver2022-clean.json

| Configuration         | Value |
| --------------------- | ----- |
| Builders              | `vmware`, `virtualbox`, `qemu` |
| ISO                   | `SERVER_EVAL_x64FRE_en-us.iso` |
| ISO sha256 checksum   | `3e4fa6d8507b554856fc9ca6079cc402df11a8b79344871669f0251535255325` |
| Output VM             | `output/<builder>/<builder>-winserver2022-clean_<timestamp>.<ova\|qemu>` |
| Output VM checksums   | `output/<builder>/<builder>-winserver2022-clean_<timestamp>.sums` |
| Default resources     | 2 CPUs, 8.0 GB, 100.0 GB |
| VM Provisioners       | `files/scripts/windows/disable-updates.ps1`, `files/scripts/windows/setup-vmwaretools.ps1`, `files/scripts/windows/setup-vboxtools.ps1`, `files/scripts/windows/tweaks.ps1`, `files/scripts/windows/cleanup.ps1` |
| Local Provisioners    | None
| Files                 | `None` -> `C:\scripts\` |
| Post Processors       | `files/scripts/create-checksums.sh` (linux), `files/scripts/create-checksums.ps1` (windows) |
| Default credentials   | `N/A, VM is sysprepped` |

</details>