$ProgressPreference = "SilentlyContinue"
$global:ProgressPreference = "SilentlyContinue"

$ToolsDir = "C:\Tools"
$BinDir = "$ToolsDir\Bin"
$ToolsList = @(
    @{
        "Url" = "https://github.com/Flangvik/SharpCollection"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/Flangvik/TeamFiltration"
        "GetRelease" = $true
        "Pattern" = "*-win-x86_64*zip*"
    },
    @{
        "Url" = "https://github.com/skahwah/SQLRecon"
        "GetRelease" = $true
        "Pattern" = "*sqlrecon*exe*"
    },
    @{
        "Url" = "https://github.com/fortra/impacket"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/fortra/nanodump"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/eladshamir/Whisker"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/Raikia/CredNinja"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/outflanknl/Dumpert"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/fuzzdb-project/fuzzdb"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/danielmiessler/SecLists"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/ohpe/juicy-potato"
        "GetRelease" = $true
        "Pattern" = "*juicypotato*exe*"
    },
    @{
        "Url" = "https://github.com/p0dalirius/Coercer"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/dafthack/MailSniper"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/PowerShellMafia/PowerSploit"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/besimorhino/powercat"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/topotam/PetitPotam"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/Kevin-Robertson/Powermad"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/NetSPI/PowerUpSQL"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/SnaffCon/Snaffler"
        "GetRelease" = $true
        "Pattern" = "*snaffler*exe*"
    },
    @{
        "Url" = "https://github.com/lkarlslund/ldapnomnom"
        "GetRelease" = $true
        "Pattern" = "*-windows-amd64*exe*"
    },
    @{
        "Url" = "https://github.com/gentilkiwi/mimikatz"
        "GetRelease" = $true
        "Pattern" = "*mimikatz*7z*"
    },
    @{
        "Url" = "https://github.com/Kevin-Robertson/Inveigh"
        "GetRelease" = $true
        "Pattern" = "*inveigh*net4*zip*"
    },
    @{
        "Url" = "https://github.com/Kevin-Robertson/Invoke-TheHash"
        "GetRelease" = $false
    },
    @{
        "Url" = "https://github.com/RedTeamPentesting/pretender"
        "GetRelease" = $true
        "Pattern" = "*windows*64*zip*"
    },
    @{
        "Url" = "https://github.com/leoloobeek/LAPSToolkit"
        "GetRelease" = $false
    }
)

function Get-LatestGithubRelease {
    [CmdletBinding()]
    param(
        [Parameter(mandatory = $true)]$Url,
        [Parameter(mandatory = $true)]$Destination,
        [Parameter(mandatory = $true)]$Pattern
    )
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $wc = New-Object System.Net.WebClient
    $wc.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"

    $Parts = $Url.Split("/")
    $RepoOwner = $parts[-2]
    $RepoName = $parts[-1]
    $ApiUrl = "https://api.github.com/repos/$RepoOwner/$RepoName/releases"

    $DownloadUrl = (Invoke-WebRequest -Uri $ApiUrl -UseBasicParsing).Content `
        | ConvertFrom-Json `
        | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
        | Where-Object { $_ -ilike "$Pattern" } `
        | Select-Object -First 1 `
        | Select-Object -ExpandProperty "browser_download_url"

    $OutputFile = [System.IO.Path]::GetFileName($DownloadUrl)
    $Output = "$Destination\$OutputFile"

    try {
        $wc.DownloadFile($DownloadUrl, $Output)
        Write-Host "[INFO] Downloaded $DownloadUrl"
    } catch {
        Write-Host "[ERR] Failed to download $DownloadUrl"
    }
}

if (Get-Command git -ErrorAction SilentlyContinue) {
    New-Item -Path $ToolsDir -ItemType "Directory" -Force | Out-Null
    New-Item -Path $BinDir -ItemType "Directory" -Force | Out-Null
    $ToolsList | % {
        try {
            git -C $ToolsDir clone --quiet $_.Url
            Write-Host "[INFO] Cloned $($_.Url)"
        } catch {
            Write-Host "[ERR] Failed to clone $($_.Url)"
        }

        if ($_.GetRelease -eq $true) {
            Get-LatestGithubRelease -Url $_.Url -Destination $BinDir -Pattern $_.Pattern 
        }
    }
}
