#!/usr/bin/env python3

import os
import glob

json_files = [os.path.basename(i) for i in sorted(glob.glob(os.path.join(os.curdir, '*.json')))]

bool_params = ''
for f in json_files:
    param_name = f.replace('.', '_').replace('-', '_')
    bool_params += f'''        booleanParam(
            name: '{param_name}',
            defaultValue: false,
            description: 'packer template name: {f}'
        )
'''

selected_template_code = ''
for f in json_files:
    param_name = f.replace('.', '_').replace('-', '_')
    selected_template_code += f'''                    if (params.{param_name}) {{
                        selectedTemplates << "{f}"
                    }}
'''

jenkinsfile = f'''pipeline {{
    agent {{ label "${{params.agent_label}}" }}

    parameters {{
        string(
            name: 'agent_label',
            defaultValue: '',
            trim: true,
            description: 'jenkins agent to run on'
        )
{bool_params}        choice(
            name: 'virt',
            choices: ['virtualbox', 'vmware', 'qemu'],
            description: 'virtualization engine to use for resulting vm'
        )
        string(
            name: 'boot_wait',
            defaultValue: '',
            trim: true,
            description: 'override template boot_wait variable; must end in "s"'
        )
        string(
            name: 'iso_url',
            defaultValue: '',
            trim: true,
            description: 'path to network hosted iso files e.g., http://nas.local.lan/iso'
        )
        text(
            name: 'download_urls',
            defaultValue: '',
            description: 'list of URLs (one per line) to download before building the template'
        )
        string(
            name: 'output',
            defaultValue: 'output',
            trim: true,
            description: 'custom output directory for built vms'
        )
        booleanParam(
            name: 'prebuild',
            defaultValue: false,
            description: 'download tools before executing packer build'
        )
    }}

    stages {{
        stage('Check and install prerequisites') {{
            steps {{
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-packer.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-virtualbox.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-qemu.sh"'
                sh 'sudo /bin/bash -c "/bin/bash files/deployment/install-docker.sh"'
                sh '/bin/bash files/deployment/install-packer-plugins.sh'
            }}
        }}

        stage('Download Files') {{
            steps {{
                script {{
                    def includeDir = "${{env.WORKSPACE}}/files/include"
                    sh "mkdir -p ${{includeDir}} &>/dev/null"

                    def urls = []
                    if (params.download_urls?.trim()) {{
                        urls = params.download_urls.split(/[,\\n\\r]+/).collect {{ it.trim() }}.findAll {{ it }}
                        for (url in urls) {{
                            def filename = url.tokenize('/')[-1]

                            sh "rm -rf ${{includeDir}}/${{filename}}"
                            sh "curl -sSkL ${{url}} -o ${{includeDir}}/${{filename}}"
                        }}
                    }}
                }}
            }}
        }}

        stage('Build template') {{
            steps {{
                script {{
                    def selectedTemplates = []
{selected_template_code}
                    if (selectedTemplates.isEmpty()) {{
                        error "No template selected. Please choose at least one template."
                    }}
                    def templateArgs = selectedTemplates.collect {{ "-t " + it }}.join(" ")

                    def bootWaitFlag = params.boot_wait ? "-w ${{params.boot_wait}}" : ""
                    def preBuildFlag = params.prebuild ? "-x" : ""
                    def cmd = "/bin/bash build.sh -v ${{params.virt}} " + templateArgs + " -u ${{params.iso_url}} -H -j 1 -o ${{params.output}} " + preBuildFlag + " " + bootWaitFlag
                    sh cmd
                }}
            }}
        }}
    }}
}}
'''

with open('Jenkinsfile', 'w') as fh:
    fh.write(jenkinsfile)

