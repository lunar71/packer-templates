$ErrorActionPreference = "Stop"

$PACKER_BUILD_NAME = $env:PACKER_BUILD_NAME.Trim()
$NAME = $env:NAME.Trim()
$TIMESTAMP = $env:TIMESTAMP.Trim()
$OVA_DIR = $env:OVA_DIR.Trim()

if (-not (Get-Command Get-FileHash -ErrorAction SilentlyContinue)) {
    Write-Host "[WARN] Get-FileHash not found, skipping"
    exit 0
}

if (-not $PACKER_BUILD_NAME) {
    Write-Host "[WARN] `$PACKER_BUILD_NAME not supplied for checksum creation, skipping"
    exit 0
}

if (-not $NAME) {
    Write-Host "[WARN] `$NAME not supplied for checksum creation, skipping"
    exit 0
}

if (-not $TIMESTAMP) {
    Write-Host "[WARN] `$TIMESTAMP not supplied for checksum creation, skipping"
    exit 0
}

New-Item -ItemType Directory -Path $OVA_DIR -Force | Out-Null

$SourcePath = "${PACKER_BUILD_NAME}_output_${NAME}\${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova"
$DestinationPath = "output\${PACKER_BUILD_NAME}\${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova"

Move-Item -Path $SourcePath -Destination $DestinationPath -Force
Remove-Item -Recurse -Force "${PACKER_BUILD_NAME}_output_${NAME}"

Push-Location -Path $OVA_DIR
try {
    $SumsFile = "${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.sums"

    $md5Hash = Get-FileHash -Path "${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova" -Algorithm MD5 | Select-Object -ExpandProperty Hash
    $sha512Hash = Get-FileHash -Path "${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova" -Algorithm SHA512 | Select-Object -ExpandProperty Hash

    "$md5Hash  ${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova" | Out-File -FilePath $SumsFile -Encoding UTF8
    "$sha512Hash  ${PACKER_BUILD_NAME}-${NAME}_${TIMESTAMP}.ova" | Out-File -FilePath $SumsFile -Encoding UTF8 -Append

    Write-Host "[INFO] created md5 and sha512 checksums for ${DestinationPath}"
}
finally {
    Pop-Location
}
