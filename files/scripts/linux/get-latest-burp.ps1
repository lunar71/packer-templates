$ProgressPreference = 'SilentlyContinue'
Push-Location -Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent)

$latestVersion = (Invoke-WebRequest -Uri "https://portswigger.net/burp/releases/professional/latest" -UseBasicParsing).Content |
                 Select-String -Pattern 'v20[0-9]{2}\.[0-9]{1,}\.[0-9]{1,}' |
                 ForEach-Object { $_.Matches.Value } |
                 Sort-Object -Unique |
                 ForEach-Object { $_ -replace '^v', '' }

Write-Host "[INFO] Found latest Burp Suite Professional version $latestVersion"

$url = "https://portswigger-cdn.net/burp/releases/download?product=pro&version=$latestVersion&type=jar"

$includeDir = Join-Path -Path (Resolve-Path ..\..\include) -ChildPath ""

if (-not (Test-Path -Path $includeDir)) {
    New-Item -Path $includeDir -ItemType Directory | Out-Null
}

$existingFile = Get-ChildItem -Path $includeDir -Filter "*$latestVersion*.jar" -ErrorAction SilentlyContinue

if (-not $existingFile) {
    Write-Host "[INFO] Attempting to download the latest Burp Suite Professional version from $url"
    Write-Host "[INFO] Latest version of Burp Suite Professional not found in $includeDir, downloading"
    
    Push-Location -Path $includeDir
    Remove-Item *.jar
    Write-Host "[INFO] Removing any Burp Suite Professional old versions"
    Write-Host "[INFO] Downloading latest Burp Suite Professional"

    try {
        Invoke-WebRequest -Uri $url -OutFile "$includeDir\burpsuite_pro_v$latestVersion.jar"
        Write-Host "[INFO] Successfully downloaded latest Burp Suite Professional"
    } catch {
        Write-Host "[ERR] Error occurred while downloading latest Burp Suite Professional: $_"
    }

    Pop-Location
} else {
    Write-Host "[INFO] Found latest Burp Suite Professional version in $includeDir, skipping download"
}

Pop-Location

