# disable first run experience on edge
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Edge", "HideFirstRunExperience", 1)

# disable windows spotlight and content delivery manager
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CloudContent", "DisableWindowsSpotlightFeatures", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CloudContent", "DisableConsumerAccountStateContent", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CloudContent", "DisableCloudOptimizedContent", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CloudContent", "DisableSoftLanding", 1)
[Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CloudContent", "DisableWindowsConsumerFeatures", 1)
