#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1

user="root"
password=$(printf "%s\n" $(tr -cd "[:alnum:]" </dev/urandom | fold -w32 | head -1))
grub_password=$(printf "%s\n%s" $password $password | grub-mkpasswd-pbkdf2 | grep -io 'grub.pbkdf2.*')

printf "%s\n" "${user}:${password}" > /root/grub_password.txt

if [[ -f /etc/grub.d/40_custom ]]; then
    printf "%s\n" "set superusers=\"${user}\"" \
        "password_pbkdf2 ${user} ${grub_password}" >> /etc/grub.d/40_custom
else
    printf "%s\n" '#!/bin/sh' \
        'exec tail -n +3 $0' \
        "# This file provides an easy way to add custom menu entries.  Simply type the" \
        "# menu entries you want to add after this comment.  Be careful not to change" \
        "# the 'exec tail' line above." \
        "set superusers=\"${user}\"" \
        "password_pbkdf2 ${user} ${grub_password}" > /etc/grub.d/40_custom
fi

sed -i 's/CLASS=".*--class gnu-linux[^"]*/& --unrestricted/' /etc/grub.d/10_linux
update-grub
