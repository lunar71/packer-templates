#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt-get update -yqq
apt-get install -yqq open-vm-tools
apt-get clean -yqq
apt-get autoclean -yqq
