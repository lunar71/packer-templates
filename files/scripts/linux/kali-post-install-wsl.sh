#!/bin/bash
# -*- coding: utf-8 -*-
set -e
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null
trap 'popd &>/dev/null' EXIT

LOWPRIV_USER="${LOWPRIV_USER:-kali}"

sed -i 's/SHELL=.*/SHELL=\/bin\/bash/g' /etc/default/useradd
adduser --disabled-password --gecos "" --shell /bin/bash "${LOWPRIV_USER}"
printf "%s\n" "${LOWPRIV_USER}:${LOWPRIV_USER}" | chpasswd
usermod -aG sudo "${LOWPRIV_USER}"

chsh -s /bin/bash root
cp -rf /etc/skel/. /root

LOWPRIV_HOME=$(getent passwd $LOWPRIV_USER | awk -F ':' '{print $6}')

chsh -s /bin/bash $LOWPRIV_USER
cp -rf /etc/skel/. $LOWPRIV_HOME

> /root/.hushlogin
> "${LOWPRIV_HOME}/.hushlogin"

printf "%s\n" \
    "[INFO] created kali user" \
    "[INFO] set /bin/bash default shell" \
    "[INFO] updated \$HOME with /etc/skel"

chown -R "${LOWPRIV_USER}:${LOWPRIV_USER}" /opt/tools "${LOWPRIV_HOME}"
