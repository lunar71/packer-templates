#!/bin/bash
# -*- coding: utf-8 -*-
# requires:
# pkg-config libpcap-dev libnetfilter-queue1 libnetfilter-queue-dev libusb-1.0-0-dev
set -e

tools_zip=$(realpath "$(pwd)/../../tools.zip")
log_file=$(realpath "$(pwd)/../../setup-tools-export.log")
>"${log_file}"
exec > >(tee -a "${log_file}") 2>&1

[[ ! $(command -v docker) ]] && printf "%s\n" "docker not installed or not in path, exiting" && exit 255

if [[ ! -f $(pwd)/tools.sh ]]; then
    printf "%s\n" "tools.sh that contains repositories and tool definitions not found"
    printf "%s\n" "run the generate-tools-array.sh script"
    exit 255
else
    source $(pwd)/tools.sh
fi

export_dir=$(TMPDIR="$(pwd)" mktemp -d)
printf "%s\n" "[INFO] using ${export_dir} as temporary export directory"
> "${export_dir}/TOOLS.txt"

for dir in "${!repos[@]}"; do
    mkdir -p "${export_dir}/${dir}" &>/dev/null
done
mkdir -p "${export_dir}/binaries/windows" &>/dev/null
mkdir -p "${export_dir}/binaries/deb" &>/dev/null

docker run --rm -it \
    -v "${export_dir}/binaries/golang/linux:/go/bin/linux" \
    -v "${export_dir}/binaries/golang/win64:/go/bin/windows_amd64" golang:latest /bin/bash -c "\
        apt-get update -yqq
        apt-get install -yqq build-essential libpcap-dev libusb-1.0-0-dev libnetfilter-queue-dev
        failed_linux_tools=()
        failed_windows_tools=()
        max_retries=3
        for i in $(printf "$golang_tools" | sort -u); do
            printf \"\\n%s\\n\" \"[INFO] installing \$i\"

            retries=0
            while [ \$retries -lt \$max_retries ]; do
                if GOBIN=/go/bin/linux go install -trimpath \$i &>/dev/null; then
                    printf \"[INFO] %s (linux) installed successfully\\n\" \"\$i\"
                    break
                else
                    printf \"[WARN] failed to install %s (linux) (attempt %d)\\n\" \"\$i\" \$((retries+1)) >&2
                    retries=\$((retries+1))
                    sleep 5
                fi
            done
            if [ \$retries -eq \$max_retries ]; then
                printf \"[ERR] giving up on %s (linux) after %d attempts\\n\" \"\$i\" \$max_retries >&2
                failed_linux_tools+=(\$i)
            fi

            retries=0
            while [ \$retries -lt \$max_retries ]; do
                if GOOS=windows go install -trimpath \$i &>/dev/null; then
                    printf \"[INFO] %s (windows) installed successfully\\n\" \"\$i\"
                    break
                else
                    printf \"[WARN] failed to install %s (windows) (attempt %d)\\n\" \"\$i\" \$((retries+1)) >&2
                    retries=\$((retries+1))
                    sleep 5
                fi
            done
            if [ \$retries -eq \$max_retries ]; then
                printf \"[ERR] giving up on %s (windows) after %d attempts\\n\" \"\$i\" \$max_retries >&2
                failed_windows_tools+=(\$i)
            fi
        done
        if [ \${#failed_linux_tools[@]} -gt 0 ]; then
            printf \"[ERR] failed to install linux versions of tools: %s\\n\" \"\${failed_linux_tools[@]}\" >&2
        fi
        if [ \${#failed_windows_tools[@]} -gt 0 ]; then
            printf \"[ERR] failed to install windows versions of tools: %s\\n\" \"\${failed_windows_tools[@]}\" >&2
        fi"

printf "%s\n\n" "[binaries] various windows and linux tools" >> "${export_dir}/TOOLS.txt"

for category in "${!repos[@]}"; do 
    for repo in "${repos[$category]}"; do
        for i in $(printf "$repo" | sort -u); do
            if [[ $(curl -o /dev/null -s -w "%{http_code}" "${i}") != "404" ]]; then
                printf "\n%s\n" "[INFO] downloading ${i} to ${export_dir}/${i}"
                if git_clone_output=$(git -C "${export_dir}/${category}" clone $i 2>&1); then
                    description=$(curl -sSL $i | grep -oP '<meta name="description" content="\K[^"]+' | awk -F ' - Git' '{print $1}')
                    printf "%s\n" "[${category}] ${i} - ${description}" >> "${export_dir}/TOOLS.txt"
                    printf "%s\n" "[INFO] ${i} cloned successfully to ${export_dir}/${category}"
                else
                    printf "%s\n" "[ERR] failed to clone ${i}"
                    printf "%s\n" "[ERR] ${git_clone_output}"
                fi
            else
                printf "%s\n" "[WARN] ${i} not found, skipping"
            fi
        done
    done
    printf "\n" >> "${export_dir}/TOOLS.txt"
done

for category in "${!binaries[@]}"; do
    for binary in "${binaries[$category]}"; do
        for i in $(printf "%s" "$binary"); do
            printf "%s\n" "[INFO] downloading ${i} to ${export_dir}/binaries"
            if wget --content-disposition -q "${i}" -P "${export_dir}/binaries" 2>&1; then
                printf "%s\n" "[INFO] ${i} downloaded successfully to ${export_dir}/binaries"
            else
                printf "%s\n" "[ERR] failed to download ${i}"
            fi
            printf "\n"
        done
    done
done

    cat > "${export_dir}/update-tools.sh" << 'EOF'
#!/bin/bash

trap 'git config --global --unset safe.directory' EXIT
[[ -z $(git config --global safe.directory) ]] && git config --global --add safe.directory '*'

for i in $(find . -name ".git" -type d | sed 's/\/.git//g'); do
    (cd $i; git pull)
done
EOF

chmod +x "${export_dir}/update-tools.sh"

printf "%s\n" "[INFO] building export zip to ${tools_zip}"

if zip_output=$(cd "${export_dir}" && zip -r "${tools_zip}" . 2>&1); then
    printf "%s\n" "[INFO] sucessfully created ${tools_zip}"
else
    printf "%s\n" "[ERR] failed to create ${tools_zip}"
    printf "%s\n" "[ERR] ${zip_output}"
fi

if zip_output=$(zip -FF "${tools_zip}" --out "${tools_zip}.fixed" 2>&1); then
    printf "%s\n" "[INFO] fixed ${tools_zip}"
else
    printf "%s\n" "[ERR] failed to fix ${tools_zip}"
    printf "%s\n" "[ERR] ${zip_output}"
fi

rm "${tools_zip}"
mv "${tools_zip}.fixed" "${tools_zip}"
rm -rf "${export_dir}" &>/dev/null

printf "%s\n" "[INFO] finished building export ${tools_zip}"
printf "%s\n" "[INFO] check log file ${log_file}"

if warnings=$(grep '\[WARN\]' $log_file); then
    printf "\n%s\n" "--- the following warnings occured during build ---"
    printf "%s\n" "$warnings"
fi

if errors=$(grep '\[ERR\]' $log_file); then
    printf "\n%s\n" "--- the following errors occured during build ---"
    printf "%s\n" "$errors"
fi
