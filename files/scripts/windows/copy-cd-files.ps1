$drives = "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"
foreach ($drive in $drives) {
    $dir = "${drive}:\\include\"
    if (Test-Path $dir) {
        Write-Host "[INFO] found ${dir}, copying"
        Copy-Item -Path "${dir}/*" -Destination C:\Tools -Recurse
        Break
    }
}

