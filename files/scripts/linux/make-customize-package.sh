#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

tar czf /tmp/skel.tar.gz \
    --transform 's|^|customize/|' \
    -C "${SCRIPT_DIR}/../../configs/etc" skel.custom \
    -C "${SCRIPT_DIR}" kali-kde-setup.sh \
    -C "${SCRIPT_DIR}" kali-xfce-setup.sh

cat > "${SCRIPT_DIR}/../../include/customize-kali.sh" << EOF
#!/bin/bash

test "\${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

if test -z "\${1}"; then
    printf "%s\\n" \\
        "usage: \$(basename \$0) install" \\
        "" \\
        "customization script:" \\
        "  - adds a customized /etc/skel and applies changes to all users home directories" \\
        "  - customized desktop environment settings and keyboard shortcuts" \\
        "  - check ~/Desktop/CUSTOMIZATIONS.txt"

    exit 1
fi

if test "\${1}" == "install"; then
    sed '0,/^#EOF#$/d' \$0 | base64 -d | tar zx
    desktop_env=\$(printf "%s\n" "\${XDG_CURRENT_DESKTOP}" | tr -d '\\n' | tr '[[:upper:]]' '[[:lower:]]')

    if test "\${desktop_env}"; then
        if test "\${desktop_env}" == "kde"; then
            printf "%s\\n" "detected kde desktop environment" 
            bash customize/kali-kde-setup.sh
            customized_desktop=true
        elif test "\${desktop_env}" == "xfce"; then
            printf "%s\\n" "detected xfce desktop environment" 
            bash customize/kali-xfce-setup.sh 
            customized_desktop=true
        else
            printf "%s\\n" \\
                "active desktop environment is not kde or xfce" \\
                "XDG_CURRENT_DESKTOP value is \${desktop_env}" \\
                "skipping desktop environment customization"
        fi
    else
        printf "%s\\n" \\
            "could not identify current desktop environment" \\
            "skipping desktop environment customization"
    fi

    cp -r /etc/skel /etc/skel.bak
    cp -r customize/skel.custom/. /etc/skel
    
    if test "\${customized_desktop}"; then
        mkdir -p /etc/skel/Desktop /etc/skel/.local
        extra_scripts=\$(find /etc/skel/.local/bin -type f -exec basename {} \; | sed 's/^/  - /g')
        cat > /etc/skel/.local/CUSTOMIZATIONS.txt << EOL
extra scripts:
\${extra_scripts}

keyboard shortcuts:
  - <Meta>+Return          launch x-terminal-emulator
  - <Meta>+<LShift>+p      launch screenshot application
  - <Meta>+e               launch file browser
  - <Meta>+<LShift>+q      kill focused window
  - <Meta>+<LShift>+space  toggle window maximize
  - <Meta>+<LShift>+<int>  move focused window to <int> workspace
  - <Meta>+<int>           switch to <int> workspace
EOL
    fi

    getent passwd | while IFS=: read -r username _ uid _ _ home_dir shell; do
        if test "\${uid}" -ge 1000 && \
           test -d "\${home_dir}" && \
           test "\${shell}" != "/sbin/nologin" && \
           test "\${shell}" != "/bin/false" && \
           test "\${home_dir#"/home"}" != "\${home_dir}"; then

           cp -r /etc/skel/. "\${home_dir}"
           if test -f "\${home_dir}/.local/CUSTOMIZATIONS.txt"; then
               ln -sfn "\${home_dir}/.local/CUSTOMIZATIONS.txt" "\${home_dir}/Desktop/CUSTOMIZATIONS.txt"
           fi

           chown -R "\${username}:\${username}" "\${home_dir}"

           printf "%s\n" "updated home directory for \${username} (\${home_dir}) with customizations"
        fi
    done

    cp -r /etc/skel/. /root

    rm -rf customize
    printf "%s\n" "applied customizations, reboot system to fully apply"

    exit 0
fi

#EOF#
$(base64 /tmp/skel.tar.gz)
EOF
rm -rf /tmp/skel.tar.gz

chmod +x "${SCRIPT_DIR}/../../include/customize-kali.sh"

printf "%s\n" "[INFO] created files/include/customize-kali.sh"
