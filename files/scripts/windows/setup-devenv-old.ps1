#Requires -RunAsAdministrator

Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction SilentlyContinue | Out-Null

$tools_dir = "C:\Tools"
$kits_dir = "C:\Tools\_kits"

New-Item $kits_dir -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
Set-MpPreference -ExclusionPath "C:\" -ErrorAction SilentlyContinue | Out-Null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$ProgressPreference = "SilentlyContinue"
$global:ProgressPreference = "SilentlyContinue"

$wc = New-Object System.Net.WebClient
$wc.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"

# sysinternals 
Write-Host "Installing SysInternals"
$wc.DownloadFile("https://download.sysinternals.com/files/SysinternalsSuite.zip", (Join-Path -Path $kits_dir -ChildPath "SysinternalsSuite.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://download.sysinternals.com/files/SysinternalsSuite.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "SysinternalsSuite.zip") 
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "SysinternalsSuite.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "SysinternalSuite")

# regshot 
Write-Host "Installing regshot"
$wc.DownloadFile("https://sourceforge.net/projects/regshot/files/latest/download", (Join-Path -Path $kits_dir -ChildPath "regshot.7z"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -UserAgent "Wget/1.21.2" -Uri "https://sourceforge.net/projects/regshot/files/latest/download" -OutFile (Join-Path -Path $kits_dir -ChildPath "regshot.7z")
Copy-Item -Path (Join-Path -Path $kits_dir -ChildPath "regshot.7z") -Destination (Join-Path -Path $tools_dir -ChildPath "regshot.7z")

# pestudio
Write-Host "Installing pestudio"
$wc.DownloadFile("https://www.winitor.com/tools/pestudio/current/pestudio.zip", (Join-Path -Path $kits_dir -ChildPath "pestudio.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://www.winitor.com/tools/pestudio/current/pestudio.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "pestudio.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "pestudio.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "pestudio")

# resource hacker
Write-Host "Installing resource hacker"
$wc.DownloadFile("http://www.angusj.com/resourcehacker/resource_hacker.zip", (Join-Path -Path $kits_dir -ChildPath "resource_hacker.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "http://www.angusj.com/resourcehacker/resource_hacker.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "resource_hacker.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "resource_hacker.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "resource_hacker")

# dnspy
Write-Host "Installing dnSpy"
$wc.DownloadFile("https://github.com/dnSpy/dnSpy/releases/download/v6.1.8/dnSpy-net-win64.zip", (Join-Path -Path $kits_dir -ChildPath "dnSpy-net-win64.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://github.com/dnSpy/dnSpy/releases/download/v6.1.8/dnSpy-net-win64.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "dnSpy-net-win64.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "dnSpy-net-win64.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "dnSpy")

# fiddler
Write-Host "Installing Fiddler"
$wc.DownloadFile("https://telerik-fiddler.s3.amazonaws.com/fiddler/FiddlerSetup.exe", (Join-Path -Path $kits_dir -ChildPath "FiddlerSetup.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://telerik-fiddler.s3.amazonaws.com/fiddler/FiddlerSetup.exe" -OutFile (Join-Path -Path $kits_dir -ChildPath "FiddlerSetup.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "FiddlerSetup.exe") -ArgumentList "/S" -Wait -Verbose

# metasploit
Write-Host "Installing Metasploit"
$wc.DownloadFile("https://windows.metasploit.com/metasploitframework-latest.msi", (Join-Path -Path $kits_dir -ChildPath "metasploitframework-latest.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://windows.metasploit.com/metasploitframework-latest.msi" -OutFile (Join-Path -Path $kits_dir -ChildPath "metasploitframework-latest.msi")
$metasploit = Join-Path -Path $kits_dir -ChildPath "metasploitframework-latest.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $metasploit /qb INSTALLLOCATION=`"C:\metasploit-framework`"" -Wait -Verbose

# burp suite pro + community
Write-Host "Installing Burp Suite Pro/Community"
$burp_pro = (Invoke-WebRequest -Uri "https://portswigger.net/burp/releases/professional/latest" -UseBasicParsing).Links | Where-Object { $_.href -like "*product=pro*jar*" } | Select-Object -ExpandProperty href
$wc.DownloadFile(("https://portswigger.net"+"$burp_pro"), (Join-Path -Path $tools_dir -ChildPath "burp_pro-latest.jar"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri ("https://portswigger.net"+"$burp_pro") -OutFile (Join-Path -Path $tools_dir -ChildPath "burp_pro-latest.jar")

$burp_community = (Invoke-WebRequest -Uri "https://portswigger.net/burp/releases/professional/latest" -UseBasicParsing).Links | Where-Object { $_.href -like "*product=community*jar*" } | Select-Object -ExpandProperty href
$wc.DownloadFile(("https://portswigger.net"+"$burp_community"), (Join-Path -Path $tools_dir -ChildPath "burp_community-latest.jar"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri ("https://portswigger.net"+"$burp_community") -OutFile (Join-Path -Path $tools_dir -ChildPath "burp_community-latest.jar")

# aws cli
Write-Host "Installing AWS CLI"
$wc.DownloadFile("https://awscli.amazonaws.com/AWSCLIV2.msi", (Join-Path -Path $kits_dir -ChildPath "AWSCLIV2.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://awscli.amazonaws.com/AWSCLIV2.msi" -OutFile (Join-Path -Path $kits_dir -ChildPath "AWSCLIV2.msi")
$awscliv2 = Join-Path -Path $kits_dir -ChildPath "AWSCLIV2.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $awscliv2 /qb" -Wait -Verbose

# SQL Server Command Line Utilities
Write-Host "Installing SQL Server CLI Utilities"
$wc.DownloadFile("https://go.microsoft.com/fwlink/?linkid=2200731", (Join-Path -Path $kits_dir -ChildPath "msodbcsql.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://go.microsoft.com/fwlink/?linkid=2200731" -OutFile (Join-Path -Path $kits_dir -ChildPath "msodbcsql.msi")
$msodbcsql_path = Join-Path -Path $kits_dir -ChildPath "msodbcsql.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $msodbcsql_path /qb IACCEPTMSODBCSQLLICENSETERMS=YES" -Wait -Verbose

$wc.DownloadFile("https://go.microsoft.com/fwlink/?linkid=2142258", (Join-Path -Path $kits_dir -ChildPath "MsSqlCmdLnUtils.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://go.microsoft.com/fwlink/?linkid=2142258" -OutFile (Join-Path -Path $kits_dir -ChildPath "MsSqlCmdLnUtils.msi")
$mssqlcmdlnutils_path = Join-Path -Path $kits_dir -ChildPath "MsSqlCmdLnUtils.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $mssqlcmdlnutils_path /qb IACCEPTMSSQLCMDLNUTILSLICENSETERMS=YES" -Wait -Verbose

# golang
Write-Host "Installing Golang" 
$wc.DownloadFile("https://go.dev/dl/go1.20.1.windows-amd64.msi", (Join-Path -Path $kits_dir -ChildPath "golang-amd64.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://go.dev/dl/go1.20.1.windows-amd64.msi" -OutFile (Join-Path -Path $kits_dir -ChildPath "golang-amd64.msi")
$golang_path = Join-Path -Path $kits_dir -ChildPath "golang-amd64.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $golang_path /qb" -Wait -Verbose

# bugged install
# winsdk/windbg
# Windows Software Development Kit - Windows 10.0.22621.1
# OptionId.WindowsPerformanceToolkit, OptionId.WindowsDesktopDebuggers, OptionId.AvrfExternal, OptionId.NetFxSoftwareDevelopmentKit, OptionId.WindowsSoftwareLogoToolkit, OptionId.IpOverUsb, OptionId.MSIInstallTools, OptionId.SigningTools, OptionId.UWPManaged, OptionId.UWPCPP, OptionId.UWPLocalized, OptionId.DesktopCPPx86, OptionId.DesktopCPPx64, OptionId.DesktopCPParm, OptionId.DesktopCPParm64, 
$wc.DownloadFile("https://download.microsoft.com/download/f/6/7/f673df4b-4df9-4e1c-b6ce-2e6b4236c802/windowssdk/winsdksetup.exe", (Join-Path -Path $tools_dir -ChildPath "winsdksetup.exe"))
#Start-Process -FilePath (Join-Path -Path $tools_dir -ChildPath "winsdksetup.exe" -ArgumentList "/features OptionId.WindowsDesktopDebuggers /q /norestart" -Wait -Verbose

# firefox-esr
Write-Host "Installing Firefox" 
$wc.DownloadFile("https://download.mozilla.org/?product=firefox-esr-latest-ssl&os=win64&lang=en-US", (Join-Path -Path $Kits_dir -ChildPath "firefox-esr.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://download.mozilla.org/?product=firefox-esr-latest-ssl&os=win64&lang=en-US" -OutFile (Join-Path -Path $Kits_dir -ChildPath "firefox-esr.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "firefox-esr.exe") -ArgumentList "/S /PreventRebootRequired=true" -Wait -Verbose

# google chrome enterprise
Write-Host "Installing Google Chrome Enterprise" 
$wc.DownloadFile("https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7B486B20EC-8AE9-BA1E-6288-EA45FE27A42D%7D%26lang%3Den%26browser%3D3%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dtrue%26ap%3Dx64-stable-statsdef_0%26brand%3DGCEA/dl/chrome/install/googlechromestandaloneenterprise64.msi", (Join-Path -Path $kits_dir -ChildPath "google-chrome-enterprise.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7B486B20EC-8AE9-BA1E-6288-EA45FE27A42D%7D%26lang%3Den%26browser%3D3%26usagestats%3D0%26appname%3D    Google%2520Chrome%26needsadmin%3Dtrue%26ap%3Dx64-stable-statsdef_0%26brand%3DGCEA/dl/chrome/install/googlechromestandaloneenterprise64.msi" -OutFile (Join-Path -Path $kits_dir -ChildPath "google-chrome-enterprise.msi")
$gc_path = Join-Path -Path $kits_dir -ChildPath "google-chrome-enterprise.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $gc_path /qb" -Wait -Verbose

# python3.10
Write-Host "Installing Python3.10" 
$wc.DownloadFile("https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe", (Join-Path -Path $kits_dir -ChildPath "python-3.10.0-amd64.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe" -OutFile (Join-Path -Path $kits_dir -ChildPath "python-3.10.0-amd64.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "python-3.10.0-amd64.exe") -ArgumentList "/quiet InstallAllUsers=1 PrependPath=1 Include_test=0" -Wait -Verbose

# hxd setup, install with HxDSetup.exe /SILENT
Write-Host "Installing HxD" 
$wc.DownloadFile("https://mh-nexus.de/downloads/HxDSetup.zip", (Join-Path -Path $kits_dir -ChildPath "HxDSetup.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://mh-nexus.de/downloads/HxDSetup.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "HxDSetup.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "HxDSetup.zip") -DestinationPath (Join-Path -Path $kits_dir -ChildPath "HxDSetup")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "HxDSetup\HxDSetup.exe") -ArgumentList "/SILENT" -Wait -Verbose
Remove-Item -Path (Join-Path -Path $kits_dir -ChildPath "HxDSetup") -Recurse -Force

# hashmyfiles
Write-Host "Installing HashMyFiles" 
$wc.DownloadFile("https://www.nirsoft.net/utils/hashmyfiles-x64.zip", (Join-Path -Path $kits_dir -ChildPath "hashmyfiles-x64.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://www.nirsoft.net/utils/hashmyfiles-x64.zip" -OutFile (Join-Path -Path $kits_dir -ChildPath "hashmyfiles-x64.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "hashmyfiles-x64.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "hashmyfiles")

# explorersuite, install with ExplorerSuite.exe /SILENT
Write-Host "Installing ExplorerSuite" 
$wc.DownloadFile("https://ntcore.com/files/ExplorerSuite.exe", (Join-Path -Path $kits_dir -ChildPath "ExplorerSuite.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri "https://ntcore.com/files/ExplorerSuite.exe" -OutFile (Join-Path -Path $kits_dir -ChildPath "ExplorerSuite.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "ExplorerSuite.exe") -ArgumentList "/SILENT" -Wait -Verbose

# apimonitor setup, silent install with apimonitor-setup.exe /s /v"/qn"
Write-Host "Installing API Monitor" 
$apimonitor = (Invoke-WebRequest -Uri "http://www.rohitab.com/download" -UseBasicParsing).Links | Where-Object { $_.href -like "*-setup-x64.exe" } | Select-Object -ExpandProperty href | Select-Object -First 1
$wc.DownloadFile($apimonitor, (Join-Path -Path $kits_dir -ChildPath "apimonitor-setup.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $apimonitor -OutFile (Join-Path -Path $kits_dir -ChildPath "apimonitor-setup.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "apimonitor-setup.exe") -ArgumentList "/s /v`"/qb`"" -Wait -Verbose

# apktool
Write-Host "Installing apktool" 
$apktool = (Invoke-WebRequest -Uri "https://ibotpeaches.github.io/Apktool/" -UseBasicParsing).Links | Where-Object { $_.href -like "*jar" } | Select-Object -ExpandProperty href | Select-Object -First 1
$wc.DownloadFile($apktool, (Join-Path -Path $tools_dir -ChildPath "apktool.jar"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $apktool -OutFile (Join-Path -Path $tools_dir -ChildPath "apktool.jar")

# tor browser
Write-Host "Installing Tor Browser" 
$tor_browser = "https://www.torproject.org/" + ((Invoke-WebRequest -Uri "https://www.torproject.org/download" -UseBasicParsing).Links | Where-Object { $_ -like "*.exe*" } | Select-Object -ExpandProperty href | Select-Object -First 1)
$wc.DownloadFile($tor_browser, (Join-Path -Path $kits_dir -ChildPath "torbrowser.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $tor_browser -OutFile (Join-Path -Path $kits_dir -ChildPath "torbrowser.exe")

# acrobat reader
#$adobe_reader = (Invoke-WebRequest -Uri "https://rdc.adobe.io/reader/downloadUrl?name=Reader%20DC%202022.003.20314%20English%20Windows(64Bit)&os=Windows%2010&site=otherversions&lang=en&api_key=dc-get-adobereader-cdn" -UseBasicParsing).Content `
#    | ConvertFrom-Json `
#    | Select-Object -ExpandProperty downloadURL
#$wc.DownloadFile($adobe_reader, (Join-Path -Path $tools_dir -ChildPath "adobe_reader.exe"))
#Start-Process -FilePath (Join-Path -Path $tools_dir -ChildPath "adobe_reader.exe") -ArgumentList "/sAll /rs /msi EULA_ACCEPT=YES" -Wait -Verbose

# subl
Write-Host "Installing Sublime Text" 
$subl = (Invoke-WebRequest -Uri "https://www.sublimetext.com/download_thanks?target=win-x64" -UseBasicParsing).Links `
    | Where-Object { $_ -like "*.exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($subl, (Join-Path -Path $kits_dir -ChildPath "sublime.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $subl -OutFile (Join-Path -Path $kits_dir -ChildPath "sublime.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "sublime.exe") -ArgumentList "/VERYSILENT /NORESTART" -Wait -Verbose

# git for windows
Write-Host "Installing git" 
$git = (Invoke-WebRequest -Uri "https://git-scm.com/download/win" -UseBasicParsing).Links `
    | Where-Object { $_.href -like "*Git*64*exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($git, (Join-Path -Path $kits_dir -ChildPath "git.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $git -OutFile (Join-Path -Path $kits_dir -ChildPath "git.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "git.exe") -ArgumentList "/VERYSILENT /NORESTART" -Wait -Verbose

# nmap
Write-Host "Installing nmap" 
$nmap = (Invoke-WebRequest -Uri "https://nmap.org/download.html" -UseBasicParsing).Links `
    | Where-Object { $_ -like "*-setup.exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($nmap, (Join-Path -Path $kits_dir -ChildPath "nmap.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $nmap -OutFile (Join-Path -Path $kits_dir -ChildPath "nmap.exe")
# can only install nmap OEM silently
# Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "nmap.exe") -ArgumentList "/S" -Wait -Verbose

Write-Host "Installing npcap" 
$npcap = (Invoke-WebRequest -Uri "https://nmap.org/download.html" -UseBasicParsing).Links `
    | Where-Object { $_ -like "*npcap*.exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($npcap, (Join-Path -Path $kits_dir -ChildPath "npcap.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $npcap -OutFile (Join-Path -Path $kits_dir -ChildPath "npcap.exe")

# putty
Write-Host "Installing Putty" 
$putty = (Invoke-WebRequest -Uri "https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html" -UseBasicParsing).Links `
    | Where-Object { $_ -like "*64bit*.msi*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($putty, (Join-Path -Path $kits_dir -ChildPath "putty.msi"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $putty -OutFile (Join-Path -Path $kits_dir -ChildPath "putty.msi")
$putty_path = Join-Path -Path $kits_dir -ChildPath "putty.msi"
Start-Process -FilePath "C:\Windows\System32\MsiExec.exe" -ArgumentList "/i $putty_path /qb" -Wait -Verbose

# heidisql
Write-Host "Installing HeidiSQL" 
$heidisql = "https://www.heidisql.com/" + ((Invoke-WebRequest -Uri "https://www.heidisql.com/download.php" -UseBasicParsing).Links | Where-Object { $_ -like "*setup*.exe*" } | Select-Object -ExpandProperty href | Select-Object -First 1)
$wc.DownloadFile($heidisql, (Join-Path -Path $kits_dir -ChildPath "heidisql.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $heidisql -OutFile (Join-Path -Path $kits_dir -ChildPath "heidisql.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "heidisql.exe") -ArgumentList "/SILENT /VERYSILENT /NORESTART /CURRENTUSER" -Wait -Verbose

# oracle java jdk 17
Write-Host "Installing Oracle Java 17" 
$java = (Invoke-WebRequest -Uri "https://www.oracle.com/java/technologies/downloads" -UseBasicParsing).Links `
    | Where-Object { $_.href -like "*jdk-17*.exe" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($java, (Join-Path -Path $kits_dir -ChildPath "jdk-17.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $java -OutFile (Join-Path -Path $kits_dir -ChildPath "jdk-17.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "jdk-17.exe") -ArgumentList "/s" -Wait -Verbose

# wireshark
Write-Host "Installing Wireshark" 
$wireshark = (Invoke-WebRequest -Uri "https://www.wireshark.org/download.html" -UseBasicParsing).Links `
    | Where-Object { $_ -like "*win64-*.exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($wireshark, (Join-Path -Path $kits_dir -ChildPath "wireshark.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $wireshark -OutFile (Join-Path -Path $kits_dir -ChildPath "wireshark.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "wireshark.exe") -ArgumentList "/S" -Wait -Verbose

# confuserex
Write-Host "Installing ConfuserEx" 
$confuserex = (Invoke-WebRequest -Uri "https://api.github.com/repos/mkaring/ConfuserEx/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*ConfuserEx*GUI*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($confuserex, (Join-Path -Path $kits_dir -ChildPath "ConfuserEx-GUI.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $confuserex -OutFile (Join-Path -Path $kits_dir -ChildPath "ConfuserEx-GUI.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "ConfuserEx-GUI.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "ConfuserEx-GUI")

# ysoserial.net
Write-Host "Installing ysoserial.net" 
$ysoserialnet = (Invoke-WebRequest -Uri "https://api.github.com/repos/pwntester/ysoserial.net/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($ysoserialnet, (Join-Path -Path $kits_dir -ChildPath "ysoserialnet.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $ysoserialnet -OutFile (Join-Path -Path $kits_dir -ChildPath "ysoserialnet.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "ysoserialnet.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "ysoserialnet")

# ysoserial
Write-Host "Installing ysoserial" 
$ysoserial = (Invoke-WebRequest -Uri "https://api.github.com/repos/frohoff/ysoserial/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*jar*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($ysoserial, (Join-Path -Path $tools_dir -ChildPath "ysoserial.jar"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $ysoserial -OutFile (Join-Path -Path $tools_dir -ChildPath "ysoserial.jar")

# jd-gui 
Write-Host "Installing jd-gui" 
$jdgui= (Invoke-WebRequest -Uri "https://api.github.com/repos/java-decompiler/jd-gui/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*jar*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($jdgui, (Join-Path -Path $tools_dir -ChildPath "jd-gui.jar"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $jdgui -OutFile (Join-Path -Path $tools_dir -ChildPath "jd-gui.jar")

# x64dbg 
Write-Host "Installing x64dbg" 
$x64dbg = (Invoke-WebRequest -Uri "https://api.github.com/repos/x64dbg/x64dbg/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { ($_ -like "*zip*") -and -not ($_ -like "*pdb*") } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($x64dbg, (Join-Path -Path $kits_dir -ChildPath "x64dbg.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $x64dbg -OutFile (Join-Path -Path $kits_dir -ChildPath "x64dbg.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "x64dbg.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "x64dbg")

# ghidra 
Write-Host "Installing Ghidra" 
$ghidra = (Invoke-WebRequest -Uri "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*ghidra_*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($ghidra, (Join-Path -Path $kits_dir -ChildPath "ghidra.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $ghidra -OutFile (Join-Path -Path $kits_dir -ChildPath "ghidra.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "ghidra.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "ghidra")

# hashcheck
Write-Host "Installing HashCheck" 
$hashcheck = (Invoke-WebRequest -Uri "https://api.github.com/repos/gurnec/HashCheck/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($hashcheck, (Join-Path -Path $kits_dir -ChildPath "hashcheck.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $hashcheck -OutFile (Join-Path -Path $kits_dir -ChildPath "hashcheck.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "hashcheck.exe") -ArgumentList "/S" -Wait -Verbose

# pd32  
Write-Host "Installing pd32" 
$pd32 = (Invoke-WebRequest -Uri "https://api.github.com/repos/glmcdona/Process-Dump/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*pd32.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($pd32, (Join-Path -Path $tools_dir -ChildPath "pd32.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $pd32 -OutFile (Join-Path -Path $tools_dir -ChildPath "pd32.exe")

# pd64
Write-Host "Installing pd64" 
$pd64 = (Invoke-WebRequest -Uri "https://api.github.com/repos/glmcdona/Process-Dump/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*pd64.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($pd64, (Join-Path -Path $tools_dir -ChildPath "pd64.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $pd64 -OutFile (Join-Path -Path $tools_dir -ChildPath "pd64.exe")

# rundotnetdll 
Write-Host "Installing rundotnetdll" 
$rundotnetdll = (Invoke-WebRequest -Uri "https://api.github.com/repos/enkomio/RunDotNetDll/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*rundotnetdll.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($rundotnetdll, (Join-Path -Path $kits_dir -ChildPath "rundotnetdll.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $rundotnetdll -OutFile (Join-Path -Path $kits_dir -ChildPath "rundotnetdll.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "rundotnetdll.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "rundotnetdll")

# uniextract2 
Write-Host "Installing uniextract2" 
$uniextract2 = (Invoke-WebRequest -Uri "https://api.github.com/repos/Bioruebe/UniExtract2/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*uniextract*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($uniextract2, (Join-Path -Path $kits_dir -ChildPath "uniextract2.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $uniextract2 -OutFile (Join-Path -Path $kits_dir -ChildPath "uniextract2.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "uniextract2.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "uniextract2")

# fakenetng 
Write-Host "Installing fakenet-ng" 
$fakenetng = (Invoke-WebRequest -Uri "https://api.github.com/repos/mandiant/flare-fakenet-ng/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*fakenet*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($fakenetng, (Join-Path -Path $kits_dir -ChildPath "fakenet-ng.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $fakenetng -OutFile (Join-Path -Path $kits_dir -ChildPath "fakenet-ng.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "fakenet-ng.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "fakenet-ng")

# cyberchef 
Write-Host "Installing CyberChef" 
$cyberchef = (Invoke-WebRequest -Uri "https://api.github.com/repos/gchq/CyberChef/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*cyberchef*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($cyberchef, (Join-Path -Path $kits_dir -ChildPath "cyberchef.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $cyberchef -OutFile (Join-Path -Path $kits_dir -ChildPath "cyberchef.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "cyberchef.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "cyberchef")

# filewindows
Write-Host "Installing file-windows" 
$filewindows = (Invoke-WebRequest -Uri "https://api.github.com/repos/nscaife/file-windows/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*file-windows*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($filewindows, (Join-Path -Path $kits_dir -ChildPath "file-windows.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $filewindows -OutFile (Join-Path -Path $kits_dir -ChildPath "file-windows.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "file-windows.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "file-windows")

# hollows_hunter32
Write-Host "Installing hollows_hunter32" 
$hollows_hunter32 = (Invoke-WebRequest -Uri "https://api.github.com/repos/hasherezade/hollows_hunter/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*hollows_hunter32*.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($hollows_hunter32, (Join-Path -Path $tools_dir -ChildPath "hollows_hunter32.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $hollows_hunter32 -OutFile (Join-Path -Path $tools_dir -ChildPath "hollows_hunter32.exe")

# hollows_hunter64
Write-Host "Installing hollows_hunter64" 
$hollows_hunter64 = (Invoke-WebRequest -Uri "https://api.github.com/repos/hasherezade/hollows_hunter/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*hollows_hunter64*.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($hollows_hunter64, (Join-Path -Path $tools_dir -ChildPath "hollows_hunter64.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $hollows_hunter64 -OutFile (Join-Path -Path $tools_dir -ChildPath "hollows_hunter64.exe")

# map, install with map_setup.exe /SILENT
Write-Host "Installing malware analyst pack" 
$map = (Invoke-WebRequest -Uri "https://api.github.com/repos/dzzie/MAP/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*map*setup*.exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($map, (Join-Path -Path $kits_dir -ChildPath "map_setup.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $map -OutFile (Join-Path -Path $kits_dir -ChildPath "map_setup.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "map_setup.exe") -ArgumentList "/silent" -Wait -Verbose

# capa 
Write-Host "Installing capa" 
$capa = (Invoke-WebRequest -Uri "https://api.github.com/repos/mandiant/capa/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*capa-*-windows*.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($capa, (Join-Path -Path $kits_dir -ChildPath "capa.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $capa -OutFile (Join-Path -Path $kits_dir -ChildPath "capa.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "capa.zip") -DestinationPath $tools_dir

# sqlrecon 
Write-Host "Installing sqlrecon" 
$sqlrecon = (Invoke-WebRequest -Uri "https://api.github.com/repos/skahwah/SQLRecon/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*sqlrecon*exe*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($sqlrecon, (Join-Path -Path $tools_dir -ChildPath "sqlrecon.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $sqlrecon -OutFile (Join-Path -Path $tools_dir -ChildPath "sqlrecon.exe")

# pebear
Write-Host "Installing pebear" 
$pebear = (Invoke-WebRequest -Uri "https://api.github.com/repos/hasherezade/pe-bear-releases/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*x64_win_vs13*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($pebear, (Join-Path -Path $kits_dir -ChildPath "pebear.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $pebear -OutFile (Join-Path -Path $kits_dir -ChildPath "pebear.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "pebear.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "pebear")

# dnspyex 
Write-Host "Installing dnSpyEx" 
$dnspyex = (Invoke-WebRequest -Uri "https://api.github.com/repos/dnSpyEx/dnSpy/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*-net-win64.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"
$wc.DownloadFile($dnspyex, (Join-Path -Path $kits_dir -ChildPath "dnSpyEx.zip"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $dnspyex -OutFile (Join-Path -Path $kits_dir -ChildPath "dnSpyEx.zip")
Expand-Archive -Path (Join-Path -Path $kits_dir -ChildPath "dnSpyEx.zip") -DestinationPath (Join-Path -Path $tools_dir -ChildPath "dnSpyEx")

# process hacker
Write-Host "Installing Process Hacker" 
$ph = (Invoke-WebRequest -Uri "https://processhacker.sourceforge.io/downloads.php" -UseBasicParsing).Links `
    | Where-Object { $_.href -like "*setup.exe*" } `
    | Select-Object -ExpandProperty href `
    | Select-Object -First 1
$wc.DownloadFile($ph, (Join-Path -Path $kits_dir -ChildPath "processhacker.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -UserAgent "Wget/1.21.2" -Uri $ph -OutFile (Join-Path -Path $kits_dir -ChildPath "processhacker.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "processhacker.exe") -ArgumentList "install /silent" -Wait -Verbose

# 7zip
Write-Host "Installing 7z" 
$zip = "https://www.7-zip.org/" + ((Invoke-WebRequest -Uri "https://www.7-zip.org/" -UseBasicParsing).Links | Where-Object { $_.href -like "*-x64.exe" } | Select-Object -ExpandProperty href)
$wc.DownloadFile($zip, (Join-Path -Path $kits_dir -ChildPath "7z.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $zip -OutFile (Join-Path -Path $kits_dir -ChildPath "7z.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "7z.exe") -ArgumentList "/S" -Wait -Verbose

# notepad++
Write-Host "Installing Notepad++" 
$npp = (Invoke-WebRequest -Uri ("https://notepad-plus-plus.org" + ((Invoke-WebRequest -Uri "https://notepad-plus-plus.org" -UseBasicParsing).Links | Where-Object { $_.outerHTML -like '*Current Version*' } | Select-Object -ExpandProperty href)) -UseBasicParsing).Links | Where-Object { $_.outerHTML -like '*npp.*.Installer.x64.exe"*' } | Select-Object -ExpandProperty href | Select-Object -First 1
$wc.DownloadFile($npp, (Join-Path -Path $kits_dir -ChildPath "npp.exe"))
#Invoke-WebRequest -UseDefaultCredentials -UseBasicParsing -TimeoutSec 500 -Uri $npp -OutFile (Join-Path -Path $kits_dir -ChildPath "npp.exe")
Start-Process -FilePath (Join-Path -Path $kits_dir -ChildPath "npp.exe") -ArgumentList "/S" -Wait -Verbose

# RSAT
Write-Host "Installing RSAT" 
Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online | Out-Null

# compress _kits for potential distribution
Write-Host "Compressing $kits_dir" 
Compress-Archive -Path $kits_dir -DestinationPath (Join-Path -Path $tools_dir -ChildPath "_kits.zip")
Remove-Item $kits_dir -Recurse -Force | Out-Null

# create shortcut to desktop
# New-Item -ItemType SymbolicLink -Path "$HOME\Desktop" -Name Tools -Value $tools_dir
$s = New-Object -ComObject WScript.Shell
$lnk = $s.CreateShortcut("$HOME\Desktop\Tools.lnk")
$lnk.TargetPath = "C:\Tools"
$lnk.Save()
