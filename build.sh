#!/bin/bash

cd "$(dirname "$0")" || exit 1

deps="packer vboxmanage vmware qemu-system-x86_64 virt-manager curl"
for i in $deps; do
    if ! command -v $i &>/dev/null; then
        printf "%s\n" "[WARN] ${i} not found or not in path"
    fi
done

if grep -E -q 'vmx|svm' /proc/cpuinfo; then
  printf "%s\n" "[INFO] hardware virtualization is supported (Intel VT-x or AMD-V)."
else
  printf "%s\n" "[INFO] hardware virtualization is not supported or enabled (Intel VT-x or AMD-V)."
  exit 1
fi

usage() {
    printf "%s\n" \
        "usage: $(basename $0) [-c] -v vmware [-v virtualbox -v qemu] -t <template1.json> | -t \"template*\" [-d http://path/to/file1.txt -d http://path/to/file2.txt] [-s <set>] [-u <network iso dir>] [-w <wait sec>] [-H] [-x] [-o <output dir>] [-n <notify id>] -h" \
        "" \
        "-c                 check and update git repo with latest changes, ignoring local changes" \
        "-v <virt>          virtualization engine; can supply multiple times; choose from vmware|virtualbox|qemu" \
        "-t <template[*]>   build specific template(s); can supply multiple times; can supply quote enclosed wildcard" \
        "-d <url>           download url file to files/include; can supply multiple times" \
        "-s <set>           build predefined set of templates; can supply multiple times" \
        "                       - kali-everything-luks" \
        "                       - kali-everything" \
        "                       - kali-default-luks" \
        "                       - kali-default" \
        "                       - kali-top10-luks" \
        "                       - kali-top10" \
        "                       - ubuntu-server" \
        "                       - ubuntu-desktop" \
        "                       - windows-server" \
        "                       - windows-desktop" \
        "-u <url dir>       url to locally hosted iso dir, default empty; e.g., http://nas.local/iso" \
        "-w <wait sec>      override default template boot_wait" \
        "-H                 enable headless mode" \
        "-x                 run the kali-download-tools.sh script as a prebuild step; useful for windows-based builds where cd_files must include files/include at runtime" \
        "-o <dir>           custom output dir" \
        "-n <notify id>     use ProjectDiscovery notify to notify on completed build" \
        "-h                 print this help message and exit" \
        "" \
        "notes:" \
        "- linux templates work with a small boot_wait e.g., 2s-5s" \
        "- windows templates require a larger boot_wait e.g., 10s"
    exit 1
}

_exit_cleanup() {
    rm -rf virtualbox_output* vmware_output* qemu_output*
}

_sigint_cleanup() {
    printf "%s\n" "[INFO] captured SIGINT, performing cleanup"
    rm -rf virtualbox_output* vmware_output* qemu_output*
}

update_repo() {
    if ! command -v git &>/dev/null; then
        printf "%s\n" "[ERR] git not found, skipping repo update"
    fi

    if ! git rev-parse --is-inside-work-tree &>/dev/null; then
        printf "%s\n" "[ERR] not a git repository, skipping repo update"
    fi

    git fetch origin

    BRANCH=$(git rev-parse --abbrev-ref HEAD)
    LOCAL_COMMIT=$(git rev-parse HEAD)
    REMOTE_COMMIT=$(git rev-parse origin/$BRANCH)

    if [ "$LOCAL_COMMIT" != "$REMOTE_COMMIT" ]; then
        printf "%s\n" "[INFO] local branch is outdated, resetting and pulling latest changes"
        git reset --hard origin/$BRANCH
        git clean -fd
        git pull origin $BRANCH --rebase
        printf "%s\n" "[INFO] repository updated successfully"
    else
        printf "%s\n" "[INFO] repository is up to date"
    fi
}

trap _exit_cleanup EXIT
trap _sigint_cleanup SIGINT

check_repo=false
virtualization=()
templates=()
download=()
build_set=()
headless=false
prebuild_tools=false

while getopts "cv:t:d:s:u:w:Hxo:n:h" opts; do
    case $opts in
        c) check_repo=true;;
        v) virtualization+=("${OPTARG}");;
        t) templates+=("${OPTARG}");;
        d) download+=("${OPTARG}");;
        s) build_set+=("${OPTARG}");;
        u) iso_url="${OPTARG}";;
        w) boot_wait="${OPTARG}";;
        H) headless=true;;
        x) prebuild_tools=true;;
        o) output="${OPTARG}";;
        n) notify_id="${OPTARG}";;
        h) usage;;
        *) usage;;
    esac
done

if "${check_repo}" == true; then
    update_repo
fi

if test "${#templates[@]}" -eq 0 && test "${#build_set[@]}" -eq 0 && test "${#virtualization[@]}" -eq 0; then
    printf "%s\n" "[ERR]  at least one template or set argument is required with -t or -s"
    printf "%s\n" "[ERR]  at least one virtualization argument is required with -v"
    usage
fi

virt_arg=()
for v in "${virtualization[@]}"; do
    case $v in
        vmware)       virt_arg+=("${v}");;
        virtualbox)   virt_arg+=("${v}");;
        qemu)         virt_arg+=("${v}");;
        *)            printf "%s\n" "[ERR]  invalid virtualization argument"; usage;;
    esac
done

to_build=()
if test "${#templates[@]}" -gt 0; then
    for template in "${templates[@]}"; do
        if test -f "${template}"; then
            to_build+=("${template}")
        else
            printf "%s\n" "[ERR]  ${template} is not a file"
            exit 1
        fi
    done
fi

if test "${#download[@]}" -gt 0; then
    for i in "${download[@]}"; do
        if curl -sSkfI "${i}" &>/dev/null; then
            (
                mkdir -p files/include &>/dev/null
                cd files/include
                printf "%s\n" "[INFO] downloading ${i} to files/include"
                curl -sSkLOJ ${i}
                printf "%s\n" "[INFO] downloaded ${i} to files/include"
            )
        else
            printf "%s\n" "[ERR] -d ${i} does not exist, skipping"
        fi
    done
fi

if test "${#build_set[@]}" -gt 0; then
    for s in "${build_set[@]}"; do
        case "${s}" in
            kali-everything-luks) to_build+=(kali-everything-offline-luks-kde.json kali-everything-offline-luks-xfce.json);;
            kali-everything)      to_build+=(kali-everything-offline-kde.json      kali-everything-offline-xfce.json);;
            kali-default-luks)    to_build+=(kali-default-offline-luks-kde.json    kali-default-offline-luks-xfce.json);;
            kali-default)         to_build+=(kali-default-offline-kde.json         kali-default-offline-xfce.json);;
            kali-top10-luks)      to_build+=(kali-top10-offline-luks-kde.json      kali-top10-offline-luks-xfce.json);;
            kali-top10)           to_build+=(kali-top10-offline-kde.json           kali-top10-offline-xfce.json);;
            ubuntu-server)        to_build+=(ubuntu-10.04-server.json ubuntu-12.04-server.json ubuntu-14.04-server.json ubuntu-16.04-server.json ubuntu-18.04-server.json ubuntu-22.04-server.json ubuntu-20.04-server.json ubuntu-24.04-server.json);;
            ubuntu-desktop)       to_build+=(ubuntu-22.04-gnome.json ubuntu-22.04-xubuntu.json ubuntu-24.04-gnome.json);;
            windows-server)       to_build+=(winserver2012-clean.json winserver2016-clean.json winserver2019-clean.json winserver2022-clean.json);;
            windows-desktop)      to_build+=(win10-22h2-pro-clean.json win10-22h2-pro-office365.json winserver2019-desktop.json);;
            *)                    printf "%s\n" "[ERR]  invalid set argument"; usage;;
        esac
        printf "%s\n" "[INFO] using build set: ${s}"
    done
fi

if test "${headless}" == "true"; then
    headless_arg="-var='headless=true'"
    printf "%s\n" "[INFO] headless mode: true"
else
    headless_arg="-var='headless=false'"
    printf "%s\n" "[INFO] headless mode: false"
fi

if test "${iso_url}"; then
    local_iso_url_arg="-var='local_iso_url=${iso_url}'"
else
    local_iso_url_arg=""
fi

if test "${boot_wait}"; then
    if [[ "${boot_wait}" == *s ]]; then
        printf "%s\n" "[INFO] overwritten boot_wait: ${boot_wait}"
        boot_wait_arg="-var='boot_wait=${boot_wait}'"
    else
        printf "%s\n" "[ERR] -w option does not end in s|m"
        exit 1
    fi
else
    boot_wait_arg=""
fi

if test "${output}"; then
    output_arg="-var='output=${output}'"
else
    output_arg=""
fi


printf "%s\n" "[INFO] validating packer templates"
for tmpl in "${to_build[@]}"; do
    printf "%s" "[INFO] ${tmpl} "
    if packer validate "${tmpl}" &>/dev/null; then
        printf "%s\n" "OK"
    else
        printf "%s\n" "NOK"
        printf "%s\n" "[ERR]  manually check ${tmpl} for errors"
        exit 1
    fi
done

build_commands=()
base_command="packer build -force -color=false ${headless_arg} ${local_iso_url_arg} ${boot_wait_arg} ${output_arg}"
for virt in "${virtualization[@]}"; do
    for tmpl in "${to_build[@]}"; do
        rand=$(tr -cd "a-f0-9" </dev/urandom | fold -w8 | head -1)
        log_file="${tmpl%.*}-${virt}-${rand}.log"
        build_command="${base_command} -only=${virt}"
        build_commands+=("${build_command} ${tmpl} | tee logs/${log_file}")
    done
done

mkdir -p logs &>/dev/null

if "${prebuild_tools}" == true; then
    printf "%s\n" "[INFO] -x option used, pre-building tools to files/include"
    /bin/bash files/scripts/linux/kali-download-tools.sh
else
    printf "%s\n" "[INFO] -x option not used, skipping pre-building tools to files/include"
fi

run_build() {
    cmd="$1"
    printf "%s\n" "[INFO] running build command: ${cmd}"
    eval "${cmd}"
}

for i in "${build_commands[@]}"; do
    run_build "$i"
    if test "${notify_id}"; then
        if ! command -v notify &>/dev/null; then
            printf "%s\n" "[ERR] supplied -n, but notify not found, skipping notification"
            exit 0
        else
            if ! test -f /etc/notify/notify.yaml; then
                printf "%s\n" "[ERR] supplied -n, but /etc/notify/notify.yaml not found, skipping notification"
                exit 0
            else
                template_name=$(printf "%s\n" "${i}" | grep -o '[^ ]*\.json')
                printf "%s\n" "finished building ${template_name}" | notify -duc -nc -duc -cl 100000 -bulk -pc /etc/notify/notify.yaml -id "${notify_id}" -mf '{{data}}' -silent
            fi
        fi
    fi
done
