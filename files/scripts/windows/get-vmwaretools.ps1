[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://packages.vmware.com/tools/releases/latest/windows/"
$src = Invoke-WebRequest -Uri $url -UseBasicParsing
$x64 = $src.Links | Where-Object { $_.href -like "*64*" } | Select-Object -ExpandProperty href
$latest = (Invoke-WebRequest -Uri ($url + $x64) -UseBasicParsing).Links | Where-Object { $_.href -like "*.exe" } | Select-Object -ExpandProperty href
$download = $url + $x64 + $latest
(New-Object System.Net.WebClient).DownloadFile($download, "C:\$latest.exe")
Write-Output "Downloaded $download to C:\$latest"
