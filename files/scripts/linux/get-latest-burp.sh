#!/bin/bash
pushd "$(dirname ${BASH_SOURCE:0})" &>/dev/null

latest_version=$(curl -sL https://portswigger.net/burp/releases/professional/latest | grep -Eo 'v20[0-9]{2}.[0-9]{1,}.[0-9]{1,}'| tr '_' '.' | sort -u | tr -d 'v')
printf "%s\n" "[INFO] found latest burp suite professional version ${latest_version}"
url="https://portswigger-cdn.net/burp/releases/download?product=pro&version=${latest_version}&type=jar"

mkdir -p ../../include &>/dev/null
if [[ ! $(\ls ../../include | grep -E "${latest_version}") ]]; then
    printf "%s\n" "[INFO] attempting to download latest burp suite professional version from ${url}"
    printf "%s\n" "[INFO] latest version of burp suite professional not found in ../../include, downloading"
    cd ../../include
    rm -rf *.jar
    printf "%s\n" "[INFO] removing any burp suite professional old versions"
    printf "%s\n" "[INFO] downloading latest burp suite professional"
    if curl -sSLOJ $url; then
        printf "%s\n" "[INFO] successfully downloaded latest burp suite professional"
    else
        printf "%s\n" "[ERR] error occured while downloading latest burp suite professional"
    fi
else
    printf "%s\n" "[INFO] found latest burp suite professional version in ../../include, skipping download"
fi
