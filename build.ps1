param (
    [Alias("t")]
    [string[]]$templates,

    [Alias("v")]
    [ValidateSet("vmware", "vbox", "all")]
    [string]$virtualization,

    [Alias("c")]
    [bool[]]$custom
)

$env:Path += ";C:\Program Files\Oracle\VirtualBox"

if ((Get-Location) -match "\s") {
    Write-Host "[ERR] The current directory path contains spaces. Rename directories that contain spaces and retry."
    Exit 1
}

function Show-Usage {
    Write-Output "usage: $(Get-Command $MyInvocation.ScriptName) [-t <template1.json> -t <template2.json>] [-t `"template*`"] -v vmware|vbox|all -c <enable extra customzations>"
    exit 1
}

function Cleanup {
    Write-Output "[INFO] Performing exit cleanup"
    Remove-Item -Recurse -Force virtualbox_output*, vmware_output* -ErrorAction SilentlyContinue
}

function Sigint-Cleanup {
    Write-Output "[INFO] Captured SIGINT, performing cleanup"
    Cleanup
    exit 1
}

trap {
    Sigint-Cleanup
}

if (-not $virtualization -and -not $templates) {
    Write-Output "[ERR] Missing required arguments"
    Show-Usage
}

if ($custom) {
    $var_param="-var='build_type=custom'" 
} else {
    $var_param=""
}

$virt_arg = switch ($virtualization) {
    "vmware" { "-only=vmware" }
    "vbox"   { "-only=virtualbox" }
    "all"    { "" }
    default  { Write-Output "[ERR] -v argument must be vmware|vbox|all"; Show-Usage }
}

$to_build = @()
$build_commands = @()

foreach ($template in $templates) {
    $expandedTemplates = Get-ChildItem -Path $template -File -ErrorAction SilentlyContinue

    if (-not $expandedTemplates) {
        Write-Output "[ERR] No files match the pattern '$template'"
        exit 1
    }

    foreach ($file in $expandedTemplates) {
        $validateProcess = Start-Process -FilePath "${pwd}/packer.exe" -ArgumentList "validate", "`"$($file.FullName)`"" -NoNewWindow -PassThru -Wait -RedirectStandardOutput ".\NUL"

        if ($validateProcess.ExitCode -eq 0) {
            Write-Output "[INFO] $($file.FullName) is a valid Packer template, continuing"
            $to_build += $file.FullName
        }
        else {
            Write-Output "[ERR] $($file.FullName) is not a valid Packer template, exiting"
            exit 1
        }
    }
}

foreach ($template in $to_build) {
    $log_file = [System.IO.Path]::ChangeExtension((Get-Item $template).Name, "log")
    $build_commands += "${pwd}\packer.exe build -color=false $virt_arg $var_param `"$template`" | Tee-Object -FilePath logs\$log_file"
}

New-Item -ItemType Directory -Force -Path logs | Out-Null

foreach ($i in $build_commands) {
    Write-Output "[INFO] Running build command: $i"
    Invoke-Expression $i
}

