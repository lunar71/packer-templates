#!/usr/bin/env python3

import os
import glob
import json
import xml.etree.ElementTree as ET
from textwrap import dedent

json_files = sorted(glob.glob(os.path.join(os.curdir, '*.json')))
#json_files = sorted(f for f in glob.glob('*.json') if not f.startswith('wsl'))

readme = dedent('''# packer-templates

## Table of Contents

### [Building templates](#building-templates-1)

### [Checksums](#checksums-1)

### [Useful commands](#useful-commands-1)

### [Notes](#notes-1)

### Available templates

<details>
<summary>Click here to display all templates</summary>

{toc}

</details>

## Building templates
- on Linux: `./build.sh [-t <template1.json> -t <template2.json>] [-t template*] -v vmware|vbox|qemu|all`
- on Windows: `.\\build.ps1 [-t <template1.json> -t <template2.json>] [-t template*] -v vmware|vbox|qemu|all`
- manually: `packer build [-only=vmware|vbox|qemu] <template>`

## Building via Jenkins server/agent on Proxmox

- ensure nested virtualization is supported

```
cat /sys/module/kvm_intel/parameters/nested
```

- enable nested virtualization for server/agent

```
echo "options kvm-intel nested=Y" | sudo tee /etc/modprobe.d/kvm-intel.conf
modprobe -r kvm_intel
modprobe kvm_intel
```

- enable nested virtualization for vm

```
qm set <vmid> --cpu host
```

- schedule parameterized builds via job configuration + Parameterized Scheduler plugin daily at 03:00 AM UTC

```
00 03 7,14,21,28 * * %kali_everything_offline_luks_kde_json=true;kali_everything_offline_luks_xfce_json=true;virt=<virtualbox|vmware|qemu>;iso_url=http://storage.lan/iso;output=/path/to/output;boot_wait=5s;download_urls=http://storage.local/file.zip,http://storage.local/file.txt;prebuild=true
```

### via Freestyle job

- configure freestyle job
- parameterize job with:

```
template:
$(ls -la *.json | awk '{{print $9}}')

virt:
virtualbox
vmware
qemu

iso_url:
http://nas.local.lan/iso

output: /path/to/output/dir
```

- build step shell script

```
if test -d "${{WORKSPACE}}/packer-templates"; then
    cd "${{WORKSPACE}}/packer-templates" && git pull
else
    git clone https://gitlab.com/lunar71/packer-templates
fi

cd "${{WORKSPACE}}/packer-templates"
/bin/bash build.sh -v "${{virt}}" -t "${{template}}" -u "${{iso_url}}" -H -j 1 -o "${{output}}"
```

### via Pipeline job

- generate a fresh Jenkinsfile:

```
python3 generate-jenkinsfile.py
```

- create a pipeline job pointing to this repo
- build a 1st time in order to add parameters to the job
- build a 2nd time with necessary parameters

## build.sh cron schedule

```
SHELL=/bin/bash
LOGNAME=root
USER=root
HOME=/root
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

00 03 7,14,21,28 * * /path/to/build.sh -c -v virtualbox -t template.json -t template.json -u http://nas.lan/iso -w 5s -H -o /path/to/nfs -x -n discord-notifications 2>&1 >>/root/cron.log
```

## build.sh notifications
- with the -n <provider id> parameter to build.sh, notifications can be sent
- requires /etc/notify/notify.yaml

```
discord:
  - id: "discord-notifications"
    discord_channel: "notifications"
    discord_username: "notifications-bot"
    discord_format: "{{data}}"
    discord_webhook_url: "https://discord.com/api/webhooks/aaa/bbb"
```

## Checksums
- all built templates run a final post-processor `create-checksums.sh|create-checksums.ps1`
- md5 and sha512 checksums are added next to each generated ova e.g.

```
vmware-ubuntu-20.04-server_2024-08-19.ova
vmware-ubuntu-20.04-server_2024-08-19.sums
```

## Useful commands
- rebuild vmmon and vmnet: `sudo vmware-modconfig --console --install-all`
- list virtualbox os types: `VBoxManage list ostypes`
- remove virtualbox old uuids: `vboxmanage list hdds; vboxmanage closemedium disk <uuid> --delete`
- add files to zip archive: `zip -urj tools.zip <file>`
- fix zip with: `zip -FFv tools.zip --out tools_fixed.zip`

## Notes
- packer opens random ports for preseed files, ensure firewall is disabled

## Templates

<details>
<summary>Click here to display template build configurations</summary>

''')

toc = ''

if os.path.isfile('files/scripts/windows/choco-packages.config'):
    with open('files/scripts/windows/choco-packages.config') as fh:
        choco_packages = fh.read()

    packages = ', '.join([i.get('id') for i in ET.fromstring(choco_packages).findall('package')])

for j in json_files:
    with open(j) as fh:
        data = json.loads(fh.read())

        variables = data.get('variables', {})
        variables = {
            'iso_name': variables.get('iso_name'),
            'iso_path': '/'.join(variables.get('iso_path').split('/')[1:]),
            'iso_sha256': variables.get('iso_sha256').split(':')[1],
            'name': variables.get('name'),
            'communicator_username': variables.get('communicator_username'),
            'communicator_password': variables.get('communicator_password'),
            'lowpriv_user': variables.get('lowpriv_user', ''),
            'cpu': variables.get('cpu'),
            'ram': int(variables.get('ram'))/1024,
            'disk_size': int(variables.get('disk_size'))/1024,
            'luks_default_password': variables.get('luks_default_password', None)
        }

        builder_types = [builder.get('name') for builder in data.get('builders', [])]

        has_sysprep = any('sysprep' in builder.get('shutdown_command', '') for builder in data.get("builders", []))
        default_creds_info = '`N/A, VM is sysprepped`' if has_sysprep else f'`{variables["communicator_username"]}`:`{variables["communicator_password"]}`, `{variables["lowpriv_user"]}`:`{variables["lowpriv_user"]}`'

        scripts = [
            script.get('script') for script in data.get('provisioners', [])
                if script.get('type') in ['powershell', 'shell']
        ]

        local_scripts = [
            (local_script.get('only_on'), local_script.get('script')) for local_script in data.get('provisioners', [])
                if local_script.get('type') == 'shell-local'
        ]

        files = [
            (file.get('source'), file.get('destination')) for file in data.get('provisioners', [])
                if file.get('type') == 'file'
        ]

        post_processors = [
            (post_procs.get('only_on'), post_procs.get('script')) for post_procs in data.get('post-processors', [])
                if post_procs.get('type') == 'shell-local'
        ]

        output_type = 'box' if any(postprocessor.get('type') == 'vagrant' \
            for postprocessor in data.get('post-processors', [])) else 'ova'

        readme += dedent(f'''
            ## {j.split('/')[1]}

            | Configuration         | Value |
            | --------------------- | ----- |
            | Builders              | {f"`{'`, `'.join(builder_types)}`" if builder_types else 'None'} |
            | ISO                   | `{variables['iso_name']}` |
            | ISO sha256 checksum   | `{variables['iso_sha256']}` |
            | Output VM             | `output/<builder>/<builder>-{variables['name']}_<timestamp>.<ova\|qemu>` |
            | Output VM checksums   | `output/<builder>/<builder>-{variables['name']}_<timestamp>.sums` |
            | Default resources     | {variables['cpu']} CPUs, {variables['ram']} GB, {variables['disk_size']} GB |
            | VM Provisioners       | {f"`{'`, `'.join(filter(None, scripts))}`" if scripts else 'None'} |
            | Local Provisioners    | {'None' if not local_scripts else ', '.join(f"`{script}` ({os[0]})" for os, script in local_scripts if os and script and len(os) > 0)}
            | Files                 | {', '.join([f'`{src}` -> `{dest}`' for src, dest in files]) or 'None'} |
            | Post Processors       | {', '.join(f"`{script}` ({os[0]})" for os, script in post_processors)} |
            | Default credentials   | {default_creds_info} |
        ''')

        toc += f'- [{j.split("/")[1]}](#{j.split("/")[1].replace(".", "")})\n'

        if variables.get('luks_default_password'):
            readme += f'| Default LUKS password | `{variables["luks_default_password"]}` |\n'

        if 'developer' in j:
            readme += f'| Choco packages | `{packages}` |\n'

readme += '\n</details>'
readme = readme.format(toc=toc)

with open('README.md', 'w') as fh:
    fh.write(readme)
