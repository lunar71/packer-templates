#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

command -v docker &>/dev/null && printf "%s\n" "[INFO] docker already installed" && exit 0

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_SUSPEND=1

curl -fsSL https://get.docker.com | bash

if getent passwd jenkins &>/dev/null; then
    printf "%s\n" "[INFO] detected jenkins user"
    if getent group docker &>/dev/null; then
        printf "%s\n" "[INFO] detected docker group"
        usermod -aG docker jenkins
        printf "%s\n" "[INFO] added jenkins user to docker group"
    fi
fi
