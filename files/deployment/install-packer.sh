#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

command -v packer &>/dev/null && printf "%s\n" "[INFO] packer already installed" && exit 0

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_SUSPEND=1
apt-get update -y
apt-get install -y curl gpg lsb-release xorriso

curl -sSL https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
printf "%s" "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
apt-get update -y
apt-get install -y packer

#hash -r
#
#if ! test command -v packer &>/dev/null; then
#    printf "%s\n" "packer not found, probably failed to install"
#    exit 1
#fi
#
#packer plugins install github.com/hashicorp/vmware
#packer plugins install github.com/hashicorp/virtualbox
#packer plugins install github.com/hashicorp/qemu
#packer plugins install github.com/hashicorp/docker
