$wc = New-Object System.Net.WebClient
$wc.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"

Write-Host "[INFO] Installing Windows Terminal" 
$wt = (Invoke-WebRequest -Uri "https://api.github.com/repos/microsoft/terminal/releases" -UseBasicParsing).Content `
    | ConvertFrom-Json `
    | ForEach { $_ | Select-Object -ExpandProperty assets | Select-Object "browser_download_url" } `
    | Where-Object { $_ -like "*windowsterminal*x64.zip*" } `
    | Select-Object -First 1 `
    | Select-Object -ExpandProperty "browser_download_url"

Write-Host "[INFO] Found Windows Terminal release $wt"

$wc.DownloadFile($wt, (Join-Path -Path C:\Windows\Temp -ChildPath "windows-terminal.zip"))
Write-Host "[INFO] Downloaded C:\Windows\Temp\windows-terminal.zip"

Expand-Archive -Path C:\Windows\Temp\windows-terminal.zip -DestinationPath "C:\Program Files" -Force
Remove-Item -Path C:\Windows\Temp\windows-terminal.zip -Force

$wtPath = Get-ChildItem -Path "C:\Program Files" -Recurse -Filter "wt.exe" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty FullName
$wtDir = Split-Path -Parent $wtPath

if (Test-Path -Type Leaf $wtPath) {
    Write-Host "[INFO] Found `"$wtPath`""
    try {
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\03WindowsTerminal", "MUIVerb", "Windows Terminal")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\03WindowsTerminal", "Icon", "`"$wtPath`"")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\03WindowsTerminal", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuWindowsTerminal")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\open", "Icon","`"$wtPath`"")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\open", "MUIVerb", "Windows Terminal")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\open\command", "", "`"$wtPath`" -d `"%V`"")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\runas", "Icon", "`"$wtPath`"")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\runas", "MUIVerb", "Elevated Windows Terminal")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\runas", "HasLUASHield", "")
        [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuWindowsTerminal\shell\runas\command", "", "`"$wtPath`" -d `"%V`"")
        Write-Host "[INFO] Added Windows Terminal to context menu"
    } catch {
        Write-Host "[ERR] Failed to add Windows Terminal to context menu"
    }

    #try {
    #    $currentPath = [Microsoft.Win32.RegistryKey]::OpenBaseKey("LocalMachine", [Microsoft.Win32.RegistryView]::Registry64).OpenSubKey("SYSTEM\CurrentControlSet\Control\Session Manager\Environment", $true).GetValue("Path")
    #    $newPath = $currentPath + ";$wtDir"
    #    [Microsoft.Win32.Registry]::SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", "Path", "$newPath")

    #    Write-Host "[INFO] Added $wtDir to global PATH"
    #} catch {
    #    Write-Host "[ERR] Error while trying to add $wtDir to global PATH"
    #}
}

