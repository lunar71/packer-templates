#!/bin/bash

if ! command -v curl &>/dev/null; then
    printf "%s\n" "[INFO] curl not found"
    exit 1
fi

declare -A urls
urls["kaspersky"]="https://www.filehorse.com/download-kaspersky-antivirus/download"
urls["bitdefender"]="https://www.filehorse.com/download-bitdefender/download"
urls["avg"]="https://www.filehorse.com/download-avg-antivirus-64/download"
#urls["avira"]="https://www.filehorse.com/download-avira/download"
urls["avast"]="https://www.filehorse.com/download-avast-antivirus/download"
urls["360ts"]="https://www.filehorse.com/download-360-total-security/download"

rm -rf ../../avs
mkdir ../../avs &>/dev/null

for i in "${!urls[@]}"; do
    f=$(curl -sSL "${urls[$i]}" | grep -oP 'href="\K[^"]+(?="[^>]*class="dl-url")')
    if curl -sSL $f -o "../../avs/${i}.exe"; then
        printf "%s\n" "[INFO] downloaded ${i}"
    else
        printf "%s\n" "[ERR] error while downloading ${i}"
    fi
done

if curl -sSL 'https://package.avira.com/download/spotlight-windows-bootstrapper/avira_en_sptl1___pavwws-spotlight-release.exe' -o "../../avs/avira.exe"; then
    printf "%s\n" "[INFO] downloaded avira"
else
    printf "%s\n" "[ERR] error while downloading avira"
fi
