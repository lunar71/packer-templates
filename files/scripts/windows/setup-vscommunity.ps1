#Requires -RunAsAdministrator

Set-MpPreference -DisableRealtimeMonitoring $true -ErrorAction SilentlyContinue | Out-Null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Write-Host "Downloading VS Community installer"
(New-Object System.Net.WebClient).DownloadFile("https://c2rsetup.officeapps.live.com/c2r/downloadVS.aspx?sku=community&channel=Release&version=VS2022&passive=true&includeRecommended=true", (Join-Path -Path C:\Windows\Temp -ChildPath "VisualStudioSetup.exe"))

Write-Host "Starting VS Community bootstrapper"
Start-Process -FilePath (Join-Path -Path C:\Windows\Temp -ChildPath "VisualStudioSetup.exe") -Wait -Verbose

Write-Host "Starting VS Community installer"
Start-Process -FilePath "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vs_installer.exe" -ArgumentList "install --productId Microsoft.VisualStudio.Product.Community --channelUri https://aka.ms/vs/17/release/channel --add Microsoft.Net.Component.4.6.2.TargetingPack --add Microsoft.Net.Component.4.7.2.SDK --add Microsoft.Net.Component.4.7.2.TargetingPack --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.VC.14.30.17.0.x86.x64 --add Microsoft.VisualStudio.Workload.ManagedDesktop --add Microsoft.VisualStudio.Workload.NativeDesktop --add Microsoft.VisualStudio.Component.CoreEditor --includeRecommended --locale en_US --passive --norestart" -Wait -Verbose

# remove this in order to install winsdk manually with windbg
# --add Microsoft.VisualStudio.Component.Windows10SDK.19041 

Write-Host "Installed VS Community"
