# https://github.com/DarwinJS/Undo-WinRMConfig/blob/master/Undo-WinRMConfig.ps1
$EnabledInboundRMPorts = @(New-object -comObject HNetCfg.FwPolicy2).rules | where-object {($_.LocalPorts -ilike '*5985*') -AND ($_.Enabled -ilike 'True')}
$EnabledInboundRMPorts += @(New-object -comObject HNetCfg.FwPolicy2).rules | where-object {($_.LocalPorts -ilike '*5986*') -AND ($_.Enabled -ilike 'True')}
ForEach ($FirewallRuleName in $EnabledInboundRMPorts) {
  netsh advfirewall firewall set rule name="$($FirewallRuleName.Name)" new enable=No
}

winrm delete winrm/config/listener?address=*+transport=http
winrm delete winrm/config/listener?address=*+transport=https

$winrm_svc = Get-Service -Name WinRM
if ($winrmService.Status -eq "Running") { Disable-PSRemoting -Force }
Stop-Service WinRM -PassThru
Set-Service WinRM -StartupType Disabled -PassThru
