syntax enable
filetype indent on 
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix
set number
set showmatch
set ignorecase
set encoding=utf-8
scriptencoding utf-8
set background=dark
colo base16-google-dark
highlight LineNr ctermbg=black
let python_highlight_all = 1
inoremap jk <esc>
nnoremap tg gT
nnoremap B ^
nnoremap E $
nnoremap $ <nop>
noremap ^ <nop>
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>
set noswapfile

let g:GPGPreferSymmetric=1
highlight StatusLineGPG ctermbg=black ctermfg=red guibg=black guifg=red
function! SetGPGStatusLine()
    setlocal laststatus=2
    setlocal statusline=%#StatusLineGPG#---\ GPG\ ENCRYPTED\ FILE\ ---
endfunction
augroup gpgFileType
    autocmd!
    autocmd User GnuPG call SetGPGStatusLine()
augroup END
