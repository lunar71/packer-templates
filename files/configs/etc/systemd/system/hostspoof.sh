#!/bin/bash

[[ ! $(command -v macchanger) ]] && printf "%s\n" "macchanger not found" && exit 1

VENDORS=(
    "microsoft corporation"
    "microsoft corp."
    "intel corporation"
    "intel corporate"
    "huawei"
    "apple"
    "apple, inc."
    "hewlett-packard"
    "hewlett packard"
    "hewlett-packard company"
)
RAND_VENDOR="${VENDORS[$((RANDOM % ${#VENDORS[@]}))]}"

OUI=$(macchanger -l | grep -iE "${RAND_VENDOR}" | shuf -n 1 | grep -oE '[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}' | tr '[[:upper:]]' '[[:lower:]]')
NID=$(printf $RANDOM | md5sum | sed 's/.\{2\}/&:/g' | cut -c 1-8)
MAC="${OUI}:${NID}"

printf "%s\n" \
    "spoofing mac vendor to ${RAND_VENDOR}" \
    "spoofing mac address to ${MAC}"

#printf "128" > /proc/sys/net/ipv4/ip_default_ttl

CURRENT_HOSTNAME=$(hostname)
if [[ "${RAND_VENDOR}" =~ microsoft|intel ]]; then
    NEW_HOSTNAME="DESKTOP-$(printf "$(printf "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | fold -w1 | shuf -n4 | tr -d '\n')$(printf "0123456789" | fold -w1 | shuf -n3 | tr -d '\n')" | fold -w1 | shuf | tr -d '\n')"
    hostnamectl set-hostname $NEW_HOSTNAME
    sed -i "s/${CURRENT_HOSTNAME}/${NEW_HOSTNAME}/g" /etc/hosts
    printf "%s\n" "set hostname to ${NEW_HOSTNAME}"

elif [[ "${RAND_VENDOR}" =~ huawei ]]; then
    HUAWEI_PREFIX=("HG" "B" "E" "WS" "MT" "NE" "AR" "HG8")
    PREFIX="${HUAWEI_PREFIX[$RANDOM % ${#HUAWEI_PREFIX[@]}]}"
    NEW_HOSTNAME="${PREFIX}$((1000 + RANDOM % 9000))"
    hostnamectl set-hostname $NEW_HOSTNAME
    sed -i "s/${CURRENT_HOSTNAME}/${NEW_HOSTNAME}/g" /etc/hosts
    printf "%s\n" "set hostname to ${NEW_HOSTNAME}"

elif [[ "${RAND_VENDOR}" =~ apple ]]; then
    IPHONE_MODELS=(
        A2221 A2223 A2160 A2215 A2217 A2161 A2220 A2218 A2275 A2296 A2298 A2176 A2398 A2400 A2399 
        A2172 A2402 A2404 A2403 A2341 A2406 A2407 A2408 A2342 A2410 A2411 A2412 A2481 A2626 A2629 
        A2630 A2628 A2482 A2631 A2634 A2635 A2633 A2483 A2636 A2639 A2640 A2638 A2484 A2641 A2644
        A2645 A2643 A2595 A2782 A2783 A2784 A2785 A2649 A2881 A2882 A2883 A2884 A2632 A2885 A2886 
        A2887 A2888 A2650 A2889 A2890 A2891 A2892 A2651 A2893 A2894 A2895 A2896 
    )
    NEW_HOSTNAME="iPhone-${IPHONE_MODELS[$RANDOM % ${#IPHONE_MODELS[@]}]}"
    hostnamectl set-hostname $NEW_HOSTNAME
    sed -i "s/${CURRENT_HOSTNAME}/${NEW_HOSTNAME}/g" /etc/hosts
    printf "%s\n" "set hostname to ${NEW_HOSTNAME}"

elif [[ "${RAND_VENDOR}" =~ hewlett ]]; then
    HP_MODELS=(
        "DeskJet-1000"   "DeskJet-1010"   "DeskJet-1100c" "DeskJet-1120c" "DeskJet-1180c" "DeskJet-1200c"
        "DeskJet-1600c"  "DeskJet-2130"   "DeskJet-2510"  "DeskJet-3070A" "DeskJet-3320"  "DeskJet-3520"
        "DeskJet-3645"   "DeskJet-3810"   "DeskJet-3920"  "DeskJet-450ci" "DeskJet-460c"  "DeskJet-5145"
        "DeskJet-5440"   "DeskJet-5650"   "DeskJet-5740"  "DeskJet-5850"  "DeskJet-5940"  "DeskJet-6122"
        "DeskJet-6520"   "DeskJet-6940"   "DeskJet-6980"  "DeskJet-710c"  "DeskJet-810c"  "DeskJet-816c"
        "DeskJet-820cXi" "DeskJet-880c"   "DeskJet-916c"  "DeskJet-9300"  "DeskJet-930c"  "DeskJet-960c"
        "DeskJet-9650"   "DeskJet-D1360"  "DeskJet-D1660" "DeskJet-D2330" "DeskJet-D4160" "DeskJet-D4260"
        "DeskJet-F2180"  "DeskJet-F22244" "DeskJet-F2420" "DeskJet-F370"  "DeskJet-F375"  "DeskJet-F380"
        "DeskJet-F4140"  "DeskJet-F4172"  "DeskJet-F4180" "DeskJet-F4190" "DeskJet-F42244"
    )
    NEW_HOSTNAME="${HP_MODELS[$RANDOM % ${#HP_MODELS[@]}]}"
    hostnamectl set-hostname $NEW_HOSTNAME
    sed -i "s/${CURRENT_HOSTNAME}/${NEW_HOSTNAME}/g" /etc/hosts
    printf "%s\n" "set hostname to ${NEW_HOSTNAME}"
fi

macchanger --mac="${MAC}" $1
