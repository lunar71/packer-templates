case $- in
    *i*) ;;
      *) return;;
esac

HISTCONTROL=ignoreboth
VIRTUAL_ENV_DISABLE_PROMPT=1
IGNOREEOF=10
PROMPT_ALTERNATIVE=twoline
NEWLINE_BEFORE_PROMPT=yes
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s checkwinsize
shopt -s histappend

export PATH=$PATH:$HOME/go/bin:$HOME/.local/bin
export PS1="\[\033[90m\]\$(_is_tmux)\[\033[0m\]\[\033[90m\]\$(_tmux_count)\[\033[0m\]\[\033[90m\]\$(_venv)\[\033[0m\]\[\033[91m\]\u@\h\[\033[0m\] in \[\033[0;38;5;33m\]\w\[\033[91m\]\$(_git_branch)\[\033[0m\] \$ "
export SHELL=/bin/bash
GPG_TTY=$(tty)
export GPG_TTY

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ll='ls --color=auto -lah'
    alias ls='ls --color=auto -lah'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'

    export LESS_TERMCAP_mb=$'\E[1;31m'
    export LESS_TERMCAP_md=$'\E[1;36m'
    export LESS_TERMCAP_me=$'\E[0m'
    export LESS_TERMCAP_so=$'\E[01;33m'
    export LESS_TERMCAP_se=$'\E[0m'
    export LESS_TERMCAP_us=$'\E[1;32m'
    export LESS_TERMCAP_ue=$'\E[0m'
fi

alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias mkdir='mkdir -pv'
alias cp='cp -r'
alias cat='cat -v'
alias vim='vim -p'
alias ll='ls --color=auto -lah'
alias ls='ls --color=auto -lah'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias ip='ip --brief --color'
alias clip='xclip -i -sel p -f | xclip -i -sel c'
alias genpasswd='tr -cd "[:alnum:]" </dev/urandom | fold -w32 | head -1'
alias noprompt='export PS1="\$ ";clear'

exit() { [[ -n "${TMUX}" ]] && tmux detach-client || builtin exit; }
reload() { source ~/.bashrc; }
tmp() { cd $(mktemp -d); }
_venv() { [[ $VIRTUAL_ENV ]] && printf "($(basename $VIRTUAL_ENV 2>/dev/null)) "; }
_git_branch() { /usr/bin/git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ (git:\1)/'; }
_tmux_count() {
    count=$(printf "%s\n" "$(tmux list-sessions 2>/dev/null | grep -v '^$' | wc -l)")
    [[ "${count}" -gt 0 && -z "${TMUX}" ]] && printf "(${count} tmux) "
}
_is_tmux() { [[ -n "${TMUX}" ]] && printf "[tmux] "; }

_ovpn_completion() {
    local DIR="${HOME}/.ovpn.d"
    [[ ! -d $DIR ]] && mkdir -p $DIR >/dev/null 2>&1
    local PROFILES=$(\ls $DIR | tr ' ' '\n')
    local PATTERN='!*.ovpn'
    local CURSOR="${COMP_WORDS[COMP_CWORD]}"
    local CURSOR_PREV="${COMP_WORDS[COMP_CWORD-1]}"
    local CURSOR_NEXT="${COMP_WORDS[COMP_CWORD+1]}"

    if [[ "${CURSOR_PREV}" == "connect" ]] || [[ "${CURSOR_PREV}" == "delete" ]]; then
        COMPREPLY=($(compgen -f -X "${PATTERN}" -W "${PROFILES}" -- "${CURSOR}"))
    elif [[ "${CURSOR_PREV}" == "install" ]]; then
        COMPREPLY=($(compgen -f -- "${CURSOR}"))
    else
        COMPREPLY=($(compgen -W "connect install delete" -- "${CURSOR}"))
    fi

    return 0
}

complete -o filenames -F _ovpn_completion ovpn

function ovpn() {
    DIR="${HOME}/.ovpn.d"

    [[ $# -lt 2 ]] && printf "%s\n" \
        "command-line OpenVPN profile manager that supports auto-completion" \
        "usage: ${FUNCNAME[0]} [connect|install|delete]" \
        "${FUNCNAME[0]} connect  <profile.ovpn>      connect using openvpn profile" \
        "${FUNCNAME[0]} install  <profile.ovpn>      installs openvpn profile ~/.ovpn.d" \
        "${FUNCNAME[0]} delete   <profile.ovpn>      deletes openvpn profile from ~/.ovpn.d" && \
        return 1

    OPT=$1
    PROFILE=$2

    case "${OPT}" in
        connect)
            sudo openvpn --cd "${DIR}" --config "${DIR}/${2}"
            ;;
        install)
            if [[ -f "${2}" ]]; then
                install --backup=numbered -o "${USER}" -g "${USER}" -m 600 --preserve-timestamps "${2}" "${DIR}"
                printf "%s\n" "Installed \"${2}\" to ${DIR}"
            elif [[ ! -f "${2}" ]]; then
                printf "%s\n" "\"${2}\" is not a file"
            fi
            ;;
        delete)
            if [[ -f "${DIR}/${2}" ]]; then
                printf "%s\n" "Deleting file \"${2}\" from ${DIR}"
                rm -f "${DIR}/${2}"
            elif [[ ! -f "${DIR}/${2}" ]]; then
                printf "%s\n" "There is no \"${2}\" file in ${DIR}, skipping"
            fi
            ;;
    esac
}

function venv() {
    [[ -z $1 ]] && printf "%s\n" \
        "create python2 or python3 virtualenv and source it" \
        "usage: ${FUNCNAME[0]} 2|3" && return 1
    [[ ! $(command -v virtualenv) ]] && printf "%s\n" "virtualenv package not installed" && return 1
    RAND=$(printf $(tr -dc 'a-z0-9' </dev/urandom | head -c 5))
    case $1 in
        "2")
            virtualenv -p python2 "py2venv-$RAND" && source "py2venv-$RAND/bin/activate";;
        "3")
            virtualenv -p python3 "py3venv-$RAND" && source "py3venv-$RAND/bin/activate";;
        *) printf "Unknown option\n";;
    esac
}

function sshtmux() {
    [[ -z $1 ]] && printf "%s\n" \
        "ssh into tmux session" \
        "usage: ${FUNCNAME[0]} <ssh args>" \
        && return 1
    ssh $@ -t 'IGNOREEOF=10 tmux attach 2>/dev/null || IGNOREEOF=10 tmux 2>/dev/null || printf "tmux not intalled, exiting...\n"'
}

function splitter() {
    [[ $# -lt 2 ]] && printf "%s\n" \
        "split file into N parts" \
        "usage: ${FUNCNAME[0]} <file.txt> <parts>" && \
        return 1
    file="$1"
    parts="$2"
    filename="${file%.*}"
    extension="${file##*.}"
    total_lines=$(wc -l < "$file")
    lines_per_part=$((total_lines / parts + 1))
    split -l "${lines_per_part}" -d --additional-suffix=".${extension}" "$file" "${filename}_"
}

function android-utils() {
    [[ ! $(command -v adb) ]] && printf "%s\n" "adb not found, exiting" && return 1
    [[ $# -lt 1 ]] && printf "%s\n" \
        "usage: ${FUNCNAME[0]} logcat <package name> | packages | ps | push-burp-cert | pull-apk <package name>" && \
        return 1
    case $1 in
        logcat)
            shift
            [[ -z $1 ]] && printf "%s\n" "supply a package name" && return 1
            adb logcat --pid=$(adb shell pidof -s $1)
            ;;
        packages)
            adb shell pm list packages | cut -d':' -f2
            ;;
        ps)
            adb shell ps -A
            ;;
        push-burp-cert)
            shift
            set -x
            [[ -z $1 ]] && PROXY='http://127.0.0.1:8080' || PROXY=$1
            curl -sSkL --proxy $PROXY -o /tmp/cacert.der http://burp/cert
            openssl x509 -inform DER -in /tmp/cacert.der -out /tmp/cacert.pem
            cp /tmp/cacert.der $(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0
            adb shell su -c "mount -o remount,rw /system"
            adb push "$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0" /sdcard/
            adb shell su -c "mv /sdcard/$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0 /system/etc/security/cacerts/"
            adb shell su -c "chmod 644 /system/etc/security/cacerts/$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem | head -1).0"
            rm $(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0
            rm /tmp/cacert.pem
            rm /tmp/cacert.der
            ;;
        pull-apk)
            shift
            [[ -z $1 ]] && printf "%s\n" "supply a package name to pull APK" && return 1
            mkdir -p $(pwd)/$1
            adb pull $(adb shell pm path $1 | cut -d':' -f2) $(pwd)/$1
            ;;
    esac
}

function username-generator() {
    [[ -z $1 ]] && printf "%s\n" \
        "generate random usernames" \
        "usage: ${FUNCNAME[0]} <length>" && \
        return 1
    [[ ! $1 =~ ^[1-9]$ ]] && printf "%s\n" "$1 is not a number between 1 and 9" && return 1

    printf "%s\n" \
        "test$(head /dev/urandom | tr -dc '0-9' | head -c $1)" \
        "user$(head /dev/urandom | tr -dc '0-9' | head -c $1)" \
        "anon$(head /dev/urandom | tr -dc '0-9' | head -c $1)"
}

function killproc() {
    [[ -z $1 ]] && printf "%s\n" \
        "kill all processes by name" \
        "usage: ${FUNCNAME[0]} <process name>" && \
        return 1
    proc="[$1:0:1]${1:1}"
    for i in $(ps aux | grep -i $1 | awk '{print $2}'); do
        printf "%s\n" "killing $i"
        kill -9 $i
    done
}

function nmap-to-csv() {
    while getopts "g:n:h" ARG; do
        case "${ARG}" in
            g) 
                parse="gnmap"
                file="${OPTARG}"
                ;;
            n) 
                parse="nmap"
                file="${OPTARG}"
                ;;
        esac
    done

    if [[ -z "${parse}" ]]; then
        printf "%s\n" "usage: ${FUNCNAME[0]} [-g <gnmap file>] | [-n <nmap file>]"
        return 1
    fi
    [[ ! -f "${file}" ]] && printf "%s\n" "${file} is not a file, exiting"

    case "${parse}" in
        gnmap)
            cat $file | awk '/Host:/ {
                ip = $2
            }
            /\/open\/tcp\// {
                for (i = 1; i <= NF; i++) {
                    if ($i ~ /^[0-9]+\/open\/tcp\//) {
                        print ip "|" $i
                    }
                }
            }
            ' | sed 's/\/\+/\//g' | awk -F '/' '{print $1"|"$4}'
            ;;
        nmap)
            cat $file | grep -v ^\# | awk -F'[ /]+' '
                /Nmap scan report for/ {
                    host = $5
                }
                /^[0-9]+\// {
                    version = ""
                    for (i = 5; i <= NF; i++) {
                        version = version $i " "
                    }
                    printf("%s|%s|%s|%s\n", host, $1, $4, version)
                }
            '
            ;;
    esac
}

function diff-to-file() {
    [[ $# -ne 2 ]] && printf "%s\n" \
        "compare two files and output the difference" \
        "usage: ${FUNCNAME[0]} <file1> <file2>" && \
        return 1

    if ! command -v comm &>/dev/null; then
        printf "%s\n" "comm not found"
        return 1
    fi

    output="${1}__not_in__${2}"
    comm -23 <(sort "$1") <(sort "$2") > "$output"
    printf "%s\n" "results of what is in $1 and not in $2 in $output"
}

function proxychains-shell() {
    if test -f /usr/lib/x86_64-linux-gnu/libproxychains.so.3; then
        LIB_PROXYCHAINS="/usr/lib/x86_64-linux-gnu/libproxychains.so.3"
    elif test -f /usr/lib/x86_64-linux-gnu/libproxychains.so.4; then
        LIB_PROXYCHAINS="/usr/lib/x86_64-linux-gnu/libproxychains.so.4"
    else
        printf "%s\n" \
            "/usr/lib/x86_64-linux-gnu/libproxychains.so.3 or /usr/lib/x86_64-linux-gnu/libproxychains.so.4 not found" \
            "is proxychains3 or libproxychains4 installed?"
        return 1
    fi
    printf "%s\n" \
        "starting proxychains shell using ${LIB_PROXYCHAINS}" \
        "type \"exit\" to return to non-proxychains shell"

    LD_PRELOAD="${LIB_PROXYCHAINS}" bash --rcfile <(echo "
        source ${HOME}/.bashrc
        export PS1='[proxychains] ${PS1}'
    ")
}
