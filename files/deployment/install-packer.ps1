$ProgressPreference = 'SilentlyContinue'

$baseUrl = "https://releases.hashicorp.com/packer/"
$response = Invoke-WebRequest -Uri $baseUrl -UseBasicParsing
$latestVersion = ($response.Links | Where-Object { $_.href -match "^\/packer\/\d+\.\d+\.\d+\/$" } | Select-Object -First 1).href -replace "\/packer\/|\/", ""

Write-Host "[INFO] Latest Packer version found: $latestVersion"

$downloadUrl = "$baseUrl$latestVersion/packer_${latestVersion}_windows_amd64.zip"
$outputPath = "${pwd}/packer_${latestVersion}_windows_amd64.zip"

Write-Host "[INFO] Downloading Packer $latestVersion..."
Invoke-WebRequest -Uri $downloadUrl -OutFile $outputPath
Write-Host "[INFO] Downloaded to $outputPath"

$extractPath = "${pwd}/packer_$latestVersion"
Expand-Archive -Path $outputPath -DestinationPath $extractPath -Force
Write-Host "[INFO] Extracted to $extractPath"
Move-Item "$extractPath\packer.exe" ../../packer.exe -Force
Remove-Item "$extractPath" -Recurse -Force
Remove-Item "$outputPath" -Force

& ..\..\packer.exe plugins install github.com/hashicorp/vmware
& ..\..\packer.exe plugins install github.com/hashicorp/virtualbox
& ..\..\packer.exe plugins install github.com/hashicorp/docker
