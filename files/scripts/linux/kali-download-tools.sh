#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
MARKER_PATH=$(realpath "${SCRIPT_DIR}/../../..")
INCLUDE_PATH=$(realpath "${SCRIPT_DIR}/../../include")
MARKER="${MARKER_PATH}/LATEST"

tmp=$(mktemp)
rm -rf "${INCLUDE_PATH}/customize-kali.sh"
# TODO: maybe fix this to not rely on sudo
sudo rm -rf "${INCLUDE_PATH}/bin"

max_wait=3600
wait_interval=90
elapsed_time=0

while docker inspect --format '{{.State.Running}}' kali-download-tools &>/dev/null; do
    printf "%s\n" "[INFO] kali-download-tools.sh currently running, waiting until done"
    sleep "$wait_interval"
    ((elapsed_time+=wait_interval))

    if ((elapsed_time >= max_wait)); then
        printf "%s\n" "[ERR] timeout reached waiting for kali-download-tools. Exiting."
        exit 1
    fi
done


if ! test -f "${MARKER}"; then
    date +%s > "${MARKER}"
    printf "%s\n" "[INFO] marker file not found, creating with today's date"
else
    run_time_now=$(date +%s)
    run_time_last=$(cat "${MARKER}")
    run_diff=$(( (run_time_now - run_time_last) / 86400 ))

    if test "${run_diff}" -ge 10; then
        printf "%s\n" "[INFO] last run time over 10 days ago, rebuilding tools and updating marker"
        date +%s > "${MARKER}"
    else
        printf "%s\n" "[INFO] last run time under 10 days, skipping rebuilding tools"
        exit 0
    fi
fi

if ! command -v docker &>/dev/null; then
    printf "%s\n" "[ERR] docker not found"
    exit 1
fi

if docker pull kalilinux/kali-last-release &>/dev/null; then
    printf "%s\n" "[INFO] pulled docker kali-last-release image"
fi

cat > $tmp << "EOF"
#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

if apt-get update; then
    printf "%s\n" "[INFO] updated packages"
fi
if apt-get install -y curl gpg apt-transport-https lsb-release \
        ca-certificates build-essential gcc musl-dev libpcap-dev \
        golang-go gcc-multilib mingw-w64 pkg-config libusb-1.0-0 \
        libnetfilter-queue1 libnetfilter-queue-dev libusb-1.0-0-dev; then
    printf "%s\n" "[INFO] installed dependencies"
fi

golang_tools="
    github.com/BishopFox/jsluice/cmd/jsluice@latest
    github.com/praetorian-inc/fingerprintx/cmd/fingerprintx@latest
    github.com/bitquark/shortscan/cmd/shortscan@latest
    golang.zx2c4.com/wireguard@latest
    github.com/lkarlslund/ldapnomnom@latest
    github.com/OJ/gobuster/v3@latest
    github.com/owasp-amass/amass/v4/...@master
    github.com/RedTeamPentesting/pretender@latest
    github.com/bettercap/bettercap@latest
    github.com/deletescape/goop@latest
    github.com/ffuf/ffuf@latest
    github.com/hakluke/hakrawler@latest
    github.com/lc/gau/v2/cmd/gau@latest
    github.com/lc/subjs@latest
    github.com/ropnop/kerbrute@latest
    github.com/sensepost/gowitness@latest
    github.com/tomnomnom/waybackurls@latest
    github.com/tomnomnom/anew@latest
    github.com/assetnote/kiterunner/cmd/kiterunner@latest
    github.com/hakluke/hakrevdns@latest
    github.com/projectdiscovery/aix/cmd/aix@latest
    github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest
    github.com/projectdiscovery/dnsx/cmd/dnsx@latest
    github.com/projectdiscovery/naabu/v2/cmd/naabu@latest
    github.com/projectdiscovery/httpx/cmd/httpx@latest
    github.com/projectdiscovery/nuclei/v2/cmd/nuclei@latest
    github.com/projectdiscovery/uncover/cmd/uncover@latest
    github.com/projectdiscovery/cloudlist/cmd/cloudlist@latest
    github.com/projectdiscovery/proxify/cmd/proxify@latest
    github.com/projectdiscovery/tlsx/cmd/tlsx@latest
    github.com/projectdiscovery/notify/cmd/notify@latest
    github.com/projectdiscovery/shuffledns/cmd/shuffledns@latest
    github.com/projectdiscovery/mapcidr/cmd/mapcidr@latest
    github.com/projectdiscovery/katana/cmd/katana@latest
    github.com/projectdiscovery/asnmap/cmd/asnmap@latest
    github.com/projectdiscovery/alterx/cmd/alterx@latest
    github.com/projectdiscovery/simplehttpserver/cmd/simplehttpserver@latest
    github.com/projectdiscovery/interactsh/cmd/interactsh-client@latest
    github.com/projectdiscovery/interactsh/cmd/interactsh-server@latest
"

mkdir -p /mnt/bin /mnt/extras/linux &>/dev/null
for i in $golang_tools; do
    if GOOS=linux go install -trimpath $i &>/dev/null; then
        printf "%s\n" "[INFO] downloaded ${i} (linux)"
    else
        printf "%s\n" "[ERR] could not download ${i} (linux)"
    fi

    if test "${i#*katana}" != "$i"; then
        if GOOS=windows CGO_ENABLED=1 CXX=x86_64-w64-mingw32-g++ CC=x86_64-w64-mingw32-gcc go install $i &>/dev/null; then
            printf "%s\n" "[INFO] downloaded ${i} (windows)"
        else
            printf "%s\n" "[ERR] could not download ${i} (windows)"
        fi
        continue
    fi

    if test "${i#*jsluice}" != "$i"; then
        if GOOS=windows CGO_ENABLED=1 CXX=x86_64-w64-mingw32-g++ CC=x86_64-w64-mingw32-gcc go install $i &>/dev/null; then
            printf "%s\n" "[INFO] downloaded ${i} (windows)"
        else
            printf "%s\n" "[ERR] could not download ${i} (windows)"
        fi
        continue
    fi

    if GOOS=windows go install -trimpath $i &>/dev/null; then
        printf "%s\n" "[INFO] downloaded ${i} (windows)"
    else
        printf "%s\n" "[ERR] could not download ${i} (windows)"
    fi
done

(
    cd /mnt/extras/linux
    if apt-get download wireguard wireguard-tools resolvconf 2>&1 >/dev/null; then
        printf "%s\n" "[INFO] downloaded extra repo debs"
    fi
)
EOF

rm -rf "${INCLUDE_PATH}/extras"
mkdir -p "${INCLUDE_PATH}/extras/linux" "${INCLUDE_PATH}/extras/windows" &>/dev/null

docker run \
    --name kali-download-tools \
    --rm -i \
    -v /tmp:/tmp \
    -v "${INCLUDE_PATH}/bin:/root/go/bin" \
    -v "${INCLUDE_PATH}/extras/linux:/mnt/extras/linux" \
    kalilinux/kali-rolling /bin/bash "${tmp}"

rm -rf "${tmp}"

curl -sSkL https://github.com/danielmiessler/SecLists/archive/refs/heads/master.zip -o "${INCLUDE_PATH}/extras/SecLists.zip"
curl -sSkL $(curl -sSkL "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases" | awk -F '"' '/browser_download_url/ {print $4}' | head -1) -o "${INCLUDE_PATH}/extras/ghidra.zip"

curl -sSkL https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o "${INCLUDE_PATH}/linux/google-chrome-stable_current_amd64.deb"
curl -sSkL $(curl -sSL 'https://www.sublimetext.com/download_thanks?target=x64-deb' | grep -Eoi '\"https://.*.deb\"' | tr -d '"' | head -1) -o "${INCLUDE_PATH}/linux/sublime-text_amd64.deb"
curl -sSkL https://out7.hex-rays.com/files/idafree81_linux.run -o "${INCLUDE_PATH}/extras/linux/idafree81_linux.run"

curl -sSkL https://github.com/dnSpy/dnSpy/releases/download/v6.1.8/dnSpy-net-win64.zip -o "${INCLUDE_PATH}/extras/windows/dnSpy-net-win64.zip"
curl -sSkL https://download.jetbrains.com/resharper/dotUltimate.2024.3.5/dotPeek64.2024.3.5.exe -o "${INCLUDE_PATH}/extras/windows/dotPeek64.2024.3.5.exe"
curl -sSkL https://windows.metasploit.com/metasploitframework-latest.msi -o "${INCLUDE_PATH}/extras/windows/metasploitframework-latest.zip"
curl -sSkL https://download.sysinternals.com/files/SysinternalsSuite.zip -o "${INCLUDE_PATH}/extras/windows/SysinternalsSuite.zip"
curl -sSkL https://github.com/Flangvik/SharpCollection/archive/refs/heads/master.zip -o "${INCLUDE_PATH}/extras/windows/SharpCollection.zip"
curl -sSkL https://nmap.org/dist/nmap-7.95-setup.exe -o "${INCLUDE_PATH}/extras/windows/nmap-7.95-setup.exe"
curl -sSkL https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe -o "${INCLUDE_PATH}/extras/windows/python-3.10.0-amd64.exe"
curl -sSkL https://go.dev/dl/go1.23.5.windows-amd64.msi -o "${INCLUDE_PATH}/extras/windows/go1.23.5.windows-amd64.msi"
curl -sSkL https://dl.google.com/dl/chrome/install/googlechromestandaloneenterprise64.msi -o "${INCLUDE_PATH}/extras/windows/googlechromestandaloneenterprise64.msi"
curl -sSkL "https://download.mozilla.org/?product=firefox-latest&os=win64&lang=en-US" -o "${INCLUDE_PATH}/extras/windows/Firefox64Setup.exe"
curl -sSkL https://www.heidisql.com/downloads/releases/HeidiSQL_12.10_64_Portable.zip -o "${INCLUDE_PATH}/extras/windows/HeidiSQL_12.10_64_Portable.zip"
curl -sSkL https://download.wireguard.com/windows-client/wireguard-amd64-0.5.3.msi -o "${INCLUDE_PATH}/extras/windows/wireguard-amd64-0.5.3.msi"
curl -sSkL $(curl -sSkL "https://api.github.com/repos/keepassxreboot/keepassxc/releases" | awk -F '"' '/browser_download_url/ {print $4}' | grep -i '.*Win64.msi$' | head -1) -o "${INCLUDE_PATH}/extras/windows/KeePassXC.msi"
curl -sSkL https://aka.ms/vs/17/release/vc_redist.x64.exe -o "${INCLUDE_PATH}/extras/windows/vc_redist.x64.exe"
curl -sSkL $(curl -sSkL "https://api.github.com/repos/x64dbg/x64dbg/releases" | awk -F '"' '/browser_download_url/ {print $4}' | grep -i '/snapshot_.*.zip' | head -1) -o "${INCLUDE_PATH}/extras/windows/x64dbg.zip"
curl -sSkL https://out7.hex-rays.com/files/idafree81_windows.exe -o "${INCLUDE_PATH}/extras/windows/idafree81_windows.exe"
