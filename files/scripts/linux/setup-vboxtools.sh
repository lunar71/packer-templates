#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt-get update -yqq
apt-get install -yqq virtualbox-guest-utils
apt-get clean -yqq
apt-get autoclean -yqq
