$ProgressPreference = 'SilentlyContinue'
Push-Location -Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent)

$downloadPageUrl = "https://www.torproject.org/download/"
$downloadPageContent = Invoke-WebRequest -Uri $downloadPageUrl -UseBasicParsing

if ($downloadPageContent.Content -match 'href="(/dist/torbrowser/[0-9.]+/tor-browser-linux-x86_64-[0-9.]+\.tar\.xz)"') {
    $latestVersion = $matches[1]
} else {
    Write-Host "[ERR] Could not find the latest version download link."
    Pop-Location
    exit 1
}

if ($downloadPageContent.Content -match 'href="(/dist/torbrowser/[0-9.]+/tor-browser-linux-x86_64-[0-9.]+\.tar\.xz\.asc)"') {
    $latestVersionSig = $matches[1]
} else {
    Write-Host "[ERR] Could not find the latest version signature link."
    Pop-Location
    exit 1
}

$latestVersionUrl = "https://www.torproject.org$latestVersion"
$latestVersionSigUrl = "https://www.torproject.org$latestVersionSig"
$latestVersionName = [System.IO.Path]::GetFileName($latestVersion)

Write-Host "[INFO] Found latest Tor Browser Bundle version $latestVersionName"

$includeDir = Join-Path -Path (Resolve-Path ..\..\include) -ChildPath ""

if (-not (Test-Path -Path $includeDir)) {
    New-Item -Path $includeDir -ItemType Directory | Out-Null
}

$existingFile = Get-ChildItem -Path $includeDir -Filter $latestVersionName -ErrorAction SilentlyContinue

if (-not $existingFile) {
    Write-Host "[INFO] Attempting to download the latest Tor Browser Bundle from $latestVersionUrl"
    Write-Host "[INFO] Latest version of Tor Browser Bundle not found in $includeDir, downloading"
    
    Push-Location -Path $includeDir
    Remove-Item *.tar.xz *.tar.xz.asc
    Write-Host "[INFO] Removing any Tor Browser Bundle old versions"

    try {
        Invoke-WebRequest -Uri $latestVersionUrl -OutFile $latestVersionName
        Write-Host "[INFO] Downloaded latest Tor Browser Bundle"

        try {
            Invoke-WebRequest -Uri $latestVersionSigUrl -OutFile "$latestVersionName.asc"
            Write-Host "[INFO] Downloaded Tor Browser Bundle signature"
        } catch {
            Write-Host "[ERR] Failed to download Tor Browser Bundle signature: $_"
        }

    } catch {
        Write-Host "[ERR] Error occurred while downloading latest Tor Browser Bundle: $_"
    }

    Pop-Location
} else {
    Write-Host "[INFO] Found latest Tor Browser Bundle version in $includeDir, skipping download"
}

Pop-Location

