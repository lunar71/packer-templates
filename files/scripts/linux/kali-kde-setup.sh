#!/bin/bash
# -*- coding: utf-8 -*-

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

mkdir -p /etc/skel/.config &>/dev/null

if command -v kwriteconfig &>/dev/null; then
    CMDWRITE="kwriteconfig"
else
    CMDWRITE="kwriteconfig5"
fi

$CMDWRITE --file /etc/xdg/kdeglobals --group 'General' --key 'fixed' 'DejaVu Sans Mono,10'

$CMDWRITE --file /etc/skel/.config/kdeglobals --group 'General' --key 'fixed' 'DejaVu Sans Mono,10'

$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.konsole.desktop' --key '_k_friendly_name' 'Launch Konsole'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.konsole.desktop' --key 'NewTab' ',none,Open a New Tab'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.konsole.desktop' --key 'NewWindow' 'none,none,Open a New Window'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.konsole.desktop' --key '_launch' 'Meta+Return,none,Launch Konsole'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.dolphin.desktop' --key '_k_friendly_name' 'Launch Dolphin'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.dolphin.desktop' --key '_launch' 'Meta+E,none,Launch Dolphin'

$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key '_k_friendly_name' 'Launch Spectacle'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key 'FullScreenScreenShot' ',none,Capture Entire Desktop'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key 'ActiveWindowScreenShot' ',none,Capture Active Window'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key 'RectangularRegionScreenShot' ',none,Capture Rectangular Region'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key '_launch' 'Meta+Shift+P,none,Launch Spectacle'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'org.kde.spectacle.desktop' --key 'CurrentMonitorScreenShot' ',none,Capture Current Monitor'

$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 1' 'Meta+!,none,Window to Desktop 1'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 1' 'Meta+1,none,Switch to Desktop 1'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 2' 'Meta+@,none,Window to Desktop 2'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 2' 'Meta+2,none,Switch to Desktop 2'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 3' 'Meta+#,none,Window to Desktop 3'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 3' 'Meta+3,none,Switch to Desktop 3'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 4' 'Meta+$,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 4' 'Meta+4,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 5' 'Meta+%,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 5' 'Meta+5,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 6' 'Meta+^,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 6' 'Meta+6,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 7' 'Meta+&,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 7' 'Meta+7,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 8' 'Meta+*,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 8' 'Meta+8,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 9' 'Meta+(,none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 9' 'Meta+9,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window to Desktop 10' 'Meta+),none,Window to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Switch to Desktop 10' 'Meta+0,none,Switch to Desktop 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window Maximize' 'Meta+Shift+Space,MetaPgUp,Maximize Window'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'kwin' --key 'Window Close' 'Meta+Shift+Q,none,Close Window'

$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 1' ',Activate Task Manager Entry 1'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 2' ',Activate Task Manager Entry 2'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 3' ',Activate Task Manager Entry 3'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 4' ',Activate Task Manager Entry 4'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 5' ',Activate Task Manager Entry 5'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 6' ',Activate Task Manager Entry 6'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 7' ',Activate Task Manager Entry 7'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 8' ',Activate Task Manager Entry 8'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 9' ',Activate Task Manager Entry 9'
$CMDWRITE --file /etc/skel/.config/kglobalshortcutsrc --group 'plasmashell' --key 'activate task manager entry 10' ',Activate Task Manager Entry 10'

$CMDWRITE --file /etc/skel/.config/konsolerc --group 'KonsoleWindow' --key 'ShowMenuBarByDefault' 'false'
$CMDWRITE --file /etc/skel/.config/konsolerc --group 'KonsoleWindow' --key 'SaveGeometryOnExit' 'false'
$CMDWRITE --file /etc/skel/.config/konsolerc --group 'MainWindow' --key 'MenuBar' 'Disabled'
$CMDWRITE --file /etc/skel/.config/konsolerc --group 'TabBar' --key 'TabBarPosition' 'Top'
$CMDWRITE --file /etc/skel/.config/konsolerc --group 'FileLocation' --key 'scrollbackUseSpecifiedLocation' 'true'
$CMDWRITE --file /etc/skel/.config/konsolerc --group 'FileLocation' --key 'scrollbackUseSystemLocation' 'false'

$CMDWRITE --file /etc/skel/.config/dolphinrc --group 'MainWindow' --key 'MenuBar' 'Enabled'
$CMDWRITE --file /etc/skel/.config/knotifyrc --group 'Sounds' --key 'No sound' 'true'
$CMDWRITE --file /etc/skel/.config/spectaclerc --group 'General' --key 'rememberLastRectangularRegion' 'UntilSpectacleIsClosed'
$CMDWRITE --file /etc/skel/.config/kwalletrc --group 'Wallet' --key 'Enabled' 'false'

$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'AC' --group 'SuspendSession' --key 'suspendThenHibernate' 'false'
$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'AC' --group 'SuspendSession' --key 'suspendType' '0'
$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'Battery' --group 'SuspendSession' --key 'suspendThenHibernate' 'false'
$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'Battery' --group 'SuspendSession' --key 'suspendType' '0'
$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'LowBattery' --group 'SuspendSession' --key 'suspendThenHibernate' 'false'
$CMDWRITE --file /etc/skel/.config/powermanagementprofilesrc --group 'LowBattery' --group 'SuspendSession' --key 'suspendType' '0'

$CMDWRITE --file /etc/skel/.config/kdeglobals --group 'KDE' --key 'SingleClick' 'false'
$CMDWRITE --file /etc/skel/.config/kdeglobals --group 'General' --key 'fixed' 'DejaVu Sans Mono,10'

mkdir -p /etc/systemd/system/sddm.service.d &>/dev/null
cat > /etc/systemd/system/sddm.service.d/override.conf << EOF
[Service]
TimeoutStopSec=1s
EOF
systemctl -q daemon-reload

update-alternatives --set x-terminal-emulator /usr/bin/konsole
