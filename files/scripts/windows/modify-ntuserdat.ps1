function Set-DefaultUserRegistryValue {
    Param (
        [Parameter(Mandatory = $true)][String]$Path,
        [Parameter(Mandatory = $true)][String]$Name,
        [Parameter(Mandatory = $true)][String]$Value,
        [Parameter(Mandatory = $true)][ValidateSet('Binary','DWord','ExpandString','MultiString','None','QWord','String','Unknown')][String]$Type
    )

    $FullPath = "HKLM:\DEFAULT\$Path"

    If (!(Test-Path $FullPath)) {
        $Item = New-Item -Path $FullPath -Force
    } else {
        $Item = Get-Item -LiteralPath $FullPath
    }

    if (Test-Path "$FullPath\$Name") {
        Set-ItemProperty -Path $FullPath -Name $Name -Value $Value -Type $Type -Force
    } else {
        New-ItemProperty -Path $FullPath -Name $Name -PropertyType $Type -Value $Value -Force
    }
    if ($Item -ne $null) {
        $Item.Handle.Close()
    }
}

& REG LOAD HKLM\DEFAULT C:\Users\Default\NTUSER.DAT

$DefaultUserRegistryKeys = @(
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\Feeds"
        "Name" = "ShellFeedsTaskbarViewMode"
        "Value" = 2
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name" = "SearchboxTaskbarMode"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\Privacy"
        "Name" = "TailoredExperiencesWithDiagnosticDataEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name" = "BingSearchEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name" = "BingSearchEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name" = "ContentDeliveryAllowed"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name" = "OemPreInstalledAppsEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" 
        "Name" = "PreInstalledAppsEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name" = "PreInstalledAppsEverEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name" = "SilentInstalledAppsEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path" = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name" = "SystemPaneSuggestionsEnabled"
        "Value" = 0
        "Type" = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name"  = "RotatingLockScreenEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager"
        "Name"  = "RotatingLockScreenOverlayEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo"
        "Name"  = "Enabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Personalization\Settings"
        "Name"  = "AcceptedPrivacyPolicy"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\InputPersonalization"
        "Name"  = "RestrictImplicitTextCollection"
        "Value" = 1
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\InputPersonalization"
        "Name"  = "RestrictImplicitInkCollection"
        "Value" = 1
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore"
        "Name"  = "HarvestContacts"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "CortanaConsent"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "HistoryViewEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "DeviceHistoryEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "VoiceShortcut"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Speech_OneCore\Preferences"
        "Name"  = "VoiceActivationOn"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Speech_OneCore\Preferences"
        "Name"  = "VoiceActivationEnableAboveLockscreen"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Speech_OneCore\Preferences"
        "Name"  = "ModelDownloadAllowed"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
        "Name"  = "ShowCortanaButton"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "CortanaEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
        "Name"  = "CanCortanaBeEnabled"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\OperationStatusManager"
        "Name"  = "EnthusiastMode"
        "Value" = 1
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
        "Name"  = "ShowTaskViewButton"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People"
        "Name"  = "PeopleBand"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer"
        "Name"  = "EnableAutoTray"
        "Value" = 0
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
        "Name"  = "Hidden"
        "Value" = 1
        "Type"  = "DWord"
    },
    @{
        "Path"  = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
        "Name"  = "HideFileExt"
        "Value" = 0
        "Type"  = "DWord"
    }
#    @{
#        "Path"  = "Console"
#        "Name"  = "FaceName"
#        "Value" = "Consolas"
#        "Type"  = "String"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "FontFamily"
#        "Value" = 0x00000036
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "WindowSize"
#        "Value" = 0x00280078
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable00"
#        "Value" = 0x00211f1d
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable01"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable02"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable03"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable04"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable05"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable06"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable07"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable08"
#        "Value" = 0x00969896
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable09"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable10"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable11"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable12"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable13"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable14"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ColorTable15"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "ScreenColors"
#        "Value" = 0x00000007
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console"
#        "Name"  = "PopupColors"
#        "Value" = 0x000000f5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "FaceName"
#        "Value" = "Consolas"
#        "Type"  = "String"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "FontFamily"
#        "Value" = 0x00000036
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "WindowSize"
#        "Value" = 0x00280078
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable00"
#        "Value" = 0x00211f1d
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable01"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable02"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable03"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable04"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable05"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable06"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable07"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable08"
#        "Value" = 0x00969896
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable09"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable10"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable11"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable12"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable13"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable14"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable15"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ScreenColors"
#        "Value" = 0x00000007
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "PopupColors"
#        "Value" = 0x000000f5
#        "Type"  = "DWord"
#    },
#
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "FaceName"
#        "Value" = "Consolas"
#        "Type"  = "String"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "FontFamily"
#        "Value" = 0x00000036
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "WindowSize"
#        "Value" = 0x00280078
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable00"
#        "Value" = 0x00211f1d
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable01"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable02"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable03"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable04"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable05"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable06"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable07"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable08"
#        "Value" = 0x00969896
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable09"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable10"
#        "Value" = 0x00448819
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable11"
#        "Value" = 0x00ed7139
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable12"
#        "Value" = 0x002b34cc
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable13"
#        "Value" = 0x00c76aa3
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable14"
#        "Value" = 0x0022a9fb
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ColorTable15"
#        "Value" = 0x00c6c8c5
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "ScreenColors"
#        "Value" = 0x00000007
#        "Type"  = "DWord"
#    },
#    @{
#        "Path"  = "Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe"
#        "Name"  = "PopupColors"
#        "Value" = 0x000000f5
#        "Type"  = "DWord"
#    }
)

$DefaultUserRegistryKeys | % {
    $path  = $_.Path
    $name  = $_.Name
    $value = $_.Value
    $type  = $_.Type
    try {
        Set-DefaultUserRegistryValue -Path $path -Name $name -Value $value -Type $type | Out-Null
        Write-Host "[INFO] Set $path\$name to $value ($type)"
    } catch {
        Write-Host "[ERR] Failed to set $path\$name to $value ($type)"
    }
}

[gc]::Collect()
& REG UNLOAD HKLM\DEFAULT
