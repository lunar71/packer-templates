try {
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "MUIVerb", "Command Prompt")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "Icon", "cmd.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\01MenuCmd", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuCmd")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "Icon","cmd.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open", "MUIVerb", "Command Prompt")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\open\command", "", "cmd.exe /s /k pushd `"%V`"")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "Icon", "cmd.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "MUIVerb", "Elevated Command Prompt")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas", "HasLUASHield", "")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuCmd\shell\runas\command", "", "cmd.exe /s /k pushd `"%V`"")
    Write-Host "[INFO] Added Command Prompt to context menu"
} catch {
    Write-Host "[ERR] Failed to add Command Prompt to context menu"
}

try {
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "MUIVerb", "PowerShell")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "Icon", "powershell.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\background\shell\02MenuPowerShell", "ExtendedSubCommandsKey", "Directory\ContextMenus\MenuPowerShell")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "Icon","powershell.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open", "MUIVerb", "PowerShell")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\open\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "Icon", "powershell.exe")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "MUIVerb", "Elevated PowerShell")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas", "HasLUASHield", "")
    [Microsoft.Win32.Registry]::SetValue("HKEY_CLASSES_ROOT\Directory\ContextMenus\MenuPowerShell\shell\runas\command", "", "powershell.exe -noexit -command Set-Location `"%V`"")
    Write-Host "[INFO] Added PowerShell to context menu"
} catch {
    Write-Host "[ERR] Failed to add PowerShell to context menu"
}
