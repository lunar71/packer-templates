#!/bin/bash
root_hist="$(find /root -type f -name .bash_history)"
user_hist="$(find /home -type f -name .bash_history | tr -s '\n' ' ')"
rm -rf $root_hist $user_hist

root_files="$(find /root -name .cache -o -name .zshrc -o -name .wget-hsts -o -name .mozilla -o -name Default | tr -s '\n' ' ')"
user_files="$(find /home -name .cache -o -name go -o -name .zshrc -o -name .wget-hsts -o -name .mozilla -o -name Default | tr -s '\n' ' ')"
rm -rf $root_files $user_files

find /var/log -maxdepth 1 -type f -exec bash -c "echo > {}" \;

crash_data_location=("/var/crash/*" "/var/log/dump/*")
for crash_data in ${crash_data_location[@]}; do rm -rf $crash_data; done

systemd_journals="$(find /var/log/journal -type f -iname "*.journal" | tr -s '\n' ' ')"
rm -rf $systemd_journals
