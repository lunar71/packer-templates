#!/bin/bash

test $EUID -ne 0 && printf "%s\n" "run as root" && exit 1

command -v vboxmanage &>/dev/null && printf "%s\n" "[INFO] vbox already installed" && exit 0

export DEBIAN_FRONTEND=noninteractive
export NEEDRESTART_SUSPEND=1
apt-get update -y
apt-get install -y curl gpg linux-generic linux-headers-generic linux-headers-$(uname -r) gcc make perl build-essential dkms

LATEST_STABLE=$(curl -sSL https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT)
URL=$(curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}")
. /etc/os-release
DEB=$(printf "%s\n" "${URL}" | grep -i "${VERSION_CODENAME}" | head -1 | grep -Eoi "\".*_amd64.deb\"" | tr -d '"')
curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${DEB}" -o /tmp/virtualbox_amd64.deb
dpkg -i /tmp/virtualbox_amd64.deb || apt-get install -fy
dpkg -i /tmp/virtualbox_amd64.deb || apt-get install -fy
rm -rf /tmp/virtualbox_amd64.deb

EXT_PACK=$(printf "%s\n" "${URL}" | grep -Eoi "\".*.vbox-extpack\"" | head -1 | tr -d '"') 
curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${EXT_PACK}" -o "/tmp/${EXT_PACK}"
echo "yes" | VBoxManage extpack install "/tmp/${EXT_PACK}"
rm -rf "/tmp/${EXT_PACK}"
